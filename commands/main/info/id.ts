import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { Omegle } from "../../../lib/sources/omegle.ts";
import { minsec } from "../../../util/number.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["id"],
    description:
      "Shows some information about your or someone else's connection",
    dict: {
      nouser: "No such user - type /list to see who's online.",
      you: "You: {0}",
      chatname: "\nChat name: {0} on mothership: {1}",
      pulse: "\n  Pulse: {0}",
      pulseid: "{0} ({1}, {2}, {3} old)",
      nopulse: "none",
    },
    args: [{ type: "text", name: "USER", required: false }],
  },
  {
    sensitive: false,
    permitted: true,
    async execute(cmd, user, args) {
      const { trollegle } = user,
        id = args.shift()?.trim();
      if (typeof id === "string") {
        const target = trollegle.users.resolve(id, trollegle.users.listable());
        if (!target) return user.serverMessage(cmd.dict.nouser);

        return user.serverMessage(target.trivia);
      }

      let result = fstring(cmd.dict.you, user.trivia);
      const { mothership } = trollegle,
        { pulses } = mothership;

      result += fstring(
        cmd.dict.chatname,
        await mothership.chatname,
        mothership.url
      );

      if (!pulses.size) result += fstring(cmd.dict.pulse, cmd.dict.nopulse);
      else
        for (const pulse of pulses.values()) {
          const { user } = pulse,
            { connection } = user,
            { interests } = connection;

          result += fstring(
            cmd.dict.pulse,
            fstring(
              cmd.dict.pulseid,
              user.nick,
              interests && interests.length
                ? interests.join(", ")
                : "<no interests>",
              (connection instanceof Omegle && connection.randid) ||
                "<no randid>",
              minsec(user.idleage || 0)
            )
          );
        }

      user.serverMessage(result);
    },
  }
);
