import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "info",
    description: "Commands for getting information about the system",
    dict: {},
  },
  {
    permitted: true,
  }
);
