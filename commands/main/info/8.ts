import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { minsec } from "../../../util/number.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["8", "eight", "9", "nine"],
    description: "Shows how long you've been in the room.",
    dict: {
      success: "You've been here for {0}. /8 count: {1}",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      user.serverMessage(
        fstring(
          cmd.dict.success,
          minsec(user.birthage || 0),
          (user.data.actions["8"] = (user.data.actions["8"] || 0) + 1)
        )
      );
    },
  }
);
