import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { formatMoney } from "../../../util/dict.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["balance", "bal", "money"],
    description: "Tells you how much money you have.",
    dict: {
      success: "You currently have {0}.",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      user.serverMessage(
        fstring(cmd.dict.success, formatMoney(user.data.balance))
      );
    },
  }
);
