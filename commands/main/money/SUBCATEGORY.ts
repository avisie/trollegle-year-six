import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Money",
    description: "Finance-related entertainment commands",
    dict: {},
  },
  {
    permitted: true,
  }
);
