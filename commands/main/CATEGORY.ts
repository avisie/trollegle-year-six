import { CommandCategoryInitializer } from "../../lib/commandhandler/mod.ts";

export default new CommandCategoryInitializer(
  {
    name: "Main",
    description: "The main command category most users can use",
    prefices: ["/"],
    dict: {
      notfound: "That isn't a known command. Type /help for more info.",
    },
  },
  {
    async permitted(category, user, command) {
      if (!command)
        return await user.serverMessage(category.dict.notfound), false;

      return true;
    },
  }
);
