import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { UserData } from "../../../lib/userlist/user.ts";
import { fdate, fstring, ftime } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["info"],
    description: "Gives you additional information about an account",
    dict: {
      usage: "Usage: /info USERNAME or ID",
      nouser: "No such user - type /list to see who's online.",
      noaccount: "That user is not logged in.",
      notfound: "Account not found.",
      ago: "{0} ago",
      success:
        "Showing information for {0}:\n" +
        "Registered at: {1} ({2})\n" +
        "Last logged in at: {3} ({4})\n" +
        "Reputation (good, bad): {5}, {6}\n" +
        "Pats (received, sent): {7}, {8}",
    },
    args: [{ type: "text", name: "USER", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const username = (args.shift() ?? "").trim();
      if (!username) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        { accounts } = trollegle;
      let u: string, data: UserData;
      if (/^\d+$/.test(username)) {
        const num = +username,
          target = trollegle.users
            .shouldbeabletoseemosttypesofuserandsystemmessagesithink()
            .get(num);
        if (!target) return user.serverMessage(cmd.dict.nouser);

        const acc = target.account;
        if (!acc) return user.serverMessage(cmd.dict.noaccount);

        u = acc.user;
        data = target.data;
      } else {
        const acc = accounts.lookup(username);
        if (!acc) return user.serverMessage(cmd.dict.notfound);

        u = acc.user;
        data = JSON.parse(acc.data);
      }

      const {
        actions: {
          pat: { received, sent },
        },
        reputation: { good, bad },
        createdat,
        lastlogin,
      } = data;
      if (createdat === null || lastlogin === null)
        throw new Error("invalid createdat or lastlogin data");

      const now = Date.now();
      user.serverMessage(
        fstring(
          cmd.dict.success,
          u,
          fdate(createdat),
          fstring(cmd.dict.ago, ftime(now - createdat)),
          fdate(lastlogin),
          fstring(cmd.dict.ago, ftime(now - lastlogin)),
          good.length,
          bad.length,
          received,
          sent
        )
      );
    },
  }
);
