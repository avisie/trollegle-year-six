import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: [
      "ignorelist",
      "blocklist",
      "mutelist",
      "ignored",
      "blocked",
      "muted",
    ],
    description: "Shows who is on your ignore list",
    dict: {
      empty: "Your ignore list is currently empty.",
      format: "\n{0} | {1}",
      successone: "There is {0} user on your ignore list:",
      success: "There are {0} users on your ignore list:",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const list = [...user.ignores.keys()],
        { length } = list;
      if (!length) return user.serverMessage(cmd.dict.empty);
      if (length === 1)
        user.serverMessage(fstring(cmd.dict.successone, length));
      else user.serverMessage(fstring(cmd.dict.success, length));

      user.serverMessage(
        list.map((u) => fstring(cmd.dict.format, u.id, u.nick)).join("")
      );
    },
  }
);
