import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { defaultUserData } from "../../../lib/userlist/user.ts";
import { fstring } from "../../../util/string.ts";

const digits = /^\d+$/;
export default new CommandInitializer(
  {
    names: ["nick", "name", "n"],
    description: "Lets you change your username in the room.",
    dict: {
      usage: "Usage: /nick NAME",
      invaliduser:
        "Username contains invalid characters. Please don't break my room.",
      taken: "Someone already has that name. Try another one.",
      same: "That already is your name!",
      notid: "Your name can't consist of nothing but digits.",
      logout: "You have been successfully logged out.",
    },
    args: [{ type: "text", name: "NAME", rest: true, required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      let nick = args.join(" ").trim();
      if (!nick) return user.serverMessage(cmd.dict.usage);
      if (digits.test(nick)) return user.serverMessage(cmd.dict.notid);

      const { trollegle } = user,
        {
          users: { nicks },
        } = trollegle;

      nick = nick.replaceAll(dict.regex.whitespacesandspaces, "-");

      if (!nicks.isValid(nick)) return user.serverMessage(cmd.dict.invaliduser);

      const { minlength: minuserlength, maxlength: maxuserlength } =
          trollegle.config.account.username,
        { length: userlength } = nick,
        userlengthcase =
          userlength < minuserlength ? -1 : userlength > maxuserlength ? 1 : 0;

      if (userlengthcase)
        return user.serverMessage(
          fstring(
            userlengthcase === 1
              ? dict.account.userlong
              : dict.account.usershort,
            fstring(dict.account.userlength, minuserlength, maxuserlength)
          )
        );

      const old = user.nick;
      if (nick === old) return user.serverMessage(cmd.dict.same);
      const { account } = user;
      if (nicks.take(nick, old, account) === false)
        return user.serverMessage(cmd.dict.taken);

      user.nick = nick;

      if (account && !nicks.filterFn(old)(nick)) {
        trollegle.accounts.used.delete(account.userlow);
        user.account = undefined;
        user.setData(defaultUserData);
        user.serverMessage(cmd.dict.logout);
      }
      trollegle.informRenamed(user, old);
    },
  }
);
