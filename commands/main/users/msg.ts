import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["msg", "pm", "bug", "msgid", "privmsg", "private", "whisper", "dm"],
    description: "Allows you to send a message to a specific user.",
    dict: {
      usage: "Usage: /pm USER MESSAGE",
      nouser: "No such user - type /list to see who's online.",
      tag: "private",
      success: "message sent",
    },
    args: [
      { type: "text", name: "USER", required: true },
      { type: "text", name: "MESSAGE", required: true },
    ],
  },
  {
    sensitive: false,
    permitted: true,
    async execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);
      let msg = args.join(" ").trim();
      if (!msg) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);

      msg = trollegle.checkMessage(user, msg);

      if ((!user.muted || target === user) && !target.ignores.has(user))
        await target
          .specialUserMessage(user, msg, dict.format.tags.pm)
          .catch(() => {});
      else
        await new Promise((r) =>
          setTimeout(r, 95 + Math.floor(Math.random() * 205))
        );

      user.serverMessage(cmd.dict.success);
    },
  }
);
