import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: [
      "clearignore",
      "clearignores",
      "clearblock",
      "clearblocks",
      "clearmute",
      "clearmutes",
      "unignoreall",
      "unblockall",
      "unmuteall",
    ],
    description: "Removes all users from your ignore list",
    dict: {
      empty: "Your ignore list is already empty.",
      success: "{0} users have been removed from your ignore list!",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const list = [...user.ignores.keys()];
      if (!list.length) return user.serverMessage(cmd.dict.empty);

      user.ignores.clear();
      user.serverMessage(fstring(cmd.dict.success, list.length));
    },
  }
);
