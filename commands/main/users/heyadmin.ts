import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: [
      "heyadmin",
      "fuckyoubecause",
      "admin",
      "summoningritual",
      "gently",
      "kindly",
    ],
    description:
      "Call the admin in on someone, or just annoy them. I don't care, I'm just a room.",
    dict: {
      // fail: "Couldn't call the admin due to a technical error. Try again later!"
      success:
        "{0} has called the admin. Might be away from the keyboard though, in which case tough luck.",
      kindly: "\n\n\nSomeone FUCKING CALLED YOU!\n\n\n",
    },
    args: [
      { type: "text", name: "USER", required: true },
      { type: "text", name: "MESSAGE", required: false, rest: true },
    ],
  },
  {
    sensitive: false,
    permitted: true,
    /* async */ execute(cmd, user) {
      if (user.muted)
        return user.serverMessage(fstring(cmd.dict.success, user.nick));

      // You could make this mention you on Discord or elsewhere.
      /* const body = JSON.stringify({
          content: cmd.dict.kindly,
        }),
        res = await fetch(
          "https://discord.com/api/v10/webhooks/...",
          {
            method: "POST",
            body,
            headers: {
              "content-type": "application/json",
              "content-length": new TextEncoder()
                .encode(body)
                .length.toString(),
            },
          }
        ).catch(() => {});
      if (!res) return user.serverMessage(cmd.dict.fail); */

      user.trollegle.cli.log(cmd.dict.kindly);

      user.trollegle.broadServer(fstring(cmd.dict.success, user.nick));
    },
  }
);
