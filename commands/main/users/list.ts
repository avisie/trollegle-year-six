import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["list", "who", "why", "users"],
    description: "Shows you the list of users in the room",
    dict: {
      count: "There are {0} users in the room:",
      format: "\n{0} | {1}",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const users = user.trollegle.users.listable(),
        count = users.size;

      user.serverMessage(fstring(cmd.dict.count, count));
      if (count > 0)
        user.serverMessage(
          users
            .map(
              (u) =>
                fstring(cmd.dict.format, u.id, u.nick) +
                (u.account ? ` ${dict.symbols.registered}` : "") +
                (user.ignores.has(u) ? ` ${dict.symbols.ignored}` : "") +
                (u.id === user.id
                  ? ` ${dict.symbols.you}`
                  : u.id === 0
                  ? ` ${dict.symbols.host}`
                  : "")
            )
            .join("")
        );
    },
  }
);
