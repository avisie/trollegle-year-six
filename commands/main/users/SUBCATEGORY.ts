import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Users",
    description: "Commands for interacting with other users",
    dict: {},
  },
  {
    permitted: true,
  }
);
