import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["invite", "iq", "ic", "ii"],
    description: "Invites another user to the chat.",
    dict: {
      toofast: "Too fast! Try again in about {0} seconds.",
      success: "{0} has invited a new user",
      successtoothpaste:
        "{0} has invited a new user (requesting toothpaste - hold on)",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const { trollegle } = user,
        lastinvite = trollegle.state.data.lastinvite,
        now = Date.now(),
        since: number | null = lastinvite && Date.now() - lastinvite,
        delay = trollegle.config.user.invitedelayms;

      if (typeof since === "number" && since < delay)
        return user.serverMessage(
          fstring(cmd.dict.toofast, Math.ceil((delay - since) / 1000))
        );

      trollegle.state.data.lastinvite = now;

      const notification = fstring(
        trollegle.config.user.toothpaste
          ? cmd.dict.successtoothpaste
          : cmd.dict.success,
        user.nick
      );
      if (user.muted) return user.serverMessage(notification);

      if (!trollegle.hasSource("omegle"))
        throw new Error("source omegle doesnt exist wtf");

      trollegle.addUser({ sourceid: "omegle", pulse: false });
      trollegle.broadServer(notification);
    },
  }
);
