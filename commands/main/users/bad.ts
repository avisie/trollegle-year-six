import { PartialAccountData } from "../../../lib/accounts/mod.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { UserData } from "../../../lib/userlist/user.ts";
import { ArrayFilter } from "../../../util/array.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["bad"],
    description: "Decrease another user's reputation",
    dict: {
      nologin: "You must be logged in to do this.",
      usage: "Usage: /bad USERNAME or ID",
      nouser: "No such user - type /list to see who's online.",
      noaccount: "That user is not logged in.",
      notfound: "Account not found.",
      noself: "You cannot decrease your own reputation!",
      already: "You have already decreased this user's reputation.",
      success: "Successfully decreased the reputation of {0}.",
    },
    args: [{ type: "text", name: "USER", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const { account } = user;
      if (!account) return user.serverMessage(cmd.dict.nologin);

      const username = (args.shift() ?? "").trim();
      if (!username) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        { accounts, users } = trollegle;
      let acc: PartialAccountData,
        data: UserData,
        offline = false;
      if (/^\d+$/.test(username)) {
        const num = +username,
          target = trollegle.users
            .shouldbeabletoseemosttypesofuserandsystemmessagesithink()
            .get(num);
        if (!target) return user.serverMessage(cmd.dict.nouser);
        if (target === user) return user.serverMessage(cmd.dict.noself);

        const a = target.account;
        if (!a) return user.serverMessage(cmd.dict.noaccount);

        acc = a;
        data = target.data;
      } else {
        const a = accounts.lookup(username);
        if (!a) return user.serverMessage(cmd.dict.notfound);

        const target = users.list.find((u) => u.account?.id === a.id);
        if (target) {
          acc = target.account!;
          data = target.data;
        } else {
          offline = true;
          acc = {
            id: a.id,
            user: a.user,
            userlow: a.userlow,
          };
          data = JSON.parse(a.data);
        }
      }

      const { user: u, userlow } = acc;
      if (account.userlow === userlow)
        return user.serverMessage(cmd.dict.noself);

      const { good, bad } = data.reputation,
        filter: ArrayFilter<string> = (str) => userlow === str;
      if (bad.find(filter)) return user.serverMessage(cmd.dict.already);

      const goodindex = good.findIndex(filter);
      if (!(goodindex < 0)) good.splice(goodindex, 1);

      bad.push(userlow);

      if (offline && !accounts.save(userlow, data))
        throw new Error(`unable to save data for ${userlow}`);

      user.serverMessage(fstring(cmd.dict.success, u));
    },
  }
);
