import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: [
      "unignore",
      "unblock",
      "unmute",
      "dontignore",
      "dontblock",
      "dontmute",
    ],
    description: "Removes a user from your ignore list",
    dict: {
      usage: "Usage: /unignore USER or /ignore USER",
      notfound: "No such user - type /list to see who's online.",
      notexists: "{0} is not on your ignore list.",
      success: "{0} has been removed from your ignore list.",
    },
    args: [{ type: "text", name: "USER", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);
      const target = user.trollegle.users.resolve(id);
      if (!target) return user.serverMessage(cmd.dict.notfound);

      if (!user.ignores.has(target))
        return user.serverMessage(fstring(cmd.dict.notexists, target.nick));

      user.ignores.delete(target);
      return user.serverMessage(fstring(cmd.dict.success, target.nick));
    },
  }
);
