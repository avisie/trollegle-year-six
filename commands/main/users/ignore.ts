import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["ignore", "block", "mute"],
    description: "Adds a user to your ignore list",
    dict: {
      usage: "Usage: /ignore USER or /unignore USER",
      notfound: "No such user - type /list to see who's online.",
      noself: "You cannot add yourself to your ignore list.",
      nohost: "You cannot add the room host to your ignore list.",
      exists: "{0} is already on your ignore list.",
      success: "{0} has been added to your ignore list.",
    },
    args: [{ type: "text", name: "USER", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);
      const target = user.trollegle.users.resolve(id);
      if (!target) return user.serverMessage(cmd.dict.notfound);

      switch (target.id) {
        case user.id:
          return user.serverMessage(cmd.dict.noself);
        case 0:
          return user.serverMessage(cmd.dict.nohost);
      }

      if (user.ignores.has(target))
        return user.serverMessage(fstring(cmd.dict.exists, target.nick));

      user.ignores.add(target);
      return user.serverMessage(fstring(cmd.dict.success, target.nick));
    },
  }
);
