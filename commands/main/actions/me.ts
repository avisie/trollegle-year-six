import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["me", "action", "self"],
    description: "Shows you the list of users in the room.",
    dict: {
      usage: "Usage: /me MESSAGE",
    },
    args: [{ name: "MESSAGE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      let msg = args.join(" ").trim();
      if (!msg.length) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user;
      msg = trollegle.checkMessage(user, msg);

      if (user.muted) return user.actionMessage(user, msg);
      trollegle.broadAction(user, msg);
    },
  }
);
