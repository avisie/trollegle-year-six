import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { normalize } from "../../../util/array.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["pat"],
    description: "Lets you pat another user through plain text.",
    dict: {
      usage: "Usage: /pat USER",
      nouser: "No such user - type /list to see who's online.",
      noself: "No free pats allowed!",
      toofast: "You're patting too fast!",
      global: [
        ["{0} pats {1} on the head!", "{0} pats {1} on the back!"],
        " (pat count: {2})",
      ],
      cooldown: 5000,
    },
    args: [{ name: "USER", type: "text", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);
      if (target === user) return user.serverMessage(cmd.dict.noself);

      const now = Date.now();
      if (now - user.data.cooldowns.pat < cmd.dict.cooldown)
        return user.serverMessage(cmd.dict.toofast);

      user.data.cooldowns.pat = now;
      user.data.actions.pat.sent++;

      user.trollegle.broadServer(
        fstring(
          normalize(cmd.dict.global),
          user.nick,
          target.nick,
          ++target.data.actions.pat.received
        )
      );
    },
  }
);
