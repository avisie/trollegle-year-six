import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Actions",
    description: "Various action commands",
    dict: {},
  },
  {
    permitted: true,
  }
);
