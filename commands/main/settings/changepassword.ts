import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["changepassword", "changepwd", "chpwd", "passwd"],
    description:
      "Lets you change the password of your (or someone else's) account.",
    dict: {
      usage: "Usage: /changepassword OLD NEW",
      notfound: "You are not logged in.",
      passlength:
        "Your password must be between {0} and {1} characters in length, inclusive.",
      passshort: "That password is too short. {0}",
      passlong: "That password is too long. {0}",
      modifying: "This account is currently being modified. Try again later.",
      checking: "Checking your credentials. Please wait.",
      incorrect: "Wrong password.",
      processing: "Your password is being changed. Please wait.",
      error:
        "An error has occurred while changing your password. Your account has not been modified.",
      success: "Successfully changed your password.",
    },
    args: [
      { name: "USER", type: "text", required: true },
      { name: "PASS", type: "text", required: true, rest: true },
    ],
  },
  {
    sensitive: true,
    permitted: true,
    async execute(cmd, user, args) {
      const { account } = user;
      if (!account) return user.serverMessage(cmd.dict.notfound);

      const oldpass = (args.shift() ?? "").trim();
      if (!oldpass) return user.serverMessage(cmd.dict.usage);

      const newpass = args.join(" ").trim();
      if (!newpass) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        { minlength: minpasslength, maxlength: maxpasslength } =
          trollegle.config.account.password,
        { length: passlength } = newpass,
        passlengthcase =
          passlength < minpasslength ? -1 : passlength > maxpasslength ? 1 : 0;

      if (passlengthcase)
        return user.serverMessage(
          fstring(
            passlengthcase === 1 ? cmd.dict.passlong : cmd.dict.passshort,
            fstring(cmd.dict.passlength, minpasslength, maxpasslength)
          )
        );

      const { accounts } = trollegle,
        { user: u, userlow } = account,
        found = accounts.lookup(u);
      if (!found)
        throw new Error(
          `user is logged in as ${user} but couldnt find account`
        );
      const oldpash = found.pash;

      if (accounts.modifying.has(userlow))
        return user.serverMessage(cmd.dict.modifying);

      user.serverMessage(cmd.dict.checking);
      if (oldpash !== (await accounts.hash(oldpass))) {
        accounts.modifying.delete(userlow);
        return user.serverMessage(cmd.dict.incorrect);
      }

      user.serverMessage(cmd.dict.processing);
      const newpash = await accounts.hash(newpass);
      accounts.changepwd(userlow, newpash);

      accounts.modifying.delete(userlow);

      user.serverMessage(cmd.dict.success);
    },
  }
);
