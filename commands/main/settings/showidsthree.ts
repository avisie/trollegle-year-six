import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { formatNick } from "../../../util/dict.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["showidsthree", "duids3", "dids3"],
    description: "Dids but the format is slim, with a free hyphen included",
    dict: {
      success: "You are a monster. (eg. {0})",
      message: "",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      user.data.dids = "psychopath";
      user.serverMessage(
        fstring(
          cmd.dict.success,
          fstring(
            dict.format.message.user.default,
            formatNick(user, user.data.dids),
            cmd.dict.message
          ).trim(),
          user.data.dids
        )
      );
    },
  }
);
