import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { formatNick } from "../../../util/dict.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["showidsfour", "duids4", "dids4"],
    description:
      "Dids but the format is the meaning of life, the universe and everything",
    dict: {
      success: "You already regret this decision. (eg. {0})",
      message: "",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      user.data.dids = "psychopath";
      user.serverMessage(
        fstring(
          cmd.dict.success,
          fstring(
            dict.format.message.user.default,
            formatNick(user, user.data.dids),
            cmd.dict.message
          ).trim(),
          user.data.dids
        )
      );
    },
  }
);
