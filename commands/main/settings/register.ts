import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["register"],
    description: "Lets you create an account to preserve your nickname.",
    dict: {
      usage: "Usage: /register USER PASS",
      loggedin: "You are already logged in as {0}.",
      loggingin: "You are already trying to register or log into an account.",
      invaliduser:
        "Username contains invalid characters. Please don't break my room.",
      creating: "This account is being created by someone else!",
      exists: "Username already registered.",
      taken: "Someone in the room already has that name. Try another one.",
      nospaces: "Please do not include space characters in your password.",
      passlength:
        "Your password must be between {0} and {1} characters in length, inclusive.",
      passshort: "That password is too short. {0}",
      passlong: "That password is too long. {0}",
      processing: "Your account is being created. Please wait.",
      success: "Successfully created your account. Welcome, {0}!",
    },
    args: [
      { name: "USER", type: "text", required: true },
      { name: "PASS", type: "text", required: true, rest: true },
    ],
  },
  {
    sensitive(_cmd, _user, args) {
      return fstring(
        dict.verbose.sensitive,
        args.splice(0, 2).join(" "),
        args.length
      );
    },
    permitted: true,
    async execute(cmd, user, args) {
      if (user.account)
        return user.serverMessage(
          fstring(cmd.dict.loggedin, user.account.user)
        );

      let username = (args.shift() ?? "").trim();
      const { trollegle } = user,
        {
          accounts,
          users: { nicks },
        } = trollegle;
      username = nicks.fix(username);
      if (!username) return user.serverMessage(cmd.dict.usage);
      if (!nicks.isValid(username))
        return user.serverMessage(cmd.dict.invaliduser);

      const password = args.join(" ").trim();
      if (!password) return user.serverMessage(cmd.dict.usage);

      const found = accounts.lookup(username);
      if (found) return user.serverMessage(cmd.dict.exists);

      if (password.includes(" ")) return user.serverMessage(cmd.dict.nospaces);

      const { minlength: minuserlength, maxlength: maxuserlength } =
          trollegle.config.account.username,
        { length: userlength } = username,
        userlengthcase =
          userlength < minuserlength ? -1 : userlength > maxuserlength ? 1 : 0;

      if (userlengthcase)
        return user.serverMessage(
          fstring(
            userlengthcase === 1
              ? dict.account.userlong
              : dict.account.usershort,
            fstring(dict.account.userlength, minuserlength, maxuserlength)
          )
        );

      if (accounts.creating.has(username.toLowerCase()))
        return user.serverMessage(cmd.dict.creating);

      const { minlength: minpasslength, maxlength: maxpasslength } =
          trollegle.config.account.password,
        { length: passlength } = password,
        passlengthcase =
          passlength < minpasslength ? -1 : passlength > maxpasslength ? 1 : 0;

      if (passlengthcase)
        return user.serverMessage(
          fstring(
            passlengthcase === 1 ? cmd.dict.passlong : cmd.dict.passshort,
            fstring(cmd.dict.passlength, minpasslength, maxpasslength)
          )
        );

      const userlow = username.toLowerCase();
      accounts.creating.add(userlow);

      const old = user.nick;
      if (nicks.take(username, old) === false) {
        accounts.creating.delete(userlow);
        return user.serverMessage(cmd.dict.taken);
      }

      user.loggingin = true;
      user.serverMessage(cmd.dict.processing);

      user.nick = username;
      trollegle.informRenamed(user, old);

      user.data.createdat = user.data.lastlogin = Date.now();

      const account = await accounts.create(username, password, user.data);
      if (!account)
        throw new Error(`got no account result while creating ${username}`);
      const { user: u } = account;

      accounts.used.add(userlow);
      accounts.creating.delete(userlow);

      user.account = {
        id: account.id,
        user: account.user,
        userlow: account.userlow,
      };

      user.loggingin = false;
      accounts.creating.delete(username);

      user.serverMessage(fstring(cmd.dict.success, u));
    },
  }
);
