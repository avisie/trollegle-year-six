import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { normalize } from "../../../util/array.ts";
import { formatNick } from "../../../util/dict.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["style"],
    description: "Changes the style of user messages.",
    dict: {
      usage: "Usage: /style STYLE - available: {0}",
      notfound: "Style not found. Available: {0}",
      success: "Successfully set your user message style to '{0}' (eg. {1})",
      message: "",
    },
    args: [
      {
        type: "text",
        name: "STYLE",
        required: true,
      },
    ],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const style = (args.shift() ?? "").trim(),
        styles = dict.format.message.user;
      if (!style)
        return user.serverMessage(
          fstring(cmd.dict.usage, Object.keys(styles).join(", "))
        );

      if (!(style in styles))
        return user.serverMessage(
          fstring(cmd.dict.notfound, Object.keys(styles).join(", "))
        );

      user.data.style = style as keyof typeof styles;

      user.serverMessage(
        fstring(
          cmd.dict.success,
          style,
          fstring(
            normalize(styles[style as keyof typeof styles]),
            // normalize(random(styles.psychopath)!),
            formatNick(user, user.data.dids),
            cmd.dict.message
          ).trim()
        )
      );
    },
  }
);
