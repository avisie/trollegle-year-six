import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Settings",
    description: "Commands to enhance your real-time Trollegle experience",
    dict: {},
  },
  {
    permitted: true,
  }
);
