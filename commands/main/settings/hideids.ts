import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: [
      "hideids",
      "huids",
      "hids",
      "hideidstwo",
      "hids2",
      "huids2",
      "hideidsthree",
      "hids3",
      "huids3",
      "hideidsfour",
      "hids4",
      "huids4",
    ],
    description:
      "Disables display of users' ids in front of most of their messages.",
    dict: {
      success: "No longer showing users' ids in front of their messages.",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      user.data.dids = "none";
      user.serverMessage(cmd.dict.success);
    },
  }
);
