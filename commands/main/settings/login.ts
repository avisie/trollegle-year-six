import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["login"],
    description: "Allows you to log into your existing account.",
    dict: {
      usage: "Usage: /login USER PASS",
      loggedin: "You are already logged in as {0}.",
      loggingin: "You are already trying to register or log into an account.",
      notfound: "Username not registered.",
      invaliduser:
        "The username of this account breaks username character rules. You are not being logged in.",
      processing: "You are being logged in. Please wait.",
      incorrect: "Wrong password.",
      taken:
        "The password was correct, but the username is not available. You have not been logged in.",
      success: "Successfully logged you in. Welcome back, {0}!",
    },
    args: [
      { name: "USER", type: "text", required: true },
      { name: "PASS", type: "text", required: true, rest: true },
    ],
  },
  {
    sensitive(_cmd, _user, args) {
      return fstring(
        dict.verbose.sensitive,
        args.splice(0, 2).join(" "),
        args.length
      );
    },
    permitted: true,
    async execute(cmd, user, args) {
      if (user.account)
        return user.serverMessage(
          fstring(cmd.dict.loggedin, user.account.user)
        );
      const { trollegle } = user,
        {
          accounts,
          users: { nicks },
        } = trollegle;
      if (user.loggingin) return user.serverMessage(cmd.dict.loggingin);

      const username = (args.shift() ?? "").trim();
      if (!username) return user.serverMessage(cmd.dict.usage);

      const password = args.join(" ").trim();
      if (!password) return user.serverMessage(cmd.dict.usage);

      const account = accounts.lookup(username);
      if (!account) return user.serverMessage(cmd.dict.notfound);

      const { user: u, userlow, pash: p, data } = account;
      if (!nicks.isValid(u)) return user.serverMessage(cmd.dict.invaliduser);

      user.loggingin = true;
      user.serverMessage(cmd.dict.processing);

      const pash = await accounts.hash(password),
        old = user.nick;
      if (accounts.used.has(userlow) || p !== pash) {
        user.loggingin = false;
        return user.serverMessage(cmd.dict.incorrect);
      }

      if (nicks.take(u, old, account) === false) {
        user.loggingin = false;
        return user.serverMessage(cmd.dict.taken);
      }

      user.nick = u;
      user.setData(JSON.parse(data));
      user.data.lastlogin = Date.now();

      accounts.used.add(userlow);
      user.account = {
        id: account.id,
        user: account.user,
        userlow: account.userlow,
      };
      user.loggingin = false;

      user.trollegle.informRenamed(user, old);
      user.serverMessage(fstring(cmd.dict.success, user.nick));
    },
  }
);
