import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["showids", "duids", "dids"],
    description:
      "Enables display of users' ids in front of most of their messages.",
    dict: {},
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(_, user) {
      user.data.dids = "classic";
    },
  }
);
