import { dict } from "../../data/dict.ts";
import { CommandInitializer } from "../../lib/commandhandler/mod.ts";
import { fstring } from "../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["help", "h", "?", "commands"],
    description: "Helps, if it actually does something.",
    dict: {
      success: `Here are the commands I accept.
/pm USER MESSAGE
    Sends a private message to the user
/me MESSAGE
    Sends an irc-like action message
/nick NAME
    Changes your name to the specifed NAME
/list
    Shows who's in the chat
/invite
    Adds another user to the chat from text mode {0}
/heyadmin MESSAGE
    Sends a message to the administrator of this system
/ignore USER or /unignore USER
    Adds or removes a user to or from your ignore list
/clearignore
    Removes all users from your ignore list
/dids or /dids2 or /dids3 or /dids4
    Enables display of user ids in front of their names
/style STYLE
    Changes what user messages look like (eg. [0 Admin] hi). Type /style for a list of available styles.
/specialstyle STYLE
    Changes how special user messages like /pms show (eg. [(private) 0 Admin] hi). Type /specialstyle for a list of available styles.
/hids
    Disables display of user ids in front of their names
/register USER PASS or /login USER PASS
    Creates an account for you to preserve your identity, pats and the like
/changepassword OLD NEW
    Changes the password of the account you are currently logged into
/good USERNAME or ID, /neutral USERNAME or ID, /bad USERNAME or ID
    Increase, reset or decrease an account's reputation. "Resetting" reputation revokes only your good or bad rating on someone.
/info USERNAME or ID
    Shows information about an account, such as when it was created, reputation, pats and such
/help
    Shows this message
Curious about how this works? Download teh codes at https://gitlab.com/truewulfey/trollegle-year-six
Want to leave and rejoin later? See https://bellawhiskey.ca/trollegle`,
      interests: "with interests {0}",
      nointerests: "without interests",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const interests = user.trollegle.config.pulse.defaultinterests;
      user.serverMessage(
        fstring(
          cmd.dict.success,
          interests && interests.length
            ? fstring(cmd.dict.interests, interests.join(", "))
            : cmd.dict.nointerests,
          Object.keys(dict.format.message.user).join(", "),
          Object.keys(dict.format.message.specialuser).join(", ")
        )
      );
    },
  }
);
