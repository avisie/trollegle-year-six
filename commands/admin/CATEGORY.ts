import { CommandCategoryInitializer } from "../../lib/commandhandler/mod.ts";

export default new CommandCategoryInitializer(
  {
    name: "Admin",
    description: "Commands for admins, or well, just the Host in this case",
    prefices: ["/.", "/!"],
    dict: {
      forbidden: "Wrong password.",
      notfound: "That isn't a known command. Type /help for more info.",
    },
  },
  {
    async permitted(cat, user, command) {
      if (!user.admin)
        return await user.serverMessage(cat.dict.forbidden), false;
      if (!command) return await user.serverMessage(cat.dict.notfound), false;

      return true;
    },
  }
);
