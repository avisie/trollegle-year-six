import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

const perpage = 50;
export default new CommandInitializer(
  {
    names: ["accountlist", "acclist", "listaccounts", "listacc"],
    description: "Shows you usernames of registered accounts",
    dict: {
      usage: "Usage: /.accountlist PAGE",
      badnumber: "Invalid page number. (1-{0})",
      success:
        "Showing usernames of accounts {0} to {1} on page {2} of {3}; {4} accounts total:\n" +
        "{5}",
    },
    args: [{ name: "PAGE", type: "text", required: false }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const page = (args.shift() ?? "").trim(),
        {
          trollegle: { accounts },
        } = user,
        count = accounts.count(),
        pagecount = Math.ceil(count / perpage);
      let p = 0;
      if (page) {
        p = Number(page) - 1;
        if (isNaN(p) || !Number.isSafeInteger(p) || p < 0 || p > pagecount - 1)
          return user.serverMessage(fstring(cmd.dict.badnumber, pagecount));
      }

      const result = accounts.fetch(perpage, perpage * p),
        start = p * perpage,
        end = start + result.length;
      user.serverMessage(
        fstring(
          cmd.dict.success,
          start + 1,
          end,
          p + 1,
          pagecount,
          count,
          result.map((a) => a.user).join(" ")
        )
      );
    },
  }
);
