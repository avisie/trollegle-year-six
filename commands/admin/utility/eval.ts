import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { BaseDummySource } from "../../../lib/sources/types.ts";

export default new CommandInitializer(
  {
    names: ["eval", "e"],
    description: "Returns the output of evaluating some JavaScript code.",
    dict: {},
    args: [{ name: "CODE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    // deno-lint-ignore no-unused-vars
    async execute(cmd, user, args) {
      if (!(user.connection instanceof BaseDummySource))
        return user.serverMessage("undefined");

      const { cli } = user.trollegle,
        code = `(async()=>{${args.join(" ")}})()`;
      try {
        cli.log(await eval(code));
      } catch (e) {
        cli.log("Error:", e instanceof Error ? e.stack : e);
      }
    },
  }
);
