import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Utility",
    description: "Commands for testing, configuration and whatnot",
    dict: {},
  },
  {
    permitted: true,
  }
);
