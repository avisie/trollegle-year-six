import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["massacre", "kill"],
    description: "Makes the room die.",
    dict: {
      pulsehalfsuccess:
        "Successfully took down {0} of {1} pulse(s), failed to take down {2} pulse(s)",
      pulsesuccess: "Successfully took down {0} of {1} pulse(s)",
      userhalfsuccess:
        "Successfully disconnected {0} of {1} user(s), failed to disconnect {2} user(s)",
      usersuccess: "Successfully disconnected {0} of {1} user(s)",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    async execute(cmd, user) {
      const { trollegle } = user,
        { mothership } = trollegle;

      // pulses

      const { pulses } = mothership,
        pulsetotal = pulses.size;
      let pulsesuccesses = 0,
        pulsefails = 0;
      for (const { time } of pulses.values()) {
        if (!(await mothership.takedown(time).catch(() => {}))) {
          pulsefails++;
          continue;
        }
        pulsesuccesses++;
      }

      if (pulsefails)
        await user.serverMessage(
          fstring(
            cmd.dict.pulsehalfsuccess,
            pulsesuccesses,
            pulsetotal,
            pulsefails
          )
        );
      else
        await user.serverMessage(
          fstring(cmd.dict.pulsesuccess, pulsesuccesses, pulsetotal)
        );

      // users

      const users = trollegle.users.except(user),
        usertotal = users.size;
      let usersuccesses = 0,
        userfails = 0;
      for (const { connection } of users.values()) {
        if (!(await connection.disconnect(true).catch(() => {}))) {
          userfails++;
          continue;
        }
        usersuccesses++;
      }

      if (userfails)
        await user.serverMessage(
          fstring(cmd.dict.userhalfsuccess, usersuccesses, usertotal, userfails)
        );
      else
        await user.serverMessage(
          fstring(cmd.dict.usersuccess, usersuccesses, usertotal)
        );

      trollegle.kill();
    },
  }
);
