import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["announce", "ann", "a"],
    description: "Make a cool announcement!",
    dict: {
      usage: "Usage: /.announce MESSAGE",
    },
    args: [{ name: "MESSAGE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      let msg = args.join(" ").trim();
      if (!msg.length) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user;
      msg = trollegle.checkMessage(user, msg);

      trollegle.broadServer(msg);
    },
  }
);
