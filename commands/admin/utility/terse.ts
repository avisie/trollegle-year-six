import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["terse", "t"],
    description: "Start seeing the commands users execute.",
    dict: {
      already: "You did not previously enable verbose.",
      success: "Verbose disabled.",
    },
    args: [{ name: "MESSAGE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      if (!user.data.verbose) return user.serverMessage(cmd.dict.already);

      user.data.verbose = false;
      return user.serverMessage(cmd.dict.success);
    },
  }
);
