import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["setinterests"],
    description: "Set interests used when inviting users with /invite.",
    dict: {
      success: "Successfully set default interests to {0}",
      successnone: "Successfully reset default interests to none",
    },
    args: [{ type: "text", name: "INTERESTS", required: false, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const list = args.map((str) => str.trim()).filter((str) => str.length),
        { trollegle } = user;

      if (!list.length) {
        trollegle.config.pulse.defaultinterests = null;
        user.serverMessage(cmd.dict.successnone);
      } else {
        trollegle.config.pulse.defaultinterests = list;
        user.serverMessage(fstring(cmd.dict.success, list.join(", ")));
      }
    },
  }
);
