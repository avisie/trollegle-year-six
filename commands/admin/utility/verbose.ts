import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["verbose", "v"],
    description: "Stop seeing the commands users execute.",
    dict: {
      already: "You already enabled verbose!",
      success: "Verbose enabled.",
    },
    args: [{ name: "MESSAGE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      if (user.data.verbose) return user.serverMessage(cmd.dict.already);

      user.data.verbose = true;
      return user.serverMessage(cmd.dict.success);
    },
  }
);
