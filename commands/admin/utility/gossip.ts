import { dict } from "../../../data/dict.ts";
import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["gossip", "goss", "g", "modchat", "mchat", "mc", "c"],
    description: "Those bloody admins.",
    dict: {
      usage: "Usage: /.gossip MESSAGE",
    },
    args: [{ name: "MESSAGE", type: "text", required: true, rest: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      let msg = args.join(" ").trim();
      if (!msg.length) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        { users } = trollegle;

      msg = trollegle.checkMessage(user, msg);

      const tag = dict.format.tags.gossip;
      for (const u of users.admins(user, true, true).values())
        u.specialUserMessage(u, msg, tag);
    },
  }
);
