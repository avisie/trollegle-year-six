import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["crash", "killandsave"],
    description: "Makes the room die hardER.",
    dict: {},
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute() {
      setTimeout(() => {
        Promise.reject("Thank you for using trollegle-year-six!");
      });
    },
  }
);
