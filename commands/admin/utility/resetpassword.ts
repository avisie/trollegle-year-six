import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["resetpassword", "resetpwd", "rspwd"],
    description: "Reset the password of an account in case of lacking memory.",
    dict: {
      usage: "Usage: /resetpassword USER",
      notfound: "Account not found.",
      modifying: "This account is currently being modified. Try again later.",
      processing: "The password is being changed. Please wait.",
      error: "An error has occurred changing the account's password.",
      success: "Successfully set account password for {0} to {1}",
    },
    args: [{ name: "USER", type: "text", required: true }],
  },
  {
    sensitive: true,
    permitted: true,
    async execute(cmd, user, args) {
      const username = (args.shift() ?? "").trim();
      if (!username) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        { accounts } = trollegle,
        userlow = username.toLowerCase(),
        account = accounts.lookup(userlow);
      if (!account) return user.serverMessage(cmd.dict.notfound);

      if (accounts.modifying.has(userlow))
        return user.serverMessage(cmd.dict.modifying);
      accounts.modifying.add(userlow);

      user.serverMessage(cmd.dict.processing);

      const password = accounts.generatePassword(16),
        pash = await accounts.hash(password);
      if (!accounts.changepwd(userlow, pash)) { 
        accounts.modifying.delete(userlow);
        return user.serverMessage(cmd.dict.error);
      }

      accounts.modifying.delete(userlow);

      user.serverMessage(fstring(cmd.dict.success, account.user, password));
    },
  }
);
