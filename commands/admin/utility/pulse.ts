import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["pulse"],
    description: "Makes the room pulse.",
    dict: {
      success: "Attempting to pulse...",
    },
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user) {
      const { trollegle } = user;
      if (!trollegle.hasSource("omegle"))
        throw new Error("omegle source not found wtf");

      trollegle.addUser({ sourceid: "omegle", pulse: true });
      user.serverMessage(cmd.dict.success);
    },
  }
);
