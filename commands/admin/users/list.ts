import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandInitializer(
  {
    names: ["list", "l"],
    description: "Shows you the list of users in the room (ong fr).",
    dict: {},
    args: [],
  },
  {
    sensitive: false,
    permitted: true,
    execute(_, user) {
      const { users } = user.trollegle,
        connected = users.connected().map((u) => u.id),
        waiting = users.waiting().map((u) => u.id),
        n = users.list.size;
      return user.serverMessage(
        `There ${n === 1 ? `is ${n} user` : `are ${n} users`} in the room.\n` +
          `${connected.length} connected: ${connected.join(", ")}\n` +
          `${waiting.length} waiting: ${waiting.join(", ")}`
      );
    },
  }
);
