import { CommandSubcategoryInitializer } from "../../../lib/commandhandler/mod.ts";

export default new CommandSubcategoryInitializer(
  {
    name: "Users",
    description: "Commands for managing little powerless users",
    dict: {},
  },
  {
    permitted: true,
  }
);
