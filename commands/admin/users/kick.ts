import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["kick"],
    description: "Kicks a loser out of the chat.",
    dict: {
      usage: "Usage: /.kick USER REASON",
      nouser: "No such user - type /list to see who's online.",
      error: "Unable to kick {0}",
      success: "Kicked {0}",
    },
    args: [
      { name: "USER", type: "text", required: true },
      { name: "REASON", type: "text", required: false, rest: true },
    ],
  },
  {
    sensitive: false,
    permitted: true,
    async execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);
      let reason = args.join(" ").trim() || undefined;

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);

      if (reason) reason = user.trollegle.checkMessage(user, reason);

      if (!(await target.kick(reason)))
        return user.serverMessage(fstring(cmd.dict.error, target.nick));
      user.serverMessage(fstring(cmd.dict.success, target.nick));
    },
  }
);
