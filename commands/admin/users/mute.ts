import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["mute"],
    description: "Mutes USER, hiding their messages from normal users.",
    dict: {
      usage: "Usage: /.mute USER",
      nouser: "No such user - type /list to see who's online.",
      already: "{0} is already muted",
      success: "Muted {0}",
    },
    args: [{ name: "USER", type: "text", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);

      if (target.muted)
        return user.serverMessage(fstring(cmd.dict.already, target.nick));

      target.muted = true;
      user.serverMessage(fstring(cmd.dict.success, target.nick));
    },
  }
);
