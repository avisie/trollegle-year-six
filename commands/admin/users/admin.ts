import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { BaseDummySource } from "../../../lib/sources/types.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["admin"],
    description: "Toggles whether or not USER should be an admininstrator.",
    dict: {
      unauthorized: "Wrong password.",
      usage: "Usage: /.admin USER",
      nouser: "No such user - type /list to see who's online.",
      idiot: "You are an idiot.",
      success: "Set {0}'s administrator status to {1}",
    },
    args: [{ name: "USER", type: "text", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      if (!(user.connection instanceof BaseDummySource))
        return user.serverMessage(cmd.dict.unauthorized);

      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);
      if (target === user) return user.serverMessage(cmd.dict.idiot);

      user.serverMessage(
        fstring(
          cmd.dict.success,
          target.nick,
          (target.admin = !target.admin).toString()
        )
      );
    },
  }
);
