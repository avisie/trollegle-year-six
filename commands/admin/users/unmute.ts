import { CommandInitializer } from "../../../lib/commandhandler/mod.ts";
import { fstring } from "../../../util/string.ts";

export default new CommandInitializer(
  {
    names: ["unmute"],
    description: "Unmutes USER, allowing normal users to see their messages.",
    dict: {
      usage: "Usage: /.unmute USER",
      nouser: "No such user - type /list to see who's online.",
      not: "{0} is not muted",
      success: "Unmuted {0}",
    },
    args: [{ name: "USER", type: "text", required: true }],
  },
  {
    sensitive: false,
    permitted: true,
    execute(cmd, user, args) {
      const id = (args.shift() ?? "").trim();
      if (!id) return user.serverMessage(cmd.dict.usage);

      const { trollegle } = user,
        target = trollegle.users.resolve(id, trollegle.users.listable());
      if (!target) return user.serverMessage(cmd.dict.nouser);

      if (!target.muted)
        return user.serverMessage(fstring(cmd.dict.not, target.nick));

      target.muted = false;
      user.serverMessage(fstring(cmd.dict.success, target.nick));
    },
  }
);
