import { dict } from "../data/dict.ts";

import { fstring } from "../util/string.ts";
import { normalize, random } from "../util/array.ts";
import { formatNick } from "../util/dict.ts";
import { ongotMessage } from "./gotMessage.ts";

import { User } from "../lib/userlist/user.ts";
import { CoreEvents, Sources } from "../lib/sources/types.ts";

export const onceconnected: CoreEvents["connected"] = function (
  this: User<Sources>
) {
  const { trollegle, connection } = this;
  this.birthedat ||= Date.now();

  if (connection instanceof trollegle.sources.dummy) this.admin = true;

  if (this.decaytimeout)
    clearTimeout(this.decaytimeout), trollegle.repulse(this);

  prepareToothpaste(
    this,
    // The "identDigets" event has no purpose other than log stuff, but it's useful to check if commonLikes were found.
    // It comes after commonLikes, so if the identDigests event was received first, we assume there were no commonLikes.
    connection.waitfor("commonLikes", "identDigests").catch(() => undefined)
  );
};

export async function prepareToothpaste(
  user: User<Sources>,
  pendinglikes: Promise<string[][] | undefined>
) {
  const { trollegle, id, nick, connection } = user,
    { interests, stoppedlooking } = connection,
    rawcommonlikes = await pendinglikes,
    commonlikes = Array.isArray(rawcommonlikes) ? rawcommonlikes[0] : undefined;

  user.joinedat = Date.now();

  trollegle.broadServerAdmins(
    `${id} ${nick} connected ${
      interests && interests.length
        ? stoppedlooking
          ? commonlikes && commonlikes.length
            ? `with interests after stopping search with interests ${interests.join(
                ", "
              )}`
            : `without interests after stopping search with interests ${interests.join(
                ", "
              )}`
          : commonlikes && commonlikes.length
          ? `with common interests ${commonlikes.join(
              ", "
            )} while searching with ${interests.join(", ")}`
          : `without common interests while searching with ${interests.join(
              ", "
            )}`
        : "without interests"
    }`
  );

  if (trollegle.config.user.toothpaste) checkToothpaste(user).catch(() => {});
  else
    try {
      passToothpaste(user, true);
    } catch (e) {
      trollegle.cli.log(
        `pass toothpaste error for ${user.id} ${user.nick}: ${
          e?.stack || e?.message || e
        }`
      );
    }
}

const formatPrompt = (tries: number) =>
  fstring(dict.intro.toothpaste, dict.intro.word, tries);
export async function checkToothpaste(user: User<Sources>) {
  const order = random(dict.intro.order);
  if (!order)
    throw new Error(
      "dictionary entry for intro message order must not be empty"
    );

  const { trollegle } = user,
    tries = trollegle.config.user.toothtries,
    messages = order.map((fmt) =>
      fstring(
        fmt,
        dict.intro.separator,
        dict.intro.stumbled,
        dict.intro.flauntline,
        formatPrompt(tries)
      )
    );
  for (const msg of messages) user.serverMessage(msg);

  if (!(trollegle.users.initiallyListed(user).size > 0))
    user.serverMessage(normalize(dict.intro.empty));

  for (let i = 0; i < tries; i++) {
    const gotMessage = await user.connection
      .waitfor("gotMessage", "end")
      .catch(() => {});
    if (!gotMessage) return;
    const msg = gotMessage[0];
    user.trollegle.broadSpecialAdmins(user, msg, "unco", user);

    if (dict.regex.tail.test(msg)) return user.connection.disconnect();
    if (dict.regex.toothpaste.test(msg)) return passToothpaste(user);
    await user.serverMessage(formatPrompt(tries));
  }
  return user.connection.disconnect();
}

export function passToothpaste(user: User<Sources>, introduce = false) {
  const { trollegle } = user,
    others = trollegle.users.initiallyListed(user),
    nothers = others.size,
    separatefirst = Math.random() > 0.66,
    firstline = normalize(dict.intro.entered),
    nick = formatNick(user, user.data.dids);

  if (!(nothers - 1 > 0)) {
    if (introduce && separatefirst) user.serverMessage(firstline);
    user.serverMessage(fstring(dict.intro.first, nick));
  } else {
    const showusers = others.size > 2 && Math.random() > 2 / 5,
      countline =
        fstring(dict.intro.nothers, nothers) +
        (showusers ? dict.intro.nshow : dict.intro.nhide);
    user.serverMessage(
      !separatefirst && introduce && Math.random() > 0.5
        ? firstline + countline
        : countline
    );

    if (showusers) {
      const nicks = others.map((u) => u.nick);

      if (nothers > 5 || Math.random() > 0.5)
        user.serverMessage(dict.intro.showpad + nicks.join(", "));
      else
        for (const { nick } of others.values())
          user.serverMessage(dict.intro.showpad + nick);
    }

    user.serverMessage(fstring(dict.intro.username, nick));
  }

  user.serverMessage(
    fstring(
      dict.intro.shorthelp,
      trollegle.commands.categories.get("Main")?.prefices?.[0] || "/"
    )
  );
  for (const msg of dict.intro.extra) user.serverMessage(msg);

  user.connection.on("gotMessage", ongotMessage.bind(user));

  trollegle.informJoined(user, user);
  user.unconfirmed = false;
}
