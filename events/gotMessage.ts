import { dict } from "../data/dict.ts";
import { CoreEvents, Sources } from "../lib/sources/types.ts";
import { User } from "../lib/userlist/user.ts";

export const ongotMessage: CoreEvents["gotMessage"] = async function (
  this: User<Sources>,
  msg: string
) {
  if (this.connection.disconnecting) return;

  msg = msg.trim();

  if (this.tailcheck > 0) {
    this.tailcheck--;
    const checkmsg = msg.toLowerCase();
    if (["|"].some((str) => checkmsg.startsWith(str.toLowerCase())))
      return this.connection.disconnect();
  }

  const { trollegle } = this;
  if (trollegle.checkFlood(this)) return;

  if (await trollegle.commands.handle(this, msg)) return;

  msg = trollegle.checkMessage(this, msg);

  if (this.muted) {
    for (const u of trollegle.users.admins(this, true, true).values())
      u.specialUserMessage(this, msg, dict.format.tags.muted);
    return;
  }
  for (const u of trollegle.users
    .shouldbeabletoseemosttypesofuserandsystemmessagesithink(this, true, true)
    .values())
    u.userMessage(this, msg).catch(() => {});
};
