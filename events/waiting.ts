import { CoreEvents, Sources } from "../lib/sources/types.ts";
import { User } from "../lib/userlist/user.ts";

export const oncewaiting: CoreEvents["waiting"] = function (
  this: User<Sources>
) {
  const { mothership } = this;
  if (mothership) {
    mothership.post(
      (this.pulse = mothership.create({
        user: this,
        word: this.connection.interests![0],
        name: this.connection.interests![1],
      }))
    );
    this.decaytimeout = mothership.createDecayTimeout(this);
  }
};
