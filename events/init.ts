import { names } from "../data/names.ts";
import { words } from "../data/words.ts";

import { oncewaiting } from "./waiting.ts";
import { onceconnected } from "./connected.ts";
import { onceend } from "./end.ts";
import { random } from "../util/array.ts";

import { UserInitializer } from "../lib/trollegle/mod.ts";
import { BaseDummySource, BaseProxiedSource } from "../lib/sources/types.ts";
import { TrollegleSources } from "../trollegle.ts";
import { genrid } from "../lib/sources/omegle.ts";

export const userinit: UserInitializer<TrollegleSources> = async function (
  trollegle,
  user
) {
  let interests: string[] | undefined;
  if (user.mothership) {
    const word = random(words)!,
      name = random(names)!;

    if (!word)
      throw new Error(
        `got no word for pulse interests for ${user.id} ${user.nick}`
      );
    if (!name)
      throw new Error(
        `got no name for pulse interests for ${user.id} ${user.nick}`
      );

    interests = [word, name];
  } else interests = user.trollegle.config.pulse.defaultinterests || undefined;

  const conn = user.connection;
  if ((conn as typeof user.connection) instanceof BaseDummySource)
    user.birthedat = trollegle.state.data.adminbirthedat;

  conn.once("waiting", oncewaiting.bind(user));
  conn.once("connected", onceconnected.bind(user));
  conn.once("end", onceend.bind(user));

  if (!(conn instanceof BaseProxiedSource))
    return await conn.connect({
      interests,
    });

  let success = false;
  for (const proxy of trollegle.proxies) {
    if (!proxy.identifier && !(await proxy.check(conn.constructor))) continue;
    proxy.use(conn);

    if (
      await conn
        .connect({
          interests,
          proxy: proxy.url,
          client: proxy.client,
          randid: genrid(),
        })
        .catch(() => {})
    ) {
      conn.once("end", () => proxy.end(conn));
      success = true;
      break;
    } else proxy.end(conn);
  }

  if (!success)
    return trollegle.broadServerAdmins(
      `Unable to start connection for ${user.id} ${user.nick} (no working proxies)`
    );

  trollegle.broadServerAdmins(
    `Started ${conn.constructor.name} connection for ${user.id} ${user.nick}`
  );
};
