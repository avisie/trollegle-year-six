import { dict } from "../data/dict.ts";
import { Dummy } from "../lib/sources/dummy.ts";
import { CoreEvents, Sources } from "../lib/sources/types.ts";
import { User } from "../lib/userlist/user.ts";

export const onceend: CoreEvents["end"] = function (
  this: User<Sources>,
  reason
) {
  const { trollegle, id, nick, connection } = this,
    { users, accounts } = trollegle;

  users.remove(this);
  connection.off();

  switch (reason) {
    case "error":
    case "died":
      this.leavereason = dict.reasons[reason];
  }
  if (!this.unconfirmed) trollegle.informLeft(this, this);

  trollegle.broadServerAdmins(
    `${id} ${nick} disconnected with reason '${reason}'`
  );

  for (const user of users.list.values()) user.ignores.delete(this);

  users.nicks.free(this.nick);
  if (this.account) accounts.used.delete(this.account.userlow);

  if (this.mothership && !connection.connected) {
    clearTimeout(this.decaytimeout);
    trollegle.repulse(this);
  }

  if (connection instanceof Dummy)
    trollegle.addUser({ sourceid: "dummy", pulse: false });
};
