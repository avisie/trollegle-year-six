Trollegle is a chat server which recruits users from text-mode Omegle, a one-on-one text chat between random, anonymous users.

Historically "Trollegle" was a man-in-the-middle interception tool for text-mode Omegle, but evolved into a group chat server.
Welcome to Trollegle.

![TROLLEGLE](uploads/TROLLEGLE.png)

(I didn't make a transparent version for some reason.)

This repository is intended as a potential alternative to [@jtrygva/trollegle](https://gitlab.com/jtrygva/trollegle), which is a Trollegle server written in Java.

This server is written in TypeScript with a focus on expandability and stability, all while providing a solid boilerplate room with a lot of features to start out with.

## Installation

Clone the project using

```
git clone https://gitlab.com/truewulfey/trollegle-year-six.git
```

Install the [Deno](https://deno.land/) runtime.
It's a cool alternative to Node.JS which supports TypeScript out of the box and makes the development experience less awful.

**Some configuration is required.**

**In `config.ts`**, you will find the following fields:

```ts
{
  account: {
    password: {
      salt: "",
      iterations: 0,
      bitlength: 0
    }
  }
}
```

This is account password security configuration.
Ideally, you should not be able to reverse passwords, even if you have access to the account database file (`accounts.db` by default).

The `salt` string should have at least 32 hexadecimal characters: numbers from 0 to 9 and letters from a to f, inclusive.
This will be used as the cryptographic salt for hashing account passwords.
Just put some random things here, as long as they fit the hexadecimal character criteria.
**It is important not to share these numbers with anyone else other than yourself**.

The `iterations` field should be set to a number greater than 150000, as it defines how many "iterations" the hashing algorithm will perform per hashing operation.
The higher the better, but higher values mean it will use more computing power to create a password hash, which means `/login` and `/register` commands take a few seconds to fully execute.
**It would be preferred not to share this value too**.
Instead of 150000, you can set it to something not rounded like 151243.

If you have no idea what or why this is, just set it to 150000.

Lastly, the `bitlength` field should be a multiple of 8 and at least 512.
This will impact how long the password hashes are in text form.

**In `trollegle.ts`**, at the bottom you will find:

```ts
  mothership: {
    sourceid: "omegle",
    url: "https://bellawhiskey.ca/trollegle",
    chatdab: null,
  }
```

Leave `sourceid` at `"omegle"` unless you want to expand your chat room to other chat services.

If you for some reason don't want to use `bellawhiskey` as your mothership (not that there are others, but still), you can change the `url` field.
Make sure it does not contain a slash at the end, and if you are using another mothership, enter `/lib/mothership/mod.ts` and change the pulse setting/deleting logic.

For `bellawhiskey`, the `chatdab` field defines what your room name will be.
It cannot be just any name you want, so you have to replace `null` with a 12-or-so decimal number, like `0.123456789012` (but please do not use the example value).
**DO NOT SHARE THIS VALUE AS WELL.**
Someone who has your `chatdab` is someone who can impersonate your room, take your pulses down or intercept users' connections!

Lastly, you should add HTTP proxies in `/util/proxyurls.ts`.
This file, by default, looks like:

```ts
export const proxyurls = [
  "username:password@127.0.0.1:8080",
  "username@0.0.0.0:420",
  "192.168.1.108:69",
  "...",
].map((str) => new URL(`http://${str}`));
```

To add proxies, simply **delete all of the default values** (which just show you the formats you can use and are **NOT** valid proxies) and replace them with your proxies.

By default, these proxies are treated as HTTP proxies.
To use HTTPS proxies, replace the `http://` part with `https://`.
I'm not sure if SOCKS proxies are supported, probably not.

Oh, and make sure you remove the `"..."` at the end of the array.

## Running

Sorry for the text walls, I will try to make this more readable in the future.

Once you have your configuration set up, run:

```
deno task trollegle
```

If you see a `You:`, and typing `/list` doesn't crash the room (it's very obvious when the room crashes, don't worry about that), you are good to go!

Your next step would be running `/.pulse`, which enables room pulsing.
Pulsing can _not_ be disabled afterwards.

To close your chat and disconnect the users, simply type `/.massacre`.

To close your chat but save users' connections to a file (`state.json` by default), type `/.crash`.
The next time you start the chat up, it will try to load users' Omegle connections again.
This will work as long as you restart within 2 minutes of shutting down.

If you use `CTRL + C` to close the room, the `state.json` file will be unmodified.
This means that next time you start the chat, it will read the file as-is and try to bring back users whose connections are sure to be gone.
**This may cause issues like rolling back certain user data**, like the /pat counts of users that were logged in.

## Modification, possibilities, ideas

To make changes to the code, if you use `Visual Studio Code`, install the `Deno` extension to help you with type checking and such.
The code is formatted using the `Prettier` extension.

The `Deno` extension for VSCode is really, really helpful here, as it can show you type errors before runtime (hence not a lot of type checking/validation is performed during runtime - if types don't match properly, it's not going to work either way).

To add your own commands or change existing ones, try to explore the `commands` directory, the subdirectory ("categories"), the commands in the category (files with the `.ts` extension), then the next subdirectories ("subcategories") and more commands in those subcategories.

Adding more action commands like `/pat`s is very straightforward, taking a look at the command file for `/pat` should make obvious what should be replaced (hint: replacing ".pat" with anything else, like ".hug" does 60% of the job).
Adding action commands requires modification of the `UserData` types, including properly modifying the `defaultUserData` constant, aaaaand making sure that the validation part of `isUserData()` and `StateKeeper.validate` won't mind the new actions.
It's best to first explore how commands are implemented before performing any work, just to prevent missing something.

This version of the Trollegle chat server theoretically supports expansion onto more "sources" other than Omegle - those could be other one-on-one text chat websites, shenanigans involving other chat services, robots and whatever it is that may be adapted to act like a "CoreEvents" compliant source.

It's also possible to create a chat "hotel", by connecting multiple instances of `Trollegle` classes and allowing users to choose which one to connect to.

These are just some ideas which may inspire someone to think outside of the box in never-seen-before-on-Trollegle ways.

Good luck. You will really need it until I write good documentation.

## Notes

This version of Trollegle lacks a good amount of admin commands included in @jtrygva/trollegle and the @TruelyThea/TrollegleDayTwo addon.

Since this project is still for the most part unfinished, various parts of the code do not make sense at all (eg. `config.json` is the config file but `trollegle.ts` contains mothership configuration and alike quirks).
Commands are stupid as well eg. every command has a `description` field which is never used, `args` fields which aren't used, the /help command is hard-coded and such.

All this mostly means issues regarding code architecture **should not be posted** on the Issues tab, until this text is removed - which is a silly way to say ".

However, design or functional suggestions are always welcome either on the Issues tab as enhancements or on my Discord, `truewulfey`.

Thank you for reading through and, thank you for using `trollegle-year-six`.
