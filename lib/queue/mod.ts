export type Queuable = (...args: any[]) => any;

export class Queue {
  size = 0;
  #pending: ReturnType<Queuable> = Promise.resolve();

  async process<F extends Queuable>(
    fun: F,
    args: Parameters<F>
  ): Promise<ReturnType<F>> {
    try {
      await this.#pending;
    } catch (_) {
      // We ignore errors, because they should be handled elsewhere.
    }

    this.size--;
    return fun(...args);
  }

  async add<F extends Queuable>(
    fun: F,
    ...args: Parameters<F>
  ): Promise<Awaited<ReturnType<F>>> {
    this.size++;
    return await (this.#pending = this.process(fun, args));
  }
}
