import { isRecord } from "../../util/object.ts";
import {
  BaseProxiedSource,
  ProxyConnectOptions,
  EndReason,
  Identifiers,
  Interests,
  RawEvent,
} from "./types.ts";

class OmegleError extends Error {
  /**
   * The error code for internal handling
   */
  code?: string;

  constructor(
    message: string,
    { code, ...options }: ErrorOptions & { code?: string }
  ) {
    super(message, options);
    if (code) this.code = code;
  }
}

const errors = {
  NoServer: new OmegleError("Haven't selected an Omegle server yet at all", {
    code: "noserver",
  }),
  NotEstablished: new OmegleError("Connection to Omegle not established", {
    code: "notestablished",
  }),
  NotConnected: new OmegleError("Not connected to a Stranger", {
    code: "notconnected",
  }),
} satisfies Record<string, OmegleError>;

/**
 * Return a random element from an ArrayLike
 * @param thing The ArrayLike to get a random element from
 * @returns A random element from the ArrayLike
 */
function random<T>(thing: ArrayLike<T>): T {
  return thing[Math.floor(Math.random() * thing.length)];
}

/**
 * Delete an element from an array if it exists.
 * @param array The array to delete the element from.
 * @param element The element to delete.
 */
function del<T>(array: T[], element: any): T[] {
  const i = array.indexOf(element);
  if (!(i < 0)) array.splice(i, 1).shift();
  return array;
}

const ridcharset = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "J",
  "K",
  "L",
  "M",
  "N",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
] as const;

/**
 * Generate a random randID string for starting Omegle connections
 * @returns The generated randID
 */
export function genrid(): string {
  let randid = "";
  for (let i = 0; i++ < 8; randid += random(ridcharset));
  return randid;
}

const sleep = (t: number) => new Promise<void>((r) => setTimeout(() => r(), t));

export type OmegleRequestOptions = {
  server: string | false;
  path: string;
  method?: string;
  data?: Record<string, string>;
  qs?: boolean;
  head?: Record<string, string>;
  oneattempt?: boolean;
  noreset?: boolean;
  attempts?: number;
  useclient: boolean;
};

export type OmegleResurrectData = {
  lang: string | null;
  server: string | null;
  proxy: string | null;
  clientid: string | null;
  identifiers: Identifiers | null;
  interests: Interests | null;
  commonlikes: Interests | null;
  lastevent: RawEvent | null;
  lastactive: number | null;
  stoppedlooking: boolean;
  connected: boolean;
  servertypingstate: boolean;
};

export type StrangerName = "Stranger !" | "Stranger 2";

export type OmegleEvents = {
  error: (message: string) => any;
  serverError: (message: string) => any;
  antinudeBanned: () => any;
  connecting: () => any;
  waiting: () => any;
  connected: () => any;
  strangerConnected: (who: string) => any;
  identDigests: (digests: string) => any;
  commonLikes: (likes: string[]) => any;
  typing: () => any;
  stoppedTyping: () => any;
  spyTyping: (which: StrangerName) => any;
  spyStoppedTyping: (which: StrangerName) => any;
  spyMessage: (which: StrangerName, message: string) => any;
  gotMessage: (message: string) => any;
  serverMessage: (message: string) => any;
  question: (question: string) => any;
  recaptchaRequired: (sitekey: string) => any;
  recaptchaRejected: (sitekey: string) => any;
  strangerDisconnected: () => any;
  spyDisconnected: () => any;
  disconnected: () => any;
  end: (reason: EndReason) => any;
  connectionDied: () => any;
  statusInfo: (json: string) => any;
  icecandidate: (ice: string) => any;
  rtccall: () => any;
  rtcpeerdescription: (desc: string) => any;
  partnerCollege: (college: string) => any;
  debug: (...args: any[]) => any;
};
export type OmegleSourceEvents = OmegleEvents & {
  event: (name: keyof OmegleEvents, ...args: any[]) => any;
};

export interface Omegle
  extends BaseProxiedSource<"omegle", OmegleSourceEvents, OmegleResurrectData> {
  constructor: typeof Omegle;
}
export class Omegle extends BaseProxiedSource<
  "omegle",
  OmegleSourceEvents,
  OmegleResurrectData
> {
  static readonly id = "omegle";
  static isValidResurrectData(obj: unknown): obj is OmegleResurrectData {
    if (!isRecord(obj)) return false;

    const {
      lang,
      server,
      proxy,
      clientid,
      identifiers,
      interests,
      commonlikes,
      lastevent,
      lastactive,
      stoppedlooking,
      connected,
      servertypingstate,
    } = obj;

    if (
      !(
        (typeof lang === "string" || lang === null) &&
        (typeof server === "string" || server === null) &&
        (typeof proxy === "string" || proxy === null) &&
        (typeof clientid === "string" || clientid === null) &&
        (Array.isArray(identifiers) || identifiers === null) &&
        (Array.isArray(interests) || interests === null) &&
        (Array.isArray(commonlikes) || commonlikes === null) &&
        (Array.isArray(lastevent) || lastevent === null) &&
        (typeof lastactive === "number" || lastactive === null) &&
        typeof stoppedlooking === "boolean" &&
        typeof connected === "boolean" &&
        typeof servertypingstate === "boolean"
      )
    )
      return false;

    if (
      identifiers &&
      !(
        identifiers.length === 4 &&
        identifiers.findIndex((e) => typeof e !== "string") < 0
      )
    )
      return false;

    if (commonlikes && !interests) return false;

    if (interests && !(interests.findIndex((e) => typeof e !== "string") < 0))
      return false;

    if (
      commonlikes &&
      !(
        !(commonlikes.length > interests!.length) &&
        commonlikes.findIndex((e) => typeof e !== "string") < 0
      )
    )
      return false;

    if (lastevent && !(typeof lastevent[0] === "string")) return false;

    return true;
  }

  static resurrect(options: OmegleResurrectData): Omegle {
    const {
        lang,
        server,
        proxy,
        clientid,
        identifiers,
        interests,
        commonlikes,
        lastevent,
        lastactive,
        stoppedlooking,
        connected,
        servertypingstate,
      } = options,
      om = new this();

    if (proxy) {
      const url = new URL(proxy),
        { protocol, host, username, password } = url,
        clientoptions: Deno.CreateHttpClientOptions = {
          proxy: {
            url: `${protocol}//${host}`,
          },
        };

      if (username)
        clientoptions.proxy!.basicAuth = {
          username: username,
          password: password,
        };

      om.proxy = url;
      om.httpclient = Deno.createHttpClient(clientoptions);
    }

    if (lang !== null) om.lang = lang;
    if (clientid !== null) om.clientid = clientid;
    if (identifiers !== null) om.identifiers = identifiers;
    if (interests !== null) om.interests = interests;
    if (commonlikes !== null) om.commonlikes = commonlikes;
    if (lastevent !== null) om.lastevent = lastevent;
    if (lastactive !== null) om.lastactive = lastactive;
    om.stoppedlooking = stoppedlooking;
    om.connected = connected;
    om.servertypingstate = servertypingstate;

    for (const [name, listener] of Object.entries(om.defaultevents))
      om.on(name as keyof OmegleEvents, listener, true);

    if (server !== null) {
      om.server = server;
      om.established = true;
      om.getevents();
    }
    return om;
  }

  serialize(): OmegleResurrectData {
    return {
      lang: this.lang ?? null,
      server: this.server ?? null,
      proxy: this.proxy ? this.proxy.href : null,
      clientid: this.clientid ?? null,
      identifiers: this.identifiers ?? null,
      interests: this.interests ?? null,
      commonlikes: this.commonlikes ?? null,
      lastevent: this.lastevent ?? null,
      lastactive: this.lastactive ?? null,
      stoppedlooking: this.stoppedlooking ?? null,
      connected: this.connected ?? null,
      servertypingstate: this.servertypingstate ?? null,
    };
  }

  static readonly eventthreads = 2;
  static readonly banevents: (keyof OmegleEvents)[] = [
    "error",
    "antinudeBanned",
  ];

  /**
   * The protocol to use for making api requests.
   * HTTP is faster by a dozen milliseconds, but probably less "secure".
   */
  readonly protocol: "http" | "https" = "https";

  /**
   * The home domain, of Omegle.
   * Can be changed for testing, i suppose.
   */
  readonly url = "omegle.com";

  /**
   * A language code for narrowing Strangers.
   * Omegle feature, apparently.
   */
  lang?: string;
  /**
   * The HttpClient to use for proxied requests.
   */
  declare httpclient;
  /**
   * The chosen Omegle server url.
   * Different servers are cool sometimes.
   */
  server?: string;
  /**
   * The chosen Omegle antinude server url.
   * As if we didn't have enough to do to connect already.
   */
  antinudeserver?: string;

  /**
   * An optional random string for starting the Omegle connection.
   * Using a different randid allows the client to connect to the same people.
   * Changing the randid changes the client's randid identifier.
   */
  randid?: string;
  /**
   * The session id used for authorization.
   */
  clientid?: string;
  /**
   * The "check code" Omegle requires for starting a chat.
   */
  checkcode?: string;

  /**
   * Whether or not the connection to an Omegle server had been made.
   */
  declare established;

  /**
   * Whether the connection stopped looking for Strangers with common interests or not.
   */
  declare stoppedlooking;

  /**
   * Whether a Stranger has been found or not.
   */
  declare connected;

  /**
   * Whether or not the connection is expecting to disconnect.
   */
  declare disconnecting;

  /**
   * An array of string keys for generating image logs. Not implemented.
   */
  declare identifiers?: Identifiers;
  /**
   * The interests used to narrow down stranger matching.
   *
   * Set after `start`ing the connection, if provided.
   */
  declare interests?: Interests;
  /**
   * If provided upon starting, the interests that matched with the Stranger's.
   */
  declare commonlikes?: Interests;
  /**
   * Is the Stranger currently typing?
   */
  declare strangertyping;

  /**
   * Does the client expect the Stranger to be able to see it typing right now?
   */
  declare typing;
  /**
   * Can the Stranger see the client typing right now?
   */
  declare servertypingstate;

  /**
   * The last emitted event with its data.
   */
  declare lastevent?: RawEvent;

  /**
   * The last time an event was received.
   */
  declare lastactive?: number;

  /**
   * Events to listen to by default, required for session handling.
   */
  readonly defaultevents: Partial<OmegleEvents> = {
    error: () => ((this.disconnecting = true), this.reset("banned")),
    antinudeBanned: () => ((this.disconnecting = true), this.reset("banned")),
    connected: () => (this.connected = true),
    identDigests: (digests) =>
      digests && (this.identifiers = digests.split(",") as Identifiers),
    commonLikes: (likes) => (this.commonlikes = likes),
    typing: () => (this.strangertyping = true),
    stoppedTyping: () => (this.strangertyping = false),
    gotMessage: () => (this.strangertyping = false),
    strangerDisconnected: () => (
      (this.disconnecting = true), this.reset("stranger")
    ),
    spyDisconnected: () => (
      (this.disconnecting = true), this.reset("stranger")
    ),
    connectionDied: () => this.reset("died"),
  };

  /**
   * An asynchronous operation queue to keep things ordered.
   */
  declare opqueue;

  /**
   * Make an Omegle API request.
   * @param opts Options for the request.
   * @returns The JSON API response.
   */
  private actualrequest = async function (
    this: Omegle,
    {
      server,
      path,
      method = "POST",
      data,
      qs,
      head,
      useclient,
      oneattempt,
      noreset,
      attempts = 3,
    }: OmegleRequestOptions
  ): Promise<string | undefined> {
    const url = new URL(path, `${this.protocol}://` + (server || this.url)),
      headers = new Headers(
        Object.assign(
          {
            "content-type": "application/x-www-form-urlencoded",
            "user-agent":
              "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
          },
          head
        )
      ),
      options: RequestInit & { client?: Deno.HttpClient } = {
        method,
        headers,
      },
      params = new URLSearchParams(data).toString();

    if (useclient) options.client = this.httpclient;
    if (data)
      if (qs) url.search = params;
      else options.body = params.toString();

    if (oneattempt) attempts = 0;
    for (let i = 0; i <= attempts; i++) {
      if (i === attempts) return void (noreset || this.reset("error"));
      if (i > 0 && !noreset) await sleep(5000);
      // if (i > 0) this.server = await this.getserver();

      const res = await fetch(url, options).catch(() => {});
      if (typeof res === "undefined" || res.status < 200 || res.status > 299)
        continue;
      const body = await res.text().catch(() => {});
      if (body === undefined) continue;
      return body;
    }
  }.bind(this);

  /**
   * Make an Omegle API request.
   * @param opts Options for the request.
   * @returns The JSON API response.
   */
  private request(opts: OmegleRequestOptions): Promise<string | undefined> {
    return this.opqueue.add(this.actualrequest, opts);
  }

  /**
   * Check available servers and pick one. Randomly.
   * @returns The chosen server.
   */
  private async getservers(attempts = 3): Promise<boolean> {
    const body = await this.request({
      server: false,
      path: "status",
      method: "GET",
      useclient: false,
    });
    if (typeof body === "undefined" || !attempts) return false;

    let servers: string[], antinudeservers: string[];
    try {
      const data = JSON.parse(body);
      if (!Object.hasOwn(data, "servers") || !Array.isArray(data.servers))
        throw new Error("status servers prop not an array");
      if (!Array.isArray(data.antinudeservers))
        throw new Error("status antinudeservers prop not an array");

      servers = (data.servers as string[]).map((str) => str + ".omegle.com");
      antinudeservers = data.antinudeservers;
    } catch (e) {
      console.log(body, e);
      await sleep(5000);
      return await this.getservers(attempts - 1);
    }

    this.server = random(del(servers, this.server));
    this.antinudeserver = random(del(antinudeservers, this.antinudeserver));

    return true;
  }

  /**
   * Get a "check code" from one of Omegle's antinude servers required for connecting.
   * @returns The check code Omegle's antinude servers return.
   */
  async getcheckcode(): Promise<string | undefined> {
    const { antinudeserver } = this;
    if (typeof antinudeserver === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });

    return await this.request({
      server: antinudeserver,
      path: "check",
      useclient: false,
    });
  }

  /**
   * Start an Omegle session.
   * @param options Options for the connection.
   * @returns The session id if successful.
   */
  async connect(options: ProxyConnectOptions): Promise<boolean | undefined> {
    const { interests, lang, randid, proxy, client } = options;
    if (proxy) {
      if (!client) throw new Error("requires proxy url and client");
      this.proxy = proxy;
      this.httpclient = client;
    } else if (client) throw new Error("received client but no proxy");

    const server = await this.getservers();
    if (!server) return;

    const checkcode = await this.getcheckcode();
    if (!checkcode) return;

    this.interests = interests;

    const data: OmegleRequestOptions["data"] = {
      topics: interests && interests.length ? JSON.stringify(interests) : "",
      randid: (this.randid = typeof randid === "string" ? randid : ""),
      firstevents: "0",
      caps: "t3",
      m: "1",
      cc: checkcode,
    };

    if (typeof lang === "string") data.lang = this.lang = lang;

    const body = await this.request({
      server: this.server!,
      path: "start",
      data,
      qs: true,
      useclient: true,
    });
    if (typeof body === "undefined") return;

    const json = JSON.parse(body || "{}"),
      clientid = json.clientID;

    if (!clientid) return false;

    for (const [name, listener] of Object.entries(this.defaultevents))
      this.on(name as keyof OmegleEvents, listener, true);
    const events: [keyof OmegleEvents, ...any[]] = json.events;

    this.established = true;
    this.clientid = clientid;

    if (events.some(([name]) => Omegle.banevents.includes(name))) return false;
    await this.emit("connecting");

    if (events.length) this.emitevents(events);

    this.getevents();
    return true;
  }

  /**
   * Fetch connection events.
   * @returns The fetched events.
   */
  private async getevents(attempts = 3): Promise<RawEvent[] | undefined> {
    const { clientid, server } = this;
    if (typeof server === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });
    if (!clientid) throw new Error(undefined, { cause: errors.NotEstablished });

    const body = await this.actualrequest({
      server,
      path: "events",
      data: {
        id: clientid,
      },
      useclient: false,
    });
    if (typeof body === "undefined" || !attempts)
      return void this.reset("error");

    let events: any;
    try {
      events = JSON.parse(body);
    } catch (e) {
      console.error(
        `invalid events json '${body}', saved the room's life: ${
          (e?.stack ?? e?.message) || e.toString()
        }`
      );
      events = [];
      await sleep(5000);
      return await this.getevents(attempts - 1);
    }

    if (events) {
      await this.emitevents(events);
      if (!this.disconnecting && this.established) this.getevents();
    } else if (!this.disconnecting) await this.emitevents([["connectionDied"]]);
    return events;
  }

  /**
   * Emit a whole bunch of events, one after the other.
   * @param events A bunch of events to emit.
   */
  async emitevents(events: RawEvent[]) {
    for (const [name, ...args] of events) {
      this.lastactive = Date.now();
      this.lastevent = [name, ...args] as RawEvent;
      await this.emit("event", name as keyof OmegleEvents, ...args);
      await this.emit(name as keyof OmegleEvents, ...args);
    }
  }

  /**
   * Send a message to the Stranger.
   * @param message The message to send.
   * @returns Whether or not the message was successfully sent.
   */
  async send(message: string, oneattempt = false): Promise<boolean> {
    const { clientid, connected, server } = this;
    if (typeof server === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });
    if (!connected) throw new Error(undefined, { cause: errors.NotConnected });
    this.typing &&= false;

    const body = await this.request({
      server,
      path: "send",
      data: {
        id: clientid!,
        msg: message,
      },
      oneattempt,
      noreset: true,
      useclient: true,
    });

    if (body !== "win") return false;
    this.servertypingstate &&= false;

    return true;
  }

  /**
   * Set whether or not Omegle should tell the Stranger that the client is typing.
   * @param state Either true (yes) or false (no).
   * @returns Whether or not updating the state was successful or `undefined` if unchanged.
   */
  async settyping(state: boolean): Promise<boolean | undefined> {
    const { clientid, connected, typing, server } = this;
    if (typeof server === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });
    if (!connected) throw new Error(undefined, { cause: errors.NotConnected });

    if (state === typing) return undefined;
    this.typing = state;

    const body = await this.request({
      server,
      path: state ? "typing" : "stoppedtyping",
      data: {
        id: clientid!,
      },
      useclient: true,
    });

    if (body !== "win") return false;
    this.servertypingstate = state;

    return true;
  }

  /**
   * Disconnect the current connection.
   * @param graceful If the thing should wait for a response before resetting the connection.
   * @returns Whether the operation was successful or not.
   */
  async disconnect(graceful?: true): Promise<boolean | undefined> {
    const { clientid, server } = this;
    if (typeof server === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });
    if (!clientid) throw new Error(undefined, { cause: errors.NotEstablished });

    this.disconnecting = true;
    const body = await this.request({
      server,
      path: "disconnect",
      data: {
        id: clientid,
      },
      useclient: true,
    });

    graceful ? await this.emit("disconnected") : this.emit("disconnected");
    this.reset("client");

    if (typeof body === "undefined") return;
    if (body !== "win") return false;

    return true;
  }

  /**
   * Stop trying to match with common interests, if formerly specified.
   * @returns Whether stoppage was successful or not.
   */
  async slfcl(): Promise<boolean | undefined> {
    const { clientid, interests, lastevent, server } = this;
    if (typeof server === "undefined")
      throw new Error(undefined, { cause: errors.NoServer });
    if (!clientid || !interests || lastevent?.[0] !== "waiting")
      throw new Error(undefined, { cause: errors.NotEstablished });

    const body = await this.request({
      server,
      path: "stoplookingforcommonlikes",
      data: {
        id: clientid,
      },
      useclient: true,
    });
    if (typeof body === "undefined") return;

    const success = body === "win";
    if (success) this.stoppedlooking = true;
    return success;
  }

  /**
   * Reset properties to defaults. Invoked usually after a connection is over.
   * @returns Nothing.
   */
  reset(reason: EndReason): void {
    this.emit("end", reason);

    for (const [name, listener] of Object.entries(this.defaultevents))
      this.off(name as keyof OmegleEvents, listener);
  }
}
