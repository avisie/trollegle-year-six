import { CLI, CLIEvent } from "../cli/mod.ts";
import {
  CoreEvents,
  BaseDummySource,
  EndReason,
  CoreSourceEvents,
  ConnectOptions,
} from "./types.ts";

import { cli } from "../../trollegle.ts";
import { join, resolve } from "std/path/mod.ts";

const logpath = resolve("./logs");
Deno.mkdirSync(logpath, { recursive: true });

const file = Deno.createSync(join(logpath, `${Date.now()}.txt`)),
  encoder = new TextEncoder();

export const log = (cli: CLI, str: string, print = true): void => (
  print && cli.log(str), void file.writeSync(encoder.encode(str + "\n"))
);

const uesc = /\\u([0-9A-Fa-f]{4})/g;
function parseunicode(input: string): string {
  return input.replace(uesc, (_, g) => String.fromCharCode(parseInt(g, 16)));
}

export type DummyResurrectOptions = Record<never, never>;

export interface Dummy
  extends BaseDummySource<"dummy", CoreEvents, DummyResurrectOptions> {
  constructor: typeof Dummy;
}
export class Dummy extends BaseDummySource<
  "dummy",
  CoreEvents,
  DummyResurrectOptions
> {
  static readonly id = "dummy";
  static isValidResurrectData(_: unknown): _ is DummyResurrectOptions {
    return true;
  }
  static resurrect(_: DummyResurrectOptions): Dummy {
    const om = new Dummy();
    om.connect({});
    return om;
  }

  serialize(): DummyResurrectOptions {
    return {};
  }

  /**
   * The dummy's command line thing thing you know what I mean.
   */
  cli: CLI;

  readonly defaultevents: Partial<CLIEvent> = {
    line: (line) => {
      log(this.cli, this.cli.prefix + line, false);
      line = parseunicode(line).trim();
      line === "dc"
        ? (this.emitevent("strangerDisconnected"), this.reset("stranger"))
        : this.emitevent("gotMessage", line);
    },
  };

  constructor() {
    super();
    this.cli = cli;
  }

  connect(options: ConnectOptions): Promise<true | false | undefined> {
    const { interests } = options;

    if (interests) this.commonlikes = this.interests = interests;
    this.established = true;

    this.cli.resume();

    for (const [name, listener] of Object.entries(this.defaultevents))
      this.cli.on(name as keyof CLIEvent, listener);

    if (interests) this.emitevent("waiting");

    this.emitevent("connected");
    this.connected = true;

    if (interests) this.emitevent("commonLikes", interests!);
    this.emitevent("identDigests", null);

    return Promise.resolve(true);
  }

  emitevent<K extends keyof CoreSourceEvents>(
    name: K,
    ...args: Parameters<CoreSourceEvents[K]>
  ): void {
    this.lastevent = [name, ...args];
    this.lastactive = Date.now();

    this.emit("event", name, ...args);
    this.emit(name, ...args);
  }

  slfcl(): Promise<boolean | undefined> {
    if (!this.interests || this.lastevent?.[0] !== "waiting") throw new Error();
    this.stoppedlooking = true;
    return Promise.resolve(true);
  }
  send(message: string): Promise<boolean> {
    log(this.cli, `\x1b[38;5;196mStranger:\x1b[0m ${message}`);
    return Promise.resolve(true);
  }
  settyping(_: boolean): Promise<undefined> {
    this.typing = true;
    this.servertypingstate = true;
    return Promise.resolve(undefined);
  }
  async disconnect(graceful?: boolean): Promise<true> {
    this.disconnecting = true;
    graceful ? await this.emit("disconnected") : this.emit("disconnected");
    this.reset("client");
    log(this.cli, "disconnected");
    return Promise.resolve(true);
  }

  reset(reason: EndReason): void {
    this.emitevent("end", reason);

    for (const [name, listener] of Object.entries(this.defaultevents))
      this.cli.off(name as keyof CLIEvent, listener);
  }
}
