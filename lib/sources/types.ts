import { EventEmitter } from "../eventemitter/mod.ts";
import { Queue } from "../queue/mod.ts";

export type CoreSourceEvents = {
  connecting: () => any;
  waiting: () => any;
  connected: () => any;
  identDigests: ((digests: string) => any) | ((digests: null) => any);
  commonLikes: (likes: string[]) => any;
  typing: () => any;
  stoppedTyping: () => any;
  gotMessage: (message: string) => any;
  strangerDisconnected: () => any;
  disconnected: () => any;
  end: (reason: EndReason) => any;
  debug: (...args: any[]) => any;
};

export type CoreEvents = CoreSourceEvents & {
  event: (name: keyof CoreSourceEvents, ...args: any[]) => any;
};

type Tuple<T, N extends number> = N extends N
  ? number extends N
    ? T[]
    : _TupleOf<T, N, []>
  : never;
type _TupleOf<T, N extends number, R extends unknown[]> = R["length"] extends N
  ? R
  : _TupleOf<T, N, [T, ...R]>;

export type Identifiers = Tuple<string, 4>;

export type Interests = string[];
export type RawEvent = [string, ...any[]];

export type EndReason = "error" | "banned" | "died" | "stranger" | "client";

export type Toggle<
  T,
  State extends boolean,
  Fallback = never
> = boolean extends State
  ? T | Fallback
  : State extends true & false
  ? Fallback
  : true extends State
  ? T
  : Fallback;

export type ConnectOptions = {
  interests?: Interests;
  lang?: string;
  randid?: string;
};

export type ProxyConnectOptions = ConnectOptions &
  (
    | {
        proxy: URL;
        client: Deno.HttpClient;
      }
    | { proxy: never; client: never }
  );

export type SourceID = PropertyKey;
export type SourceMap<S extends SourceID> = {
  [K in S]: BaseSourceConstructor<K, CoreEvents>;
};
export type Sources<S extends SourceID = never> = SourceMap<"dummy" | S>;

export type BaseSourceResurrectCallback<
  ID extends SourceID,
  SourceEvents extends CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> = (data: ResurrectData) => BaseSource<ID, SourceEvents, ResurrectData>;

export interface BaseSourceConstructor<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> {
  id: ID;
  isValidResurrectData: (data: unknown) => data is ResurrectData;
  resurrect: BaseSourceResurrectCallback<ID, SourceEvents, ResurrectData>;

  new (): BaseSource<ID, SourceEvents, ResurrectData>;
}

export interface BaseSource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> {
  constructor: BaseSourceConstructor<ID, SourceEvents, ResurrectData>;
}
export abstract class BaseSource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends EventEmitter<SourceEvents> {
  abstract serialize(): ResurrectData;

  established = false;
  stoppedlooking = false;
  connected = false;
  disconnecting = false;

  identifiers?: Identifiers;

  strangertyping = false;
  typing = false;
  servertypingstate = false;

  interests?: Interests;
  commonlikes?: Interests;

  lastevent?: RawEvent;
  lastactive?: number;

  opqueue: Queue = new Queue();

  abstract slfcl(): Promise<boolean | undefined>;
  abstract send(message: string): Promise<boolean>;
  abstract settyping(state: boolean): Promise<boolean | undefined>;
  abstract disconnect(graceful?: true): Promise<boolean | undefined>;
  abstract connect(options: ConnectOptions): Promise<boolean | undefined>;

  abstract reset(reason: EndReason): void;
}

export interface BaseDummySourceConstructor<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends BaseSourceConstructor<ID, SourceEvents, ResurrectData> {
  new (): BaseDummySource<ID, SourceEvents, ResurrectData>;
  id: ID;
}
export interface BaseDummySource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends BaseSource<ID, SourceEvents, ResurrectData> {
  constructor: BaseDummySourceConstructor<ID, SourceEvents, ResurrectData>;
}
export abstract class BaseDummySource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends BaseSource<ID, SourceEvents, ResurrectData> {
  /**
   * Indicates that the source is a dummy source.
   */
  static readonly dummy: true = true;
}

export interface BaseProxiedSourceConstructor<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends BaseSourceConstructor<ID, SourceEvents, ResurrectData> {
  new (): BaseProxiedSource<ID, SourceEvents, ResurrectData>;
  id: ID;
}
export interface BaseProxiedSource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> {
  constructor: BaseProxiedSourceConstructor<ID, SourceEvents, ResurrectData>;
}
export abstract class BaseProxiedSource<
  ID extends SourceID,
  SourceEvents extends CoreEvents = CoreEvents,
  ResurrectData extends Record<PropertyKey, any> = any
> extends BaseSource<ID, SourceEvents, ResurrectData> {
  /**
   * Indicates that the source requires the usage of proxies for connections.
   */
  static readonly needsproxies: true = true;

  proxy?: URL;
  httpclient?: Deno.HttpClient;

  abstract connect(options: ProxyConnectOptions): Promise<boolean | undefined>;
}
