// deno-lint-ignore-file no-explicit-any
import { EventEmitter } from "../eventemitter/mod.ts";
import * as tty from "x/tty@0.1.4/mod.ts";

const ESC = "\x1b",
  encoder = new TextEncoder(),
  decoder = new TextDecoder(),
  env = Deno.env;

export function CSI(strings: TemplateStringsArray, ...args: any[]) {
  let ret = `${ESC}[`;
  for (
    let n = 0;
    n < strings.length;
    ret += strings[n], n < args.length && (ret += args[n]), n++
  );
  return ret;
}

/**
 * Trim an array of souls until before the first sinner
 * @param bytes The selection of souls to purify
 * @returns The pure souls
 */
function clean(bytes: Uint8Array): Uint8Array {
  const result: number[] = [];
  for (const byte of bytes) {
    if (!byte) break;
    result.push(byte);
  }
  return Uint8Array.from(result);
}

/**
 * Figure out if a UTF-8 buffer needs to be extended to fit everything well or something
 * @param buffer The buffer to check
 * @returns The amount of bytes the buffer should be extended
 */
function howmanymore(buffer: Uint8Array): number {
  const l = clean(buffer).length - 1;
  let clen = 0,
    skipped = 0;

  while (clen === 0) {
    const int = buffer[l - skipped];

    let n = 0;
    for (let i = 8; i-- > 0; i)
      if ((int >> i) % 2) n++;
      else break;

    if (n === 1) skipped++;
    else clen = n || 1;
  }

  return clen - skipped - 1;
}

type Sequence = string | undefined;
type KeyInit = {
  sequence: Sequence;
  name?: string;
  ctrl: boolean;
  meta: boolean;
  shift: boolean;
  code?: string;
};

export type CLIEvent = {
  char: (char: string) => any;
  line: (line: string) => any;
  answer: (answer: string) => any;
  history: (history: string[]) => any;
  key: (key: KeyInit, sequence: Sequence) => any;
  close: () => any;
  pause: () => any;
  resume: () => any;
} & { event: (name: string, ...args: any[]) => any };

type ChangeEntry = { text: string; cursor: number };

const windows = Deno.build.os === "windows",
  rawopts = { cbreak: true };

const maxundoredostacksize = 2048,
  readbuffersize = 64,
  utf16surrogatethreshold = 2 ** 16,
  maxkillringlength = 32;
export class CLI extends EventEmitter<CLIEvent> {
  /**
   * The readable input stream
   */
  readonly input = Deno.stdin;
  /**
   * The writable output stream
   */
  readonly output = Deno.stdout;

  /**
   * Whether or not the instance is actively listening for user input
   */
  listening = false;
  /**
   * Whether or not the instance is ready to parse user input
   */
  ready = false;
  /**
   * Whether or not the instance is paused, not emitting user input
   */
  paused = false;
  /**
   * Whether or not the instance is closed
   */
  closed = false;

  /**
   * The cursor position
   */
  cursor = 0;
  /**
   * The previous cursor row position
   */
  prevrows = 0;

  /**
   * How many spaces a TAB character equals to
   */
  tabsize = 8;
  /**
   * The prompt prefix
   */
  prefix = "> ";
  /**
   * The contents of the current line
   */
  line = "";

  /**
   * Whether or not yanking is in process
   */
  yanking = false;
  /**
   * The last key pressed
   */
  previouskey?: KeyInit;
  /**
   * The string to substring search for
   */
  substringsearch: string | null = null;

  /**
   * The input history
   */
  history: string[] = [];
  /**
   * The index of the current history entry
   */
  historyindex = -1;
  /**
   * The maximum length of the input history
   */
  historysize = 250;
  /**
   * Whether or not to not include duplicate entries in the input history
   */
  removehistoryduplicates = false;

  /**
   * The list of undo entries
   */
  undostack: ChangeEntry[] = [];
  /**
   * The list of undo entries
   */
  redostack: ChangeEntry[] = [];
  /**
   * Yank killring storage array kind of thing
   */
  killring: string[] = [];
  /**
   * The current position in the killring list array thing
   */
  killringcursor = 0;

  /**
   * The timestamp in milliseconds of when the return key was last seen at
   */
  sawreturnat = 0;
  /**
   * The maximum time to wait for an LF character after a CR character to parse them as one newline
   */
  crlfdelay = 100;

  /**
   * The input key generator
   */
  readonly keygen = this.emitkeys();

  constructor({
    tabsize,
    prefix,
    paused,
  }: { tabsize?: number; prefix?: string; paused?: boolean } = {}) {
    super();
    typeof tabsize === "number" && (this.tabsize = tabsize);
    typeof prefix === "string" && (this.prefix = prefix);

    this.input.setRaw(true, windows ? undefined : rawopts);

    this.prompt();
    this.on("key", async (key, seq) => {
      if (this.paused) return;
      await this.ttywrite(seq, key);
      if (key && key.sequence) {
        // If the key.sequence is half of a surrogate pair
        // (>= 0xd800 and <= 0xdfff), refresh the line so
        // the character is displayed appropriately.
        const ch = key.sequence.codePointAt(0) || 0;
        if (ch >= 0xd800 && ch <= 0xdfff) this.refreshline();
      }
    });
    if (!paused) this.init();
    else this.paused = true;
  }

  /**
   * Initialize the input reader
   */
  private async init() {
    if (this.listening) throw new Error("already listening, please stop init");
    this.listening = true;

    const keygen = this.keygen;
    if (!this.ready) await keygen.next(), (this.ready = true);

    const emit = async (char: string) => (
      await keygen.next(char), await this.emit("char", char)
    );

    while (true) {
      if (this.paused) break;
      let bytes: Uint8Array = new Uint8Array(readbuffersize);
      await this.input.read(bytes);
      if (this.paused) break;

      const extendby = howmanymore(bytes);
      if (extendby) {
        const part = new Uint8Array(extendby);
        await this.input.read(part);

        const final = new Uint8Array(bytes.length + extendby);
        final.set(bytes);
        final.set(part, bytes.length);
        bytes = final;
      }

      const char = decoder.decode(clean(bytes));
      for (const c of char) await emit(c);
    }

    this.listening = false;
  }

  /**
   * Print some stuff to the output
   * @param args The stuff to print
   */
  log(...args: any[]) {
    const rowsize1 = Math.ceil(
      (tty.wcswidth(
        tty.stripAnsi(this.prefix + this.line.slice(0, this.cursor))
      ) +
        1) /
        this.size.columns
    );

    this.writeraw(
      new Array(rowsize1).fill(CSI`2K`).join(CSI`1A`) +
        "\r" +
        args
          .map((a) =>
            typeof a === "string" ? a : Deno.inspect(a, { colors: true })
          )
          .join(" ") +
        new Array(rowsize1).fill("\n").join("")
    );

    this.refreshline();
  }

  /**
   * Write some text to the output, raw
   * @param string The text to write
   * @returns The written text
   */
  writeraw(string: string) {
    const encoded = encoder.encode(string);
    return this.output.writeSync(encoded), decoder.decode(encoded);
  }

  /**
   * Process a key sequence with all the cool tty features
   * @param seq The sequence to process
   * @param key The current key to process
   * @returns Nothing.
   */
  async ttywrite(seq: Sequence, key: KeyInit) {
    if (this.paused) this.resume();
    key = key || {};
    this.previouskey = key;

    if (!key.meta || key.name !== "y")
      // Reset yanking state unless we are doing yank pop.
      this.yanking = false;

    // Activate or deactivate substring search.
    if (
      (key.name === "up" || key.name === "down") &&
      !key.ctrl &&
      !key.meta &&
      !key.shift
    ) {
      this.substringsearch === null &&
        (this.substringsearch = this.line.slice(0, this.cursor));
    } else if (this.substringsearch !== null) {
      this.substringsearch = null;
      // Reset the index in case there's no match.
      this.history.length === this.historyindex && (this.historyindex = -1);
    }

    // Undo
    if (
      typeof key.sequence === "string" &&
      key.sequence.codePointAt(0) === 0x1f
    ) {
      this.undo();
      return;
    }

    // Ignore escape key, fixes
    // https://github.com/nodejs/node-v0.x-archive/issues/2876.
    if (key.name === "escape") return;

    if (key.ctrl && key.shift) {
      /* Control and shift pressed */
      switch (key.name) {
        // TODO(BridgeAR): The transmitted escape sequence is `\b` and that is
        // identical to <ctrl>-h. It should have a unique escape sequence.
        case "backspace":
          this.deletelineleft();
          break;

        case "delete":
          this.deletelineright();
          break;
      }
    } else if (key.ctrl) {
      /* Control key pressed */

      switch (key.name) {
        case "c":
        case "z":
          this.input.setRaw(false);
          Deno.exit(0);
        // falls through but never actually does so

        case "h": // delete left
          this.deleteleft();
          break;

        case "d": // delete right or EOF
          this.cursor < this.line.length && this.deleteright();
          break;

        case "u": // Delete from current to start of line
          this.deletelineleft();
          break;

        case "k": // Delete from current to end of line
          this.deletelineright();
          break;

        case "a": // Go to the start of the line
          this.movecursorline(-Infinity);
          break;

        case "e": // Go to the end of the line
          this.movecursorline(+Infinity);
          break;

        case "b": // back one character
          this.movecursorline(-this.charlengthleft(this.line, this.cursor));
          break;

        case "f": // Forward one character
          this.movecursorline(+this.charlengthat(this.line, this.cursor));
          break;

        case "l": // Clear the whole screen
          this.cursorto(0, 0);
          this.clearscreendown();
          this.refreshline();
          break;

        case "n": // next history item
          this.historynext();
          break;

        case "p": // Previous history item
          this.historyprev();
          break;

        case "y": // Yank killed string
          this.yank();
          break;

        case "w": // Delete backwards to a word boundary
        // TODO(BridgeAR): The transmitted escape sequence is `\b` and that is
        // identical to <ctrl>-h. It should have a unique escape sequence.
        // Falls through
        case "backspace":
          this.deletewordleft();
          break;

        case "delete": // Delete forward to a word boundary
          this.deletewordright();
          break;

        case "left":
          this.wordleft();
          break;

        case "right":
          this.wordright();
          break;
      }
    } else if (key.meta) {
      /* Meta key pressed */

      switch (key.name) {
        case "b": // backward word
          this.wordleft();
          break;

        case "f": // forward word
          this.wordright();
          break;

        case "d": // delete forward word
        case "delete":
          this.deletewordright();
          break;

        case "backspace": // Delete backwards to a word boundary
          this.deletewordleft();
          break;

        case "y": // Doing yank pop
          this.yankpop();
          break;
      }
    } else {
      /* No modifier keys used */

      // \r bookkeeping is only relevant if a \n comes right after.
      if (this.sawreturnat && key.name !== "enter") this.sawreturnat = 0;

      switch (key.name) {
        case "return": // Carriage return, i.e. \r
          this.sawreturnat = Date.now();
          await this.resetline();
          break;

        case "enter":
          // When key interval > crlfdelay
          this.sawreturnat &&
            Date.now() - this.sawreturnat > this.crlfdelay &&
            (await this.resetline());

          this.sawreturnat = 0;
          break;

        case "backspace":
          this.deleteleft();
          break;

        case "delete":
          this.deleteright();
          break;

        case "left":
          // Obtain the code point to the left
          this.movecursorline(-this.charlengthleft(this.line, this.cursor));
          break;

        case "right":
          this.movecursorline(+this.charlengthat(this.line, this.cursor));
          break;

        case "home":
          this.movecursorline(-Infinity);
          break;

        case "end":
          this.movecursorline(+Infinity);
          break;

        case "up":
          this.historyprev();
          break;

        case "down":
          this.historynext();
          break;

        // falls through
        default:
          if (typeof seq === "string" && seq) {
            const lines = seq.split(/\r\n|\n|\r/);
            for (let i = 0, len = lines.length; i < len; i++) {
              i > 0 && (await this.resetline());
              this.insertstring(lines[i]);
            }
          }
      }
    }
  }

  /**
   * Set the prompt prefix.
   * @param prefix The new prompt prefix
   */
  setprefix(prefix: string) {
    this.prefix = prefix;
    this.prompt();
  }

  /**
   * Write the prompt prefix and allow user input
   * @param preserve Whether or not to preserve the cursor position
   */
  prompt(preserve?: boolean) {
    this.paused && this.resume();
    env.get("TERM") === "dumb"
      ? this.writeraw(this.prefix)
      : (preserve || (this.cursor = 0), this.refreshline());
  }

  /**
   * Clear the current line
   */
  clearline() {
    this.movecursorline(+Infinity);
    const pos = this.displaypos(this.prefix + this.line);
    if (pos.cols > 0 || !pos.rows) this.writeraw("\r\n");
    this.line = "";
    this.cursor = 0;
    this.prevrows = 0;
  }

  /**
   * Reset the current line and emit its contents as a "line" event
   */
  async resetline() {
    const line = this.addhistory();
    this.undostack = [];
    this.redostack = [];
    this.clearline();
    this.prompt();
    await this.emit("line", line);
  }

  /**
   * Push an undo entry to the undo stack
   * @param text The text to push
   * @param cursor The cursor position
   */
  pushtoundostack(text: string, cursor: number) {
    this.undostack.push({ text, cursor }) > maxundoredostacksize &&
      this.undostack.shift();
  }

  /**
   * Undo a text input change
   * @returns Nothing
   */
  undo() {
    if (this.undostack.length <= 0) return;

    const entry = this.undostack.pop();
    if (!entry) return;

    this.line = entry.text;
    this.cursor = entry.cursor;

    this.redostack.push(entry);
    this.refreshline();
  }

  /**
   * Redo a text input change
   * @returns Nothing
   */
  redo() {
    if (this.redostack.length <= 0) return;

    const entry = this.redostack.pop();
    if (!entry) return;

    this.line = entry.text;
    this.cursor = entry.cursor;

    this.undostack.push(entry);
    this.refreshline();
  }

  // TODO(BridgeAR): Add underscores to the search part and a red background in
  // case no match is found. This should only be the visual part and not the
  // actual line content!
  // TODO(BridgeAR): In case the substring based search is active and the end is
  // reached, show a comment how to search the history as before. E.g., using
  // <ctrl> + N. Only show this after two/three UPs or DOWNs, not on the first
  // one.
  /**
   * Restore the next line from the history array
   */
  historynext() {
    if (this.historyindex >= 0) {
      this.beforeedit(this.line, this.cursor);
      const search = this.substringsearch || "";
      let index = this.historyindex - 1;
      while (
        index >= 0 &&
        (!this.history[index].startsWith(search) ||
          this.line === this.history[index])
      )
        index--;

      index === -1 ? (this.line = search) : (this.line = this.history[index]);

      this.historyindex = index;
      this.cursor = this.line.length; // Set cursor to end of line.
      this.refreshline();
    }
  }

  /**
   * Restore the previous line from the history array
   */
  historyprev() {
    if (this.historyindex < this.history.length && this.history.length) {
      this.beforeedit(this.line, this.cursor);
      const search = this.substringsearch || "";
      let index = this.historyindex + 1;
      while (
        index < this.history.length &&
        (!this.history[index].startsWith(search) ||
          this.line === this.history[index])
      )
        index++;

      index === this.history.length
        ? (this.line = search)
        : (this.line = this.history[index]);

      this.historyindex = index;
      this.cursor = this.line.length; // Set cursor to end of line.
      this.refreshline();
    }
  }

  /**
   * Push the unedited line contents to the undo stack
   * @param text The text to push
   * @param cursor The current cursor position
   */
  beforeedit(text: string, cursor: number) {
    this.pushtoundostack(text, cursor);
  }

  /**
   * Insert a string at the cursor position
   * @param string The string to insert
   */
  insertstring(string: string) {
    this.beforeedit(this.line, this.cursor);
    if (this.cursor < this.line.length) {
      const beg = this.line.slice(0, this.cursor);
      const end = this.line.slice(this.cursor, this.line.length);
      this.line = beg + string + end;
      this.cursor += string.length;
      this.refreshline();
    } else {
      const oldPos = this.cursorpos();
      this.line += string;
      this.cursor += string.length;
      const newPos = this.cursorpos();

      oldPos.rows < newPos.rows ? this.refreshline() : this.writeraw(string);
    }
  }

  /**
   * Move the cursor to the beginning of the word to the left
   */
  wordleft() {
    if (this.cursor) {
      const length = Array.from(this.line.slice(0, this.cursor))
        .reverse()
        .join("")
        .match(/^\s*(?:[^\w\s]+|\w+)?/)?.[0]?.length;
      typeof length === "number" && this.movecursorline(-length);
    }
  }

  /**
   * Move the cursor to the end of the word to the right
   */
  wordright() {
    if (this.cursor < this.line.length) {
      const length = this.line
        .slice(this.cursor)
        .match(/^(?:\s+|[^\w\s]+|\w+)\s*/)?.[0]?.length;
      typeof length === "number" && this.movecursorline(length);
    }
  }

  /**
   * Delete the character to the left of the cursor
   */
  deleteleft() {
    if (this.cursor > 0 && this.line.length > 0) {
      this.beforeedit(this.line, this.cursor);
      // The number of UTF-16 units comprising the character to the left
      const charSize = this.charlengthleft(this.line, this.cursor);
      this.line =
        this.line.slice(0, this.cursor - charSize) +
        this.line.slice(this.cursor, this.line.length);

      this.cursor -= charSize;
      this.refreshline();
    }
  }

  /**
   * Delete the character to the right of the cursor
   */
  deleteright() {
    if (this.cursor < this.line.length) {
      this.beforeedit(this.line, this.cursor);
      // The number of UTF-16 units comprising the character to the left
      const charSize = this.charlengthat(this.line, this.cursor);
      this.line =
        this.line.slice(0, this.cursor) +
        this.line.slice(this.cursor + charSize, this.line.length);
      this.refreshline();
    }
  }

  /**
   * Delete the word to the left of the cursor
   */
  deletewordleft() {
    if (this.cursor > 0) {
      this.beforeedit(this.line, this.cursor);
      // Reverse the string and match a word near beginning
      // to avoid quadratic time complexity
      let leading = this.line.slice(0, this.cursor);
      const length =
        Array.from(leading)
          .reverse()
          .join("")
          .match(/^\s*(?:[^\w\s]+|\w+)?/)?.[0]?.length || 0;
      leading = leading.slice(0, leading.length - length);
      this.line = leading + this.line.slice(this.cursor, this.line.length);
      this.cursor = leading.length;
      this.refreshline();
    }
  }

  /**
   * Delete the word to the right of the cursor
   */
  deletewordright() {
    if (this.cursor < this.line.length) {
      this.beforeedit(this.line, this.cursor);
      const trailing = this.line.slice(this.cursor);
      const length = trailing.match(/^(?:\s+|\W+|\w+)\s*/)?.[0].length;
      this.line = this.line.slice(0, this.cursor) + trailing.slice(length);
      this.refreshline();
    }
  }

  /**
   * Delete everything from the beginning of the line to the cursor
   */
  deletelineleft() {
    this.beforeedit(this.line, this.cursor);
    const del = this.line.slice(0, this.cursor);
    this.line = this.line.slice(this.cursor);
    this.cursor = 0;
    this.pushtokillring(del);
    this.refreshline();
  }

  /**
   * Delete everything from the cursor to the end of the line
   */
  deletelineright() {
    this.beforeedit(this.line, this.cursor);
    const del = this.line.slice(this.cursor);
    this.line = this.line.slice(0, this.cursor);
    this.pushtokillring(del);
    this.refreshline();
  }

  /**
   * Delete                 the                          line
   */
  deleteline() {
    this.beforeedit(this.line, this.cursor);
    const del = this.line.slice();
    this.line = "";
    this.pushtokillring(del);
    this.refreshline();
  }

  /**
   * Push a deleted string to the killring
   * @param del The deleted string
   * @returns Nothing
   */
  pushtokillring(del?: string) {
    if (!del || del === this.killring[0]) return;
    this.killring.unshift(del);
    this.killringcursor = 0;
    while (this.killring.length > maxkillringlength) this.killring.pop();
  }

  /**
   * Insert a string at the current cursor position from the killring
   */
  yank() {
    if (this.killring.length > 0) {
      this.yanking = true;
      this.insertstring(this.killring[this.killringcursor]);
    }
  }

  /**
   * Yank pop a piece of text and push it to the killring
   * @returns Nothing
   */
  yankpop() {
    if (!this.yanking) return;
    if (this.killring.length > 1) {
      const lastYank = this.killring[this.killringcursor];
      ++this.killringcursor < this.killring.length || (this.killringcursor = 0);

      const currentYank = this.killring[this.killringcursor],
        head = this.line.slice(0, this.cursor - lastYank.length),
        tail = this.line.slice(this.cursor);
      this.line = head + currentYank + tail;
      this.cursor = head.length + currentYank.length;
      this.refreshline();
    }
  }

  /**
   * Move the cursor, relative to its current position
   * @param dx The amount of columns to move the cursor by
   * @param dy The amount of rows to move the cursor by
   * @returns True if the cursor is already where it should be, else returns bogus characters
   */
  movecursor(dx: number, dy: number) {
    if (!(dx || dy)) return true;

    let data = "";
    dx < 0 ? (data += CSI`${-dx}D`) : dx > 0 && (data += CSI`${dx}C`);
    dy < 0 ? (data += CSI`${-dy}A`) : dy > 0 && (data += CSI`${dy}B`);

    return this.writeraw(data);
  }

  /**
   * Move the cursor on the current line horizontally, relative to its current position
   * @param dx The amount of columnts to move the cursor by
   * @param refresh Whether or not to refresh the line at the end, true by default
   * @returns Nothing
   */
  movecursorline(dx: number, refresh = true) {
    if (dx === 0) return;

    const oldPos = this.cursorpos();
    this.cursor += dx;

    // Bounds check
    this.cursor < 0
      ? (this.cursor = 0)
      : this.cursor > this.line.length && (this.cursor = this.line.length);

    const newPos = this.cursorpos();

    // Check if cursor stayed on the line.
    oldPos.rows === newPos.rows
      ? this.movecursor(newPos.cols - oldPos.cols, 0)
      : refresh && this.refreshline();
  }

  /**
   * Set the cursor position to the given coordinates
   * @param x The new x coordinate (columns)
   * @param y The new y coordinate (rows)
   * @returns The sequence written to the output to set the cursor position
   */
  cursorto(x: number, y?: number) {
    return this.writeraw(
      typeof y === "number" ? CSI`${y + 1};${x + 1}H` : CSI`${x + 1}G`
    );
  }

  /**
   * Get the byte length of a character to the left of the cursor in the string
   * @param string The entire string to inspect
   * @param i The horizontal cursor position
   * @returns The character length in bytes
   */
  charlengthleft(string: string, i: number) {
    return i <= 0
      ? 0
      : (i > 1 &&
          (string.codePointAt(i - 2) || 0) >= utf16surrogatethreshold) ||
        (string.codePointAt(i - 1) || 0) >= utf16surrogatethreshold
      ? 2
      : 1;
  }

  /**
   * Get the byte length of a character at the position in the string
   * @param string The entire string to inspect
   * @param i The horizontal cursor position
   * @returns The character length in bytes
   */
  charlengthat(string: string, i: number) {
    return string.length <= i
      ? 1
      : (string.codePointAt(i) || 0) >= utf16surrogatethreshold
      ? 2
      : 1;
  }

  /**
   * Clear the entire screen downwards from where the cursor is located
   * @returns The sequence to clear the screen downwards
   */
  clearscreendown() {
    return this.writeraw(CSI`0J`);
  }

  /**
   * Add the current input line to the history
   * @returns The added line
   */
  addhistory() {
    if (!this.line.length) return "";
    // If the history is disabled then return the line
    if (!this.historysize) return this.line;
    // If the trimmed line is empty then return the line
    if (!this.line.trim().length) return this.line;

    if (!(this.history.length && this.history[0] === this.line)) {
      if (this.removehistoryduplicates) {
        // Remove older history line if identical to new one
        const dupIndex = this.history.indexOf(this.line);
        if (dupIndex !== -1) this.history.splice(dupIndex, 1);
      }

      this.history.unshift(this.line);

      // Only store so many
      this.history.length > this.historysize && this.history.pop();
    }

    this.historyindex = -1;

    // The listener could change the history object, possibly
    // to remove the last added entry if it is sensitive and should
    // not be persisted in the history, like a password
    const line = this.history[0];

    // Emit history event to notify listeners of update
    this.emit("history", this.history);

    return line;
  }

  /**
   * Refresh the input line
   */
  refreshline() {
    // line length
    const line = this.prefix + this.line;
    const dispPos = this.displaypos(line);
    const lineCols = dispPos.cols;
    const lineRows = dispPos.rows;

    // cursor position
    const cursorpos = this.cursorpos();

    // First move to the bottom of the current line, based on cursor pos
    const { prevrows } = this;
    prevrows && this.movecursor(0, -prevrows);

    // Cursor to left edge.
    this.cursorto(0);
    // erase data
    this.clearscreendown();

    // Write the prompt and the current buffer content.
    this.writeraw(line);

    // Force terminal to allocate a new line
    lineCols || this.writeraw(" ");

    // Move cursor to original position.
    this.cursorto(cursorpos.cols);

    const diff = lineRows - cursorpos.rows;
    diff > 0 && this.movecursor(0, -diff);

    this.prevrows = cursorpos.rows;
  }

  /**
   * Close the input stream
   * @returns Nothing
   */
  close() {
    if (this.closed) return;
    this.pause();
    this.input.setRaw(false, windows ? undefined : rawopts);
    this.closed = true;
    this.emitSync("close");
  }

  /**
   * Pause the input stream
   * @returns Nothing
   */
  pause() {
    if (this.paused) return;
    this.paused = true;
    this.emitSync("pause");
    return this;
  }

  /**
   * Resume the input stream
   * @returns Nothing
   */
  resume() {
    if (!this.paused) return;
    this.paused = false;
    if (!this.listening) this.init();
    this.emitSync("resume");
    return this;
  }

  /**
   * Get the size of the output console in columns and rows
   */
  get size() {
    return Deno.consoleSize();
  }

  /**
   * Get the display position of the last character of a string
   * @param string The string to check for
   * @returns The display position
   */
  displaypos(string: string) {
    const col = this.size.columns;
    let offset = 0,
      rows = 0;
    for (const char of tty.stripAnsi(string)) {
      if (char === "\n") {
        // Rows must be incremented by 1 even if offset = 0 or col = +Infinity.
        rows += Math.ceil(offset / col) || 1;
        offset = 0;
        continue;
      }
      // Tabs must be aligned by an offset of the tab size.
      if (char === "\t") {
        offset += this.tabsize - (offset % this.tabsize);
        continue;
      }
      const width = tty.wcswidth(char);
      !width || width === 1
        ? (offset += width)
        : // width === 2
          ((offset + 1) % col === 0 && offset++, (offset += 2));
    }
    const cols = offset % col;
    rows += (offset - cols) / col;
    return { cols, rows };
  }

  /**
   * Return the position of the cursor on the current line
   * @returns The cursor position
   */
  cursorpos() {
    return this.displaypos(this.prefix + this.line.slice(0, this.cursor));
  }

  /**
   * The internal key input processor
   */
  private async *emitkeys(): AsyncGenerator<undefined, any, string> {
    while (true) {
      let ch = yield;
      let s = ch;
      let escaped = false;
      const key: KeyInit = {
        sequence: undefined,
        name: undefined,
        ctrl: false,
        meta: false,
        shift: false,
      };

      if (ch === ESC) {
        escaped = true;
        s += ch = yield;

        if (ch === ESC) {
          s += ch = yield;
        }
      }

      if (escaped && (ch === "O" || ch === "[")) {
        // ANSI escape sequence
        let code = ch;
        let modifier = 0;

        if (ch === "O") {
          // ESC O letter
          // ESC O modifier letter
          s += ch = yield;

          if (ch >= "0" && ch <= "9") {
            modifier = (ch.charCodeAt(0) >> 0) - 1;
            s += ch = yield;
          }

          code += ch;
        } else if (ch === "[") {
          // ESC [ letter
          // ESC [ modifier letter
          // ESC [ [ modifier letter
          // ESC [ [ num char
          s += ch = yield;

          if (ch === "[") {
            // \x1b[[A
            //      ^--- escape codes might have a second bracket
            code += ch;
            s += ch = yield;
          }

          /*
           * Here and later we try to buffer just enough data to get
           * a complete ascii sequence.
           *
           * We have basically two classes of ascii characters to process:
           *
           *
           * 1. `\x1b[24;5~` should be parsed as { code: '[24~', modifier: 5 }
           *
           * This particular example is featuring Ctrl+F12 in xterm.
           *
           *  - `;5` part is optional, e.g. it could be `\x1b[24~`
           *  - first part can contain one or two digits
           *
           * So the generic regexp is like /^\d\d?(;\d)?[~^$]$/
           *
           *
           * 2. `\x1b[1;5H` should be parsed as { code: '[H', modifier: 5 }
           *
           * This particular example is featuring Ctrl+Home in xterm.
           *
           *  - `1;5` part is optional, e.g. it could be `\x1b[H`
           *  - `1;` part is optional, e.g. it could be `\x1b[5H`
           *
           * So the generic regexp is like /^((\d;)?\d)?[A-Za-z]$/
           *
           */
          const cmdStart = s.length - 1;

          // Skip one or two leading digits
          if (ch >= "0" && ch <= "9") {
            s += ch = yield;

            if (ch >= "0" && ch <= "9") {
              s += ch = yield;
            }
          }

          // skip modifier
          if (ch === ";") {
            s += ch = yield;

            if (ch >= "0" && ch <= "9") {
              s += yield;
            }
          }

          /*
           * We buffered enough data, now trying to extract code
           * and modifier from it
           */
          const cmd = s.slice(cmdStart);
          let match;

          if ((match = cmd.match(/^(\d\d?)(;(\d))?([~^$])$/))) {
            code += match[1] + match[4];
            modifier = (+match[3] || 1) - 1;
          } else if ((match = cmd.match(/^((\d;)?(\d))?([A-Za-z])$/))) {
            code += match[4];
            modifier = (+match[3] || 1) - 1;
          } else {
            code += cmd;
          }
        }

        // Parse the key modifier
        key.ctrl = !!(modifier & 4);
        key.meta = !!(modifier & 10);
        key.shift = !!(modifier & 1);
        key.code = code;

        // Parse the key itself
        switch (code) {
          /* xterm/gnome ESC [ letter (with modifier) */
          case "[P":
            key.name = "f1";
            break;
          case "[Q":
            key.name = "f2";
            break;
          case "[R":
            key.name = "f3";
            break;
          case "[S":
            key.name = "f4";
            break;

          /* xterm/gnome ESC O letter (without modifier) */
          case "OP":
            key.name = "f1";
            break;
          case "OQ":
            key.name = "f2";
            break;
          case "OR":
            key.name = "f3";
            break;
          case "OS":
            key.name = "f4";
            break;

          /* xterm/rxvt ESC [ number ~ */
          case "[11~":
            key.name = "f1";
            break;
          case "[12~":
            key.name = "f2";
            break;
          case "[13~":
            key.name = "f3";
            break;
          case "[14~":
            key.name = "f4";
            break;

          /* from Cygwin and used in libuv */
          case "[[A":
            key.name = "f1";
            break;
          case "[[B":
            key.name = "f2";
            break;
          case "[[C":
            key.name = "f3";
            break;
          case "[[D":
            key.name = "f4";
            break;
          case "[[E":
            key.name = "f5";
            break;

          /* common */
          case "[15~":
            key.name = "f5";
            break;
          case "[17~":
            key.name = "f6";
            break;
          case "[18~":
            key.name = "f7";
            break;
          case "[19~":
            key.name = "f8";
            break;
          case "[20~":
            key.name = "f9";
            break;
          case "[21~":
            key.name = "f10";
            break;
          case "[23~":
            key.name = "f11";
            break;
          case "[24~":
            key.name = "f12";
            break;

          /* xterm ESC [ letter */
          case "[A":
            key.name = "up";
            break;
          case "[B":
            key.name = "down";
            break;
          case "[C":
            key.name = "right";
            break;
          case "[D":
            key.name = "left";
            break;
          case "[E":
            key.name = "clear";
            break;
          case "[F":
            key.name = "end";
            break;
          case "[H":
            key.name = "home";
            break;

          /* xterm/gnome ESC O letter */
          case "OA":
            key.name = "up";
            break;
          case "OB":
            key.name = "down";
            break;
          case "OC":
            key.name = "right";
            break;
          case "OD":
            key.name = "left";
            break;
          case "OE":
            key.name = "clear";
            break;
          case "OF":
            key.name = "end";
            break;
          case "OH":
            key.name = "home";
            break;

          /* xterm/rxvt ESC [ number ~ */
          case "[1~":
            key.name = "home";
            break;
          case "[2~":
            key.name = "insert";
            break;
          case "[3~":
            key.name = "delete";
            break;
          case "[4~":
            key.name = "end";
            break;
          case "[5~":
            key.name = "pageup";
            break;
          case "[6~":
            key.name = "pagedown";
            break;

          /* putty */
          case "[[5~":
            key.name = "pageup";
            break;
          case "[[6~":
            key.name = "pagedown";
            break;

          /* rxvt */
          case "[7~":
            key.name = "home";
            break;
          case "[8~":
            key.name = "end";
            break;

          /* rxvt keys with modifiers */
          case "[a":
            key.name = "up";
            key.shift = true;
            break;
          case "[b":
            key.name = "down";
            key.shift = true;
            break;
          case "[c":
            key.name = "right";
            key.shift = true;
            break;
          case "[d":
            key.name = "left";
            key.shift = true;
            break;
          case "[e":
            key.name = "clear";
            key.shift = true;
            break;

          case "[2$":
            key.name = "insert";
            key.shift = true;
            break;
          case "[3$":
            key.name = "delete";
            key.shift = true;
            break;
          case "[5$":
            key.name = "pageup";
            key.shift = true;
            break;
          case "[6$":
            key.name = "pagedown";
            key.shift = true;
            break;
          case "[7$":
            key.name = "home";
            key.shift = true;
            break;
          case "[8$":
            key.name = "end";
            key.shift = true;
            break;

          case "Oa":
            key.name = "up";
            key.ctrl = true;
            break;
          case "Ob":
            key.name = "down";
            key.ctrl = true;
            break;
          case "Oc":
            key.name = "right";
            key.ctrl = true;
            break;
          case "Od":
            key.name = "left";
            key.ctrl = true;
            break;
          case "Oe":
            key.name = "clear";
            key.ctrl = true;
            break;

          case "[2^":
            key.name = "insert";
            key.ctrl = true;
            break;
          case "[3^":
            key.name = "delete";
            key.ctrl = true;
            break;
          case "[5^":
            key.name = "pageup";
            key.ctrl = true;
            break;
          case "[6^":
            key.name = "pagedown";
            key.ctrl = true;
            break;
          case "[7^":
            key.name = "home";
            key.ctrl = true;
            break;
          case "[8^":
            key.name = "end";
            key.ctrl = true;
            break;

          /* misc. */
          case "[Z":
            key.name = "tab";
            key.shift = true;
            break;
          default:
            key.name = "undefined";
            break;
        }
      } else if (ch === "\r") {
        // carriage return
        key.name = "return";
        key.meta = escaped;
      } else if (ch === "\n") {
        // Enter, should have been called linefeed
        key.name = "enter";
        key.meta = escaped;
      } else if (ch === "\t") {
        // tab
        key.name = "tab";
        key.meta = escaped;
      } else if (ch === "\b" || ch === "\x7f") {
        // backspace or ctrl+h
        key.name = "backspace";
        key.meta = escaped;
      } else if (ch === ESC) {
        // escape key
        key.name = "escape";
        key.meta = escaped;
      } else if (ch === " ") {
        key.name = "space";
        key.meta = escaped;
      } else if (!escaped && ch <= "\x1a") {
        // ctrl+letter
        key.name = String.fromCharCode(
          ch.charCodeAt(0) + "a".charCodeAt(0) - 1
        );
        key.ctrl = true;
      } else if (/^[0-9A-Za-z]$/.test(ch)) {
        // Letter, number, shift+letter
        key.name = ch.toLowerCase();
        key.shift = /^[A-Z]$/.test(ch);
        key.meta = escaped;
      } else if (escaped) {
        // Escape sequence timeout
        key.name = ch.length ? undefined : "escape";
        key.meta = true;
      }

      key.sequence = s;

      const cla = this.charlengthat(s, 0);
      s.length !== 0 && (key.name !== undefined || escaped)
        ? /* Named character or sequence */
          await this.emit("key", key, escaped ? undefined : s)
        : cla === s.length &&
          /* Single unnamed character, e.g. "." */
          (await this.emit("key", key, s));

      /* Unrecognized or broken escape sequence, don't emit anything */
    }
  }

  emitSync<K extends keyof CLIEvent>(
    event: K,
    ...args: Parameters<CLIEvent[K]>
  ): this {
    try {
      super.emitSync("event", event, ...args);
      return super.emitSync(event, ...args);
    } catch (e) {
      console.log(e);
      return this;
    }
  }

  async emit<K extends keyof CLIEvent>(
    event: K,
    ...args: Parameters<CLIEvent[K]>
  ): Promise<this> {
    try {
      await super.emit("event", event, ...args);
      return await super.emit(event, ...args);
    } catch (e) {
      console.log(e);
      return this;
    }
  }

  /**
   * Get a promise that resolves to a single received line
   */
  get nextline(): Promise<string> {
    return new Promise((r) => this.once("line", r, true));
  }

  /**
   * Get a promise that resolves to a single input character
   */
  get char(): Promise<string> {
    return new Promise((r) => this.once("char", r, true));
  }
  /**
   * Get an async iterator returning single character inputs
   */
  get chars() {
    let done = false;
    const iter = (): AsyncIterator<string, null> => ({
      next: async (): Promise<IteratorResult<string>> => {
        if (done) return { value: undefined, done: true };
        return await this.char.then((c) => ({ value: c, done: false }));
      },
      return: (v: any) => Promise.resolve({ value: v, done: (done = true) }),
    });
    return {
      [Symbol.asyncIterator]: iter,
    };
  }
}
