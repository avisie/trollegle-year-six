import { dict } from "../../data/dict.ts";

import { join, resolve, extname, basename } from "std/path/mod.ts";
import { fstring } from "../../util/string.ts";

import { Sources } from "../sources/types.ts";
import { Strictly } from "../../util/types.ts";
import { Collection } from "../collection/mod.ts";
import { User } from "../userlist/user.ts";

export type PermittedCallbackReturnValue = boolean;
export type CommandCategoryPermittedCallbackReturnValue =
  PermittedCallbackReturnValue | null;

async function importInitializer<
  I extends {
    configname: string | null;
    new (...args: any[]): any;
  }
>(
  path: string,
  initclass: I
): Promise<
  | (I extends {
      new (...args: any[]): infer R;
    }
      ? R
      : never)
  | string
> {
  const configpath =
      typeof initclass["configname"] === "string"
        ? resolve(path, initclass["configname"] + ".ts")
        : path,
    mod = await import(configpath).catch((e: Error) => e),
    classname = initclass.name;
  if (!mod || mod instanceof Error)
    return `${classname} at '${configpath}' could not be imported (${
      (mod instanceof Error && mod.stack) || "unknown error"
    })`;

  if (!Object.hasOwn(mod, "default"))
    return `${classname} at '${configpath}' does not have a default export`;

  const init = mod.default;
  if (!(init instanceof initclass))
    return `Initializer at '${configpath}' is not an instance of ${classname}`;

  return init;
}

// Command types

export type CommandArguments = (
  | {
      type: "text";
      rest?: boolean;
    }
  | {
      type: "option";
      values: string[];
    }
) & {
  name: string;
  required: boolean;
};

export type FormatVerboseCallback<
  I extends CommandInformation,
  C extends CommandCallbacks<I, C>
> = (
  cmd: Command<I, C>,
  user: User<Sources>,
  args: string[]
) => string | Promise<string>;

export type CommandPermittedCallback<
  I extends CommandInformation,
  C extends CommandCallbacks<I, C>
> = (
  cmd: Command<I, C>,
  user: User<Sources>,
  args: string[]
) => PermittedCallbackReturnValue | Promise<PermittedCallbackReturnValue>;

export type CommandExecuteCallback<
  I extends CommandInformation,
  C extends CommandCallbacks<I, C>
> = (cmd: Command<I, C>, user: User<Sources>, args: string[]) => any;

export type CommandInformation = {
  names: string[];
  description: string;
  dict: Record<any, any>;
  args: CommandArguments[];
};

export type CommandCallbacks<
  I extends CommandInformation,
  C extends CommandCallbacks<any, any>
> = {
  sensitive: boolean | FormatVerboseCallback<I, C>;
  permitted: boolean | CommandPermittedCallback<I, C>;
  execute: CommandExecuteCallback<I, C>;
};

export class CommandInitializer<
  I extends CommandInformation,
  C extends CommandCallbacks<I, C>
> implements CommandInformation, CommandCallbacks<I, C>
{
  static configname = null;

  names: string[];
  description: string;
  dict: Record<any, any>;
  args: CommandArguments[];
  sensitive: boolean | FormatVerboseCallback<I, C>;
  permitted: boolean | CommandPermittedCallback<I, C>;
  execute: CommandExecuteCallback<I, C>;

  constructor(
    { names, description, dict, args }: Strictly<I>,
    { sensitive, permitted, execute }: Strictly<C>
  ) {
    this.names = names;
    this.description = description;
    this.dict = dict;
    this.args = args;

    this.sensitive = sensitive;
    this.permitted = permitted;
    this.execute = execute;
  }
}

type CommandOptions = {
  handler: CommandHandler;
  category: CommandCategory;
  subcategory?: CommandSubcategory;
  path: string;
};
class Command<
  I extends CommandInformation = any,
  C extends CommandCallbacks<I, C> = CommandCallbacks<I, any>
> implements CommandInitializer<I, C>
{
  handler: CommandHandler;
  category: CommandCategory;
  subcategory: CommandSubcategory | undefined;
  path: string;

  names: I["names"];
  description: I["description"];
  dict: I["dict"];
  args: I["args"];
  sensitive: C["sensitive"];
  permitted: C["permitted"];
  execute: C["execute"];

  constructor(
    { handler, path, category, subcategory }: Strictly<CommandOptions>,
    {
      names,
      description,
      sensitive,
      dict,
      args,
      permitted,
      execute,
    }: Strictly<CommandInitializer<I, C>>
  ) {
    this.handler = handler;
    this.category = category;
    this.subcategory = subcategory;
    this.path = path;

    if (names.some((str) => str.includes(" ")))
      throw new Error(
        `command at ${path} has a space character in at least one of the names`
      );
    if (names.some((str) => str.toLowerCase() !== str))
      throw new Error(
        `command at ${path} has one or more names which are not fully lowercase`
      );
    if (new Set(names).size !== names.length)
      throw new Error(`command at ${path} has duplicate names`);

    const base = basename(path),
      ext = extname(base),
      name = base.slice(0, -ext.length || undefined);
    if (!names.includes(name.toLowerCase()))
      throw new Error(
        `command at ${path} must have a name equivalent to the filename (without .ts)`
      );

    this.names = names;
    this.description = description;
    this.sensitive = sensitive;
    this.dict = dict;
    this.args = args;
    this.permitted = permitted;
    this.execute = execute;
  }
}

// Command subcategory types

export type CommandSubcategoryPermittedCallback<
  I extends CommandSubcategoryInformation,
  C extends CommandSubcategoryCallbacks<I, C>
> = (
  subcat: CommandSubcategory<I, C>,
  user: User<Sources>,
  command: Command,
  args: string[]
) => PermittedCallbackReturnValue | Promise<PermittedCallbackReturnValue>;

type CommandSubcategoryInformation = {
  name: string;
  description: string;
  dict: Record<any, any>;
};
type CommandSubcategoryCallbacks<
  I extends CommandSubcategoryInformation,
  C extends CommandSubcategoryCallbacks<any, any>
> = {
  permitted: boolean | CommandSubcategoryPermittedCallback<I, C>;
};

export class CommandSubcategoryInitializer<
  I extends CommandSubcategoryInformation,
  C extends CommandSubcategoryCallbacks<I, C>
> implements CommandSubcategoryInformation, CommandSubcategoryCallbacks<I, C>
{
  static configname = "SUBCATEGORY";

  name: I["name"];
  description: I["description"];
  dict: I["dict"];
  permitted: C["permitted"];

  constructor(
    { name, description, dict }: Strictly<I>,
    { permitted }: Strictly<C>
  ) {
    this.name = name;
    this.description = description;
    this.dict = dict;
    this.permitted = permitted;
  }
}

export type CommandSubcategoryOptions = {
  handler: CommandHandler;
  category: CommandCategory;
  path: string;
};
export class CommandSubcategory<
  I extends CommandSubcategoryInformation = any,
  C extends CommandSubcategoryCallbacks<I, C> = CommandSubcategoryCallbacks<
    I,
    any
  >
> implements CommandSubcategoryInitializer<I, C>
{
  handler: CommandHandler;
  category: CommandCategory;
  readonly path: string;

  commands: Collection<string, Command> = new Collection();

  name: I["name"];
  description: I["description"];
  dict: I["dict"];
  permitted: C["permitted"];

  constructor(
    { handler, category, path }: Strictly<CommandSubcategoryOptions>,
    {
      name,
      description,
      dict,
      permitted,
    }: Strictly<CommandSubcategoryInitializer<I, C>>
  ) {
    this.handler = handler;
    this.category = category;
    this.path = path;

    this.name = name;
    this.description = description;
    this.dict = dict;
    this.permitted = permitted;
  }
}

// Command category types

type CommandCategoryPermittedCallback<
  I extends CommandCategoryInformation,
  C extends CommandCategoryCallbacks<I, C>
> = (
  cat: CommandCategory<I, C>,
  user: User<Sources>,
  command: Command | undefined,
  args: string[]
) =>
  | CommandCategoryPermittedCallbackReturnValue
  | Promise<CommandCategoryPermittedCallbackReturnValue>;

type CommandCategoryInformation = {
  name: string;
  description: string;
  prefices: string[];
  dict: Record<any, any>;
};

type CommandCategoryCallbacks<
  I extends CommandCategoryInformation,
  C extends CommandCategoryCallbacks<any, any>
> = {
  permitted: boolean | CommandCategoryPermittedCallback<I, C>;
};

export class CommandCategoryInitializer<
  I extends CommandCategoryInformation,
  C extends CommandCategoryCallbacks<I, C>
> implements CommandCategoryInformation, CommandCategoryCallbacks<I, C>
{
  static configname = "CATEGORY";

  name: I["name"];
  description: I["description"];
  prefices: I["prefices"];
  dict: I["dict"];
  permitted: C["permitted"];

  constructor(
    { name, description, prefices, dict }: Strictly<I>,
    { permitted }: Strictly<C>
  ) {
    this.name = name;
    this.description = description;
    this.prefices = prefices;
    this.dict = dict;
    this.permitted = permitted;
  }
}

export type CommandCategoryOptions = {
  handler: CommandHandler;
  path: string;
};
export class CommandCategory<
  I extends CommandCategoryInformation = any,
  C extends CommandCategoryCallbacks<I, C> = CommandCategoryCallbacks<I, any>
> implements CommandCategoryInitializer<I, C>
{
  handler: CommandHandler;
  readonly path: string;

  subcategories: Collection<string, CommandSubcategory> = new Collection();
  commands: Collection<string, Command> = new Collection();

  name: I["name"];
  description: I["description"];
  prefices: I["prefices"];
  dict: I["dict"];
  permitted: C["permitted"];

  constructor(
    { handler, path }: Strictly<CommandCategoryOptions>,
    {
      name,
      description,
      prefices,
      dict,
      permitted,
    }: Strictly<CommandCategoryInitializer<I, C>>
  ) {
    this.handler = handler;
    this.path = path;

    this.name = name;
    this.description = description;
    this.prefices = prefices;
    this.dict = dict;
    this.permitted = permitted;
  }
}

export type HandlerOptions = {
  path: string;
  communicate: (...args: any[]) => any;
};
export class CommandHandler {
  readonly path: string;
  communicate: (...args: any[]) => any;

  categories: Collection<string, CommandCategory> = new Collection();
  prefices: Collection<string, string> = new Collection();

  constructor({ path, communicate }: HandlerOptions) {
    this.path = resolve(path);
    this.communicate = communicate;
  }

  async init(): Promise<boolean> {
    const { path } = this;
    let error = false;

    // CommandCategory loading
    for (const { name, isFile } of Deno.readDirSync(path)) {
      const catpath = join(path, name);
      if (isFile) {
        this.communicate(
          `Category directory at '${catpath}' must be a directory, not a file`
        );
        error ||= true;
      }

      const catinit = await importInitializer(
        catpath,
        CommandCategoryInitializer
      );
      if (typeof catinit === "string") {
        this.communicate(catinit);
        error ||= true;
        continue;
      }

      for (const prefix of catinit.prefices) {
        if (prefix.startsWith(" ")) {
          this.communicate(
            `Prefix '${prefix}' of category at '${this.path}' starts with whitespace, whereas it must not`
          );
          error ||= true;
        }
        if (prefix !== prefix.toLowerCase()) {
          this.communicate(
            `Prefix '${prefix}' of category at '${this.path}' must not contain uppercase characters`
          );
          error ||= true;
        }
      }
      if (error) continue;

      // CommandCategory initialization
      const category = new CommandCategory(
        {
          handler: this,
          path: catpath,
        },
        catinit
      );

      // CommandSubcategory loading
      const subcatpaths: string[] = [];
      for (const { name, isDirectory } of Deno.readDirSync(catpath)) {
        if (name === CommandCategoryInitializer.configname + ".ts") continue;
        const nextpath = join(catpath, name);
        if (isDirectory) {
          subcatpaths.push(nextpath);
          continue;
        }

        const commandinit = await importInitializer(
          nextpath,
          CommandInitializer
        );
        if (typeof commandinit === "string") {
          this.communicate(commandinit);
          error ||= true;
          continue;
        }

        // Command initialization
        const command = new Command(
          {
            handler: this,
            path: nextpath,
            category,
          },
          commandinit
        );

        if (error) continue;
        for (const name of command.names) category.commands.set(name, command);
      }
      if (error) continue;

      // CommandCategory initialization finished
      this.categories.set(category.name, category);

      // CommandSubcategory loading
      for (const subcatpath of subcatpaths) {
        const subcatinit = await importInitializer(
          subcatpath,
          CommandSubcategoryInitializer
        );
        if (typeof subcatinit === "string") {
          this.communicate(subcatinit);
          error ||= true;
          continue;
        }

        const subcategory = new CommandSubcategory(
          {
            handler: this,
            category,
            path: subcatpath,
          },
          subcatinit
        );

        for (const { name, isDirectory } of Deno.readDirSync(subcatpath)) {
          if (name === CommandSubcategoryInitializer.configname + ".ts")
            continue;
          const subcatcmdpath = join(subcatpath, name);
          if (isDirectory) {
            this.communicate(
              `Command initializer at '${subcatcmdpath}' must be a file, not a directory`
            );
            error ||= true;
            continue;
          }

          const subcatcommandinit = await importInitializer(
            subcatcmdpath,
            CommandInitializer
          );
          if (typeof subcatcommandinit === "string") {
            this.communicate(subcatcommandinit);
            error ||= true;
            continue;
          }

          const subcatcommand = new Command(
            {
              handler: this,
              path: subcatcmdpath,
              category,
              subcategory,
            },
            subcatcommandinit
          );

          if (error) continue;
          for (const name of subcatcommand.names)
            subcategory.commands.set(name, subcatcommand);
        }
        if (error) continue;
        category.subcategories.set(subcategory.name, subcategory);
      }
    }
    if (error) return false;

    const prefices: Record<string, NonNullable<CommandCategory["name"]>> = {};
    for (const category of this.categories.values())
      for (const prefix of category.prefices) {
        if (Object.hasOwn(prefices, prefix)) {
          this.communicate(
            `Prefix '${prefix}' of category at '${
              this.categories.get(prefices[prefix])!.path
            }' is duplicated by category at '${category.path}'`
          );
          error ||= true;
        }
        prefices[prefix] = category.name;
      }

    if (error) return false;

    for (const prefix of Object.keys(prefices).sort(
      (a, b) => b.length - a.length
    ))
      this.prefices.set(prefix, prefices[prefix]);

    return true;
  }

  async verbose(user: User<Sources>, msg: string, cmd?: Command) {
    if (cmd) {
      const { sensitive } = cmd;
      if (sensitive) {
        const split = msg.split(" ");
        if (sensitive === true)
          msg = fstring(dict.verbose.sensitive, split.shift(), split.length);
        else msg = await sensitive(cmd, user, split);
      }
    }

    for (const u of user.trollegle.users.verbose(user, true, true).values())
      u.specialUserMessage(user, msg, dict.format.tags.verbose);
  }

  async handle(user: User<Sources>, message: string): Promise<boolean> {
    const original = message;
    message = message.trim();

    const candidates: [string, CommandCategory][] = [],
      low = message.toLowerCase();
    for (const [prefix, category] of this.prefices.entries())
      if (low.startsWith(prefix))
        candidates.push([prefix, this.categories.get(category)!]);

    if (!candidates.length) return false;

    let permitted: boolean | null = null,
      command: Command | undefined,
      // I would've named this variable "arguments" to name the one below
      // "args" instead of "pieces", but that is reserved by JS :(
      args: string[] | undefined;
    for (const [prefix, category] of candidates) {
      const pieces = message.slice(prefix.length).split(" "),
        cmdarg = pieces.shift()?.toLowerCase?.();
      let cmd: Command | undefined =
        cmdarg === undefined ? undefined : category.commands.get(cmdarg);

      if (cmdarg !== undefined && !cmd)
        for (const subcat of category.subcategories.values())
          if ((cmd = subcat.commands.get(cmdarg))) break;

      await this.verbose(user, original, cmd);

      const catpermitted =
        typeof category.permitted === "function"
          ? await category.permitted(category, user, cmd, pieces)
          : category.permitted;
      if (catpermitted !== null) {
        permitted = catpermitted;
        command = cmd;
        args = pieces;

        break;
      }
    }

    if (permitted === null) return false;
    if (!command || !args || !permitted) return true;

    const { subcategory } = command;
    if (
      subcategory &&
      !(typeof subcategory.permitted === "function"
        ? await subcategory.permitted(subcategory, user, command, args)
        : subcategory.permitted)
    )
      return true;

    if (
      !(typeof command.permitted === "function"
        ? await command.permitted(command, user, args)
        : command.permitted)
    )
      return true;

    try {
      await command.execute(command, user, args);
    } catch (e: any) {
      this.communicate(
        `An error has occurred at command '${command.names.join(", ")}', at '${
          command.path
        }': ${e instanceof Error ? e.stack : e}`
      );
      await user.serverMessage(dict.error.command).catch(() => {});
    }

    return await Promise.resolve(true);
  }
}
