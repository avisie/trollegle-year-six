import { random, uint8arraytobase64 } from "../../util/array.ts";

import { Database, Statement } from "x/sqlite3@0.9.1/mod.ts";
import { Sources } from "../sources/types.ts";
import { Trollegle } from "../trollegle/mod.ts";
import { UserData } from "../userlist/user.ts";

export type PartialAccountData = {
  id: number;
  user: string;
  userlow: string;
};

export type AccountData = PartialAccountData & {
  pash: string;
  data: string;
};

export type AccountManagerOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
};

export type WorkerListener<K extends keyof WorkerEventMap> = (
  this: Worker,
  ev: WorkerEventMap[K]
) => any;

const passwordcharset = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
] as const;
export class AccountManager<S extends Sources> {
  trollegle: Trollegle<S>;

  creating: Set<string> = new Set();
  attempting: Set<string> = new Set();
  modifying: Set<string> = new Set();

  used: Set<string> = new Set();

  db: Database;

  readonly createstmt: Statement;
  readonly lookupstmt: Statement;
  readonly updatestmt: Statement;
  readonly passwdstmt: Statement;
  readonly deletestmt: Statement;
  readonly amountstmt: Statement;
  readonly returnstmt: Statement;

  constructor({ trollegle }: AccountManagerOptions<S>) {
    this.trollegle = trollegle;
    (this.db = new Database("accounts.db", { unsafeConcurrency: true })).exec(
      `CREATE TABLE IF NOT EXISTS Accounts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userlow TEXT,
        user TEXT,
        pash TEXT,
        data TEXT
      )`
    );
    this.createstmt = this.db.prepare(
      "INSERT INTO Accounts (userlow, user, pash, data) VALUES (?, ?, ?, ?) RETURNING id, userlow, user, pash, data"
    );
    this.lookupstmt = this.db.prepare(
      "SELECT * FROM Accounts WHERE userlow = ?"
    );
    this.updatestmt = this.db.prepare(
      "UPDATE Accounts SET data = ? WHERE userlow = ?"
    );
    this.passwdstmt = this.db.prepare(
      "UPDATE Accounts SET pash = ? WHERE userlow = ?"
    );
    this.deletestmt = this.db.prepare("DELETE FROM Accounts WHERE userlow = ?");
    this.amountstmt = this.db.prepare("SELECT COUNT(1) FROM Accounts");
    this.returnstmt = this.db.prepare(
      "SELECT * FROM Accounts ORDER BY id LIMIT ?, ?"
    );
  }

  generatePassword(length: number): string {
    let result = "";
    for (let i = 0; i < length; i++) result += random(passwordcharset);
    return result;
  }

  hash(pass: string): Promise<string> {
    const { salt, iterations, bitlength } =
      this.trollegle.config.account.password;

    return new Promise<string>((resolve, reject) => {
      const w = new Worker(new URL("./hashworker.js", import.meta.url).href, {
          type: "module",
        }),
        onmessage: WorkerListener<"message"> = ({ data }) => {
          w.removeEventListener("error", onerror);
          w.terminate();
          if (!(data instanceof Uint8Array))
            reject(
              new Error(
                `didnt get a uint8array from the worker, got ${
                  data?.constructor?.name || typeof data
                } instead`
              )
            );
          resolve(uint8arraytobase64(data));
        },
        onerror: WorkerListener<"error"> = (error) => {
          w.removeEventListener("message", onmessage);
          w.terminate();
          reject(error.error);
        };
      w.addEventListener("message", onmessage, { once: true });
      w.addEventListener("error", onerror, { once: true });

      w.postMessage([pass, salt, iterations, bitlength]);
    });
  }

  async create(
    user: string,
    pass: string,
    data: UserData
  ): Promise<AccountData | false | undefined> {
    if (this.lookup(user)) return false;

    return this.createstmt.get<AccountData>(
      user.toLowerCase(),
      user,
      await this.hash(pass),
      JSON.stringify(data)
    );
  }

  lookup(user: string): AccountData | undefined {
    const accounts = this.lookupstmt.all<AccountData>(user.toLowerCase());
    if (accounts.length > 1)
      throw new Error(
        `received ${accounts.length} account entries for ${user}`
      );
    return accounts[0];
  }

  save(user: string, data: UserData): number {
    return this.updatestmt.run(JSON.stringify(data), user.toLowerCase());
  }

  changepwd(user: string, pash: string): number {
    return this.passwdstmt.run(pash, user.toLowerCase());
  }

  delete(user: string): number {
    return this.deletestmt.run(user.toLowerCase());
  }

  count(): number {
    return this.amountstmt.get<{ "COUNT(1)": number }>()!["COUNT(1)"];
  }

  fetch(count: number, offset: number): AccountData[] {
    return this.returnstmt.all(offset, count);
  }
}
