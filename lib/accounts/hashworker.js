const { subtle } = crypto;
addEventListener(
  "message",
  async ({ data: [pass, salt, iterations, bitlength] }) => {
    if (
      !(
        typeof pass === "string" &&
        salt instanceof Uint8Array &&
        typeof iterations === "number" &&
        typeof bitlength === "number"
      )
    )
      throw new TypeError("you are b-b-b-b-bad to the bone");
    postMessage(
      new Uint8Array(
        await subtle.deriveBits(
          {
            name: "PBKDF2",
            hash: "SHA-512",
            iterations,
            salt,
          },
          await subtle.importKey(
            "raw",
            new TextEncoder().encode(pass),
            "PBKDF2",
            false,
            ["deriveBits"]
          ),
          bitlength
        )
      )
    );
    close();
  }
);
