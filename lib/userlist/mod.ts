import { Sources } from "../sources/types.ts";
import { Trollegle, UserID } from "../trollegle/mod.ts";

import { Collection } from "../collection/mod.ts";
import { NickList } from "./nicklist.ts";
import { User } from "./user.ts";

export type UserListOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  defaultNicks: string[];
};

export class UserList<S extends Sources> {
  list: Collection<UserID, User<S>> = new Collection();

  nicks: NickList<S>;

  constructor({ trollegle, defaultNicks }: UserListOptions<S>) {
    this.nicks = new NickList({ trollegle, defaultNicks });
  }

  getid(admin: boolean): number {
    let id = +!admin;
    while (this.list.has(id)) id++;
    return id;
  }

  resolve(
    id: string | number,
    list: Collection<UserID, User<S>> = this.list
  ): User<S> | undefined {
    const n = +id;
    if (typeof id === "number" || !isNaN(n)) return list.get(n);
    const f = this.nicks.filterFn(id);
    return list.find(({ nick }) => f(nick));
  }

  except(user: User<S>): Collection<UserID, User<S>> {
    return this.list.filter((u) => u !== user);
  }
  connected(): Collection<UserID, User<S>> {
    return this.list.filter((u) => u.connection.connected);
  }
  waiting(): Collection<UserID, User<S>> {
    return this.list.filter(
      (u) => u.connection.established && !u.connection.connected
    );
  }

  inside(): Collection<UserID, User<S>> {
    return this.list.filter((u) => u.connection.connected && !u.unconfirmed);
  }

  shouldbeabletoseemosttypesofuserandsystemmessagesithink(): Collection<
    UserID,
    User<S>
  >;
  shouldbeabletoseemosttypesofuserandsystemmessagesithink(
    user: User<S>,
    exclude: boolean,
    respectignores: boolean
  ): Collection<UserID, User<S>>;
  shouldbeabletoseemosttypesofuserandsystemmessagesithink(
    user?: User<S>,
    exclude?: boolean,
    respectignores?: boolean
  ): Collection<UserID, User<S>> {
    return this.inside().filter(
      (u) =>
        !user ||
        ((!exclude || u !== user) && (!respectignores || !u.ignores.has(user)))
    );
  }

  listable(): Collection<UserID, User<S>> {
    return this.inside();
  }

  initiallyListed(user: User<S>): Collection<UserID, User<S>> {
    return this.listable().filter((u) => u !== user);
  }

  admins(): Collection<UserID, User<S>>;
  admins(
    user: User<S>,
    exclude: boolean,
    respectignores: boolean
  ): Collection<UserID, User<S>>;
  admins(
    user?: User<S>,
    exclude?: boolean,
    respectignores?: boolean
  ): Collection<UserID, User<S>> {
    return this.inside().filter(
      (u) =>
        (!user ||
          ((!exclude || u !== user) &&
            (!respectignores || !u.ignores.has(user)))) &&
        u.admin
    );
  }

  verbose(): Collection<UserID, User<S>>;
  verbose(
    user: User<S>,
    exclude: boolean,
    respectignores: boolean
  ): Collection<UserID, User<S>>;
  verbose(
    user?: User<S>,
    exclude?: boolean,
    respectignores?: boolean
  ): Collection<UserID, User<S>> {
    return (
      user ? this.admins(user, exclude!, respectignores!) : this.admins()
    ).filter((u) => u.data.verbose);
  }

  remove<U extends User<S>>(user: U): U {
    this.list.delete(user?.id ?? user);
    return user;
  }
  add<U extends User<S>>(user: U): U {
    this.list.set(user.id, user);
    return user;
  }
}
