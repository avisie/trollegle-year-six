import { dict } from "../../data/dict.ts";
import { random } from "../../util/array.ts";
import { PartialAccountData } from "../accounts/mod.ts";
import { Sources } from "../sources/types.ts";
import { Trollegle } from "../trollegle/mod.ts";

export type NickListOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  defaultNicks: string[];
};

export class NickList<S extends Sources> {
  trollegle: Trollegle<S>;

  readonly default: string[];
  freeDefaults: string[];
  taken: string[] = [];

  constructor({ trollegle, defaultNicks }: NickListOptions<S>) {
    this.trollegle = trollegle;

    this.default = [...defaultNicks];
    this.freeDefaults = [...defaultNicks];
  }

  filterFn(nick: string) {
    const low = nick.toLowerCase(),
      up = nick.toUpperCase();
    return (n: string) => n.toLowerCase() === low || n.toUpperCase() === up;
  }

  fix(nick: string): string {
    nick = nick.replace(dict.regex.whitespacesandspaces, " ");

    return nick;
  }

  isValid(nick: string): boolean {
    if (dict.regex.whitespacesandspaces.test(nick)) return false;
    if (nick.includes(dict.symbols.registered)) return false;

    const { minlength, maxlength } = this.trollegle.config.account.username;
    if (nick.length < minlength || nick.length > maxlength) return false;

    return true;
  }

  isTaken(nick: string, oldnick?: string): boolean {
    const defnick: string | undefined = this.default.find(this.filterFn(nick)),
      takenindex = this.taken.findIndex(this.filterFn(nick)),
      nodifference =
        typeof oldnick === "string" && this.filterFn(nick)(oldnick);

    if (!(takenindex < 0))
      if (nodifference) return true;
      else return false;

    if (defnick && this.freeDefaults.findIndex(this.filterFn(defnick)) < 0)
      return false;

    const found = this.trollegle.accounts.lookup(nick);
    if (found && !nodifference) return false;

    return true;
  }

  take(
    nick?: string,
    oldnick?: string,
    account?: PartialAccountData
  ): string | false {
    const defnick: string | undefined =
      typeof nick === "string"
        ? this.default.find(this.filterFn(nick))
        : random(this.freeDefaults);

    if (typeof defnick !== "string" && typeof nick !== "string") return false;

    const newnick = (nick || defnick)!,
      takenindex = this.taken.findIndex(this.filterFn(newnick)),
      nodifference =
        typeof oldnick === "string" &&
        typeof nick === "string" &&
        this.filterFn(nick)(oldnick),
      found = this.trollegle.accounts.lookup(newnick);
    if (!(takenindex < 0)) {
      if (nodifference) {
        if (oldnick) this.free(oldnick);
        return (this.taken[takenindex] = newnick);
      }
      return false;
    }

    if (defnick) {
      const f = this.filterFn(defnick),
        freeindex = this.freeDefaults.findIndex(f);
      if (freeindex < 0) return false;
      const deleted = this.freeDefaults.splice(freeindex, 1)[0];
      if (typeof deleted !== "string")
        throw new Error(`am got no idea '${nick}' def '${newnick}'`);
      if (!f(deleted))
        throw new Error(`deleted nick not same '${nick}' def '${newnick}'`);
    }

    if (found && found.id !== account?.id) return false;

    if (oldnick) this.free(oldnick);
    this.taken.push(newnick);
    return newnick;
  }

  free(nick: string): void {
    let n: string | undefined = this.default.find(this.filterFn(nick)),
      isDefault = false;
    if (typeof n === "string") {
      isDefault = true;
      if (!(this.freeDefaults.indexOf(n) < 0)) return;
    } else n = nick;

    const f = this.filterFn(n),
      takenindex = this.taken.findIndex(f);
    if (takenindex < 0) return;
    const deleted = this.taken.splice(takenindex, 1)[0];
    if (typeof deleted !== "string")
      throw new Error(`am got no idea '${nick}' def '${n}' `);
    if (!f(deleted))
      throw new Error(`deleted nick not same '${nick}' def '${n}'`);
    if (isDefault) this.freeDefaults.push(n);
    return;
  }
}
