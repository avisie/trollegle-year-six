import { dict } from "../../data/dict.ts";

import { formatNick } from "../../util/dict.ts";
import { fstring } from "../../util/string.ts";
import { equals } from "../../util/array.ts";
import { minsec } from "../../util/number.ts";

import { PartialAccountData } from "../accounts/mod.ts";
import { Mothership, Pulse } from "../mothership/mod.ts";
import { Sources, BaseSource, CoreEvents } from "../sources/types.ts";
import { Trollegle, UserID } from "../trollegle/mod.ts";
import { deepproxify, isRecord } from "../../util/object.ts";

export type ActionData = {
  received: number;
  sent: number;
};
export type Actions = {
  pat: ActionData;
  8: number;
};

export function isMutualAction(obj: unknown): obj is ActionData {
  if (!isRecord(obj)) return false;
  if (!("received" in obj && "sent" in obj)) return false;

  const { received, sent } = obj;
  if (!(typeof received === "number" && typeof sent === "number")) return false;

  return true;
}

export type Cooldowns = {
  pat: number;
};
export type ReputationData = {
  good: string[];
  bad: string[];
};

export type UserData = {
  dids: keyof (typeof dict)["format"]["dids"];
  style: keyof (typeof dict)["format"]["message"]["user"];
  specialstyle: keyof (typeof dict)["format"]["message"]["specialuser"];
  verbose: boolean;
  balance: number;
  actions: Actions;
  cooldowns: Cooldowns;
  reputation: ReputationData;
  createdat: number | null;
  lastlogin: number | null;
};
export const defaultUserData: UserData = {
  dids: "none",
  style: "default",
  specialstyle: "default",
  verbose: false,
  balance: 0,
  actions: {
    pat: {
      received: 0,
      sent: 0,
    },
    8: 0,
  },
  cooldowns: {
    pat: 0,
  },
  reputation: {
    good: [],
    bad: [],
  },
  createdat: null,
  lastlogin: null,
};

export function isUserData(obj: unknown): obj is UserData {
  if (!isRecord(obj)) return false;

  const {
    dids,
    style,
    specialstyle,
    actions,
    cooldowns,
    reputation,
    createdat,
    lastlogin,
  } = obj;
  if (
    !(
      typeof dids === "string" &&
      typeof style === "string" &&
      typeof specialstyle === "string" &&
      isRecord(actions) &&
      isRecord(cooldowns) &&
      isRecord(reputation) &&
      (typeof createdat === "number" || createdat === null) &&
      (typeof lastlogin === "number" || lastlogin === null)
    )
  )
    return false;

  if (
    !(
      dids in dict.format.dids &&
      style in dict.format.message.user &&
      specialstyle in dict.format.message.specialuser
    )
  )
    return false;

  const { pat, 8: eight } = actions;
  if (!(isRecord(pat) && typeof eight === "number")) return false;
  if (!isMutualAction(pat)) return false;

  if (!(isRecord(cooldowns) && typeof cooldowns.pat === "number")) return false;

  if (
    !(
      isRecord(reputation) &&
      Array.isArray(reputation.good) &&
      Array.isArray(reputation.bad)
    )
  )
    return false;

  return true;
}

if (!isUserData(defaultUserData))
  throw new Error("user data validation failed for default user data");

export type UserOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  connection: BaseSource<keyof S>;
  id: UserID;
  nick: string;
  mothership?: Mothership<S>;
};

export class User<S extends Sources> {
  /**
   * The Trollegle the user is a part of.
   */
  trollegle: Trollegle<S>;

  /**
   * The user's connection.
   */
  connection: BaseSource<keyof S, CoreEvents>;

  /**
   * The user's identifier.
   */
  id: UserID;

  /**
   * A set of IDs of users this user prefers to shut up.
   */
  ignores: Set<User<S>> = new Set();

  /**
   * The user's assigned mothership, depending on its connection.
   */
  mothership?: Mothership<S>;

  /**
   * The user's optional pulse.
   */
  pulse?: Pulse<S>;

  /**
   * The user's nickname.
   */
  nick: string;

  /**
   * A timeout for taking down the user's optional pulse.
   */
  decaytimeout?: number;

  /**
   * Timestamp of when the user connected.
   */
  birthedat?: number;

  /**
   * Timestamp of when the user actually entered the room.
   */
  joinedat?: number;

  /**
   * The reason the user left, provided when kicked.
   */
  leavereason?: string;

  /**
   * Whether or not the user is in their toothpaste phase.
   */
  unconfirmed = true;

  /**
   * How many future messages to check for taileating.
   */
  tailcheck = 2;

  /**
   * The amount of messages the user had sent within a flood check interval.
   */
  floodcount = 0;

  /**
   * Whether or not the user had received the flood warning message.
   */
  floodwarned = false;

  /**
   * An array of current flood count decrement timeouts.
   */
  floodtimeouts: number[] = [];

  /**
   * Whether or not the user is muted for regular users.
   */
  muted = false;

  /**
   * Whether or not the user is an admin. Wow.
   */
  admin = false;

  /**
   * The user's data.
   */
  data!: UserData;

  /**
   * Whether or not the user is currently in the process of registering or logging into an account.
   */
  loggingin = false;

  /**
   * The user's account information if they are logged in.
   */
  account?: PartialAccountData;

  get lastactive(): number | undefined {
    return this.connection.lastactive;
  }

  get idleage(): number | undefined {
    const { lastactive } = this;
    if (lastactive === undefined) return undefined;
    return Date.now() - lastactive;
  }
  get birthage(): number | undefined {
    const { birthedat } = this;
    if (birthedat === undefined) return undefined;
    return Date.now() - birthedat;
  }
  get joinage(): number | undefined {
    const { joinedat } = this;
    if (joinedat === undefined) return undefined;
    return Date.now() - joinedat;
  }
  get agestring() {
    return this.birthedat
      ? fstring(dict.connection.age, minsec(this.birthage || 0))
      : fstring(
          dict.connection[this.lastactive ? "wage" : "zage"],
          minsec(this.idleage || 0)
        );
  }
  get conninfo() {
    const { interests, commonlikes } = this.connection;

    if (commonlikes) return commonlikes.join(", ");

    if (!interests) return dict.connection.tmode;
    const defaultinterests = this.trollegle.config.pulse.defaultinterests;
    if (defaultinterests && equals(interests, defaultinterests))
      return dict.connection.dints;
    return (
      interests.slice(0, 7).join(", ") + (interests.length > 7 ? " [...]" : "")
    );
  }
  get info() {
    return `${this.conninfo}, ${this.agestring}`;
  }
  get trivia(): string {
    return `${this.nick} (${this.info})`;
  }

  constructor({ trollegle, connection, id, nick, mothership }: UserOptions<S>) {
    this.trollegle = trollegle;
    this.connection = connection;
    this.id = id;
    this.nick = nick;
    mothership && (this.mothership = mothership);

    this.setData(structuredClone(defaultUserData));
  }

  setData(data: UserData) {
    this.data = deepproxify(data, () => this.account && this.saveData());
  }
  saveData() {
    const { account } = this;
    if (!account) throw new Error("no account to save data to");

    this.trollegle.accounts.save(account.userlow, this.data);
  }

  serverMessage(msg: string): Promise<boolean> {
    return this.connection.send(fstring(dict.format.message.server, msg));
  }

  userMessage(user: User<S>, msg: string): Promise<boolean> {
    return this.connection.send(
      fstring(
        dict.format.message.user[this.data.style],
        formatNick(user, this.data.dids),
        msg
      )
    );
  }

  actionMessage(user: User<S>, msg: string): Promise<boolean> {
    return this.connection.send(
      fstring(dict.format.message.action, formatNick(user, this.data.dids), msg)
    );
  }

  specialUserMessage(
    user: User<S>,
    msg: string,
    tag: string
  ): Promise<boolean> {
    return this.connection.send(
      fstring(
        dict.format.message.specialuser[this.data.specialstyle],
        formatNick(user, this.data.dids),
        msg,
        tag
      )
    );
  }

  kick(reason?: string): Promise<boolean | undefined> {
    this.leavereason = reason;
    return this.connection.disconnect(true);
  }
}
