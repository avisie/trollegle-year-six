import { deepproxify, isRecord } from "../../util/object.ts";
import { Strictly } from "../../util/types.ts";
import { PartialAccountData } from "../accounts/mod.ts";
import { PulseState, isPulseState } from "../mothership/mod.ts";
import { Sources } from "../sources/types.ts";
import { Trollegle } from "../trollegle/mod.ts";
import { UserData, isUserData } from "../userlist/user.ts";

export type UserState<S extends Sources> = {
  source: keyof S;
  resurrectdata: Record<PropertyKey, any>;
  ignores: number[];
  mothership: string | null;
  pulse: PulseState | null;
  nick: string;
  birthedat: number | null;
  joinedat: number | null;
  leavereason: string | null;
  unconfirmed: boolean;
  tailcheck: number;
  floodwarned: boolean;
  muted: boolean;
  admin: boolean;
  data: UserData;
  account: PartialAccountData | null;
};

export type TrollegleState<S extends Sources> = Strictly<{
  adminbirthedat: number;
  lastinvite: number | null;
  users: Record<string, UserState<S>>;
}>;

export const defaultState: TrollegleState<Sources> = {
  adminbirthedat: Date.now(),
  lastinvite: null,
  users: {},
};

export type StateKeeperOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  path: string;
  savedelayms: number;
};

export class StateKeeper<S extends Sources> {
  trollegle: Trollegle<S>;
  path: string;

  data: TrollegleState<S>;

  savedelayms: number;
  private savetimeout?: number;

  constructor({ trollegle, path, savedelayms }: StateKeeperOptions<S>) {
    this.trollegle = trollegle;
    this.path = path;

    let existed = false,
      data: TrollegleState<S> = structuredClone(defaultState);
    try {
      const parsed = JSON.parse(Deno.readTextFileSync(path).trim());
      if (!this.validate(parsed))
        throw new Error(
          `trollegle state file at ${path} did not pass validation`
        );
      data = parsed;
      existed = true;
    } catch (error) {
      if (!(error instanceof Deno.errors.NotFound)) throw error;
    }

    this.data = deepproxify(data, () => {
      this.schedulesave();
    });
    this.savedelayms = savedelayms;

    if (!existed) this.save();
  }

  validate(data: unknown): data is TrollegleState<S> {
    if (!isRecord(data)) return false;

    const { adminbirthedat, lastinvite, users } = data;
    if (
      !(
        typeof adminbirthedat === "number" &&
        (typeof lastinvite === "number" || lastinvite === null) &&
        isRecord(users)
      )
    )
      return false;

    const { trollegle } = this,
      ids: Set<number> = new Set();
    for (const key of Object.keys(users)) {
      if (!/^\d+$/.test(key)) return false;
      const id = +key;
      if (ids.size === ids.add(id).size) return false;

      const state = users[key];
      if (!isRecord(state)) return false;

      const {
        source,
        resurrectdata,
        ignores,
        mothership,
        pulse,
        nick,
        birthedat,
        joinedat,
        leavereason,
        unconfirmed,
        tailcheck,
        floodwarned,
        muted,
        admin,
        data,
        account,
      } = state;
      if (
        !(
          typeof source === "string" &&
          isRecord(resurrectdata) &&
          Array.isArray(ignores) &&
          (typeof mothership === "string" || mothership === null) &&
          (isRecord(pulse) || pulse === null) &&
          typeof nick === "string" &&
          (typeof birthedat === "number" || birthedat === null) &&
          (typeof joinedat === "number" || joinedat === null) &&
          (typeof leavereason === "string" || leavereason === null) &&
          typeof unconfirmed === "boolean" &&
          typeof tailcheck === "number" &&
          typeof floodwarned === "boolean" &&
          typeof muted === "boolean" &&
          typeof admin === "boolean" &&
          isRecord(data) &&
          (isRecord(account) || account === null)
        )
      )
        return false;

      if (!trollegle.hasSource(source)) return false;

      if (!trollegle.sources[source].isValidResurrectData(resurrectdata))
        return false;

      if (!(ignores.findIndex((e) => typeof e !== "number") < 0)) return false;

      if (mothership !== null && trollegle.mothership.url !== mothership)
        return false;

      if (pulse !== null && !isPulseState(pulse)) return false;

      if (!isUserData(data)) return false;

      if (
        account &&
        !(
          typeof account.id === "number" &&
          typeof account.user === "string" &&
          typeof account.userlow === "string"
        )
      )
        return false;
    }

    return true;
  }

  schedulesave(): boolean {
    if (this.savetimeout) return false;

    this.savetimeout = setTimeout(() => {
      this.savetimeout = undefined;
      this.save();
    }, this.savedelayms);

    return true;
  }

  save() {
    Deno.writeTextFileSync(this.path, JSON.stringify(this.data));
  }
}
