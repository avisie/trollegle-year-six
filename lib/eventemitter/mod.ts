// Taken from https://deno.land/x/eventemitter
// GitHub @ihack2712/eventemitter

// deno-lint-ignore-file no-explicit-any
/** The callback type. */
type Callback = (...args: any[]) => void;

/** A listener type. */
type Listener = Callback & { __once__?: true };

/** The name of an event. */
type EventName = string | number;

export type EventsType = { [key: EventName]: Callback };

/** Utility method to remove array elements */
const del = (array: any[], element: any) => {
  const i = array.indexOf(element);
  i < 0 || array.splice(i, 1).shift();
};

/**
 * The event emitter.
 */
export class EventEmitter<E extends EventsType = EventsType> {
  /**
   * This is where the events and listeners are stored.
   */
  _events_: Map<EventName, Array<Listener>> = new Map();

  /**
   * Listen for a typed event.
   * @param event The typed event name to listen for.
   * @param listener The typed listener function.
   */
  public on<K extends keyof E>(
    event: K,
    listener: E[K],
    prepend?: boolean
  ): this;

  /**
   * Listen for an event.
   * @param event The event name to listen for.
   * @param listener The listener function.
   */
  public on(event: EventName, listener: Callback, prepend = false): this {
    this._events_.has(event) || this._events_.set(event, new Array<Listener>());
    prepend
      ? this._events_.get(event)!.unshift(listener)
      : this._events_.get(event)!.push(listener);
    return this;
  }

  /**
   * Listen for a typed event once.
   * @param event The typed event name to listen for.
   * @param listener The typed listener function.
   */
  public once<K extends keyof E>(
    event: K,
    listener: E[K],
    prepend?: boolean
  ): void;

  /**
   * Listen for an event once.
   * @param event The event name to listen for.
   * @param listener The listener function.
   */
  public once(event: EventName, listener: Callback, prepend = false): void {
    const l: Listener = listener;
    l.__once__ = true;
    this.on(event, l as E[typeof event], prepend);
  }

  /**
   * Remove a specific listener in the event emitter on a specific
   * typed event.
   * @param event The typed event name.
   * @param listener The typed event listener function.
   */
  public off<K extends keyof E>(event: K, listener: E[K]): this;

  /**
   * Remove all listeners on a specific typed event.
   * @param event The typed event name.
   */
  public off<K extends keyof E>(event: K): this;

  /**
   * Remove all events from the event listener.
   */
  public off(): this;

  /**
   * Remove a specific listener on a specific event if both `event`
   * and `listener` is defined, or remove all listeners on a
   * specific event if only `event` is defined, or lastly remove
   * all listeners on every event if `event` is not defined.
   * @param event The event name.
   * @param listener The event listener function.
   */
  public off(event?: EventName, listener?: Callback): this {
    if (event == null && listener)
      throw new Error("Why is there a listener defined here?");
    else if ((event === undefined || event === null) && !listener)
      this._events_.clear();
    else if (event && !listener) this._events_.delete(event);
    else if (event && listener && this._events_.has(event)) {
      const _ = this._events_.get(event)!;
      del(_, listener);
      if (!_.length) this._events_.delete(event);
    } else;
    return this;
  }

  /**
   * Emit a typed event without waiting for each listener to
   * return.
   * @param event The typed event name to emit.
   * @param args The arguments to pass to the typed listeners.
   */
  public emitSync<K extends keyof E>(event: K, ...args: Parameters<E[K]>): this;

  /**
   * Emit an event without waiting for each listener to return.
   * @param event The event name to emit.
   * @param args The arguments to pass to the listeners.
   */
  public emitSync(event: EventName, ...args: Parameters<Callback>): this {
    if (!this._events_.has(event)) return this;
    const _ = this._events_.get(event)!,
      listeners = _.slice(0);
    for (const listener of listeners)
      if (listener.__once__) {
        delete listener.__once__;
        del(_, listener);
      }
    _.length || this._events_.delete(event);
    for (const listener of listeners) listener(...args);
    return this;
  }

  /**
   * Emit a typed event and wait for each typed listener to return.
   * @param event The typed event name to emit.
   * @param args The arguments to pass to the typed listeners.
   */
  public async emit<K extends keyof E>(
    event: K,
    ...args: Parameters<E[K]>
  ): Promise<this>;

  /**
   * Emit an event and wait for each listener to return.
   * @param event The event name to emit.
   * @param args The arguments to pass to the listeners.
   */
  public async emit(
    event: EventName,
    ...args: Parameters<Callback>
  ): Promise<this> {
    if (!this._events_.has(event)) return this;
    const _ = this._events_.get(event)!,
      listeners = _.slice(0);
    for (const listener of listeners)
      if (listener.__once__) {
        delete listener.__once__;
        del(_, listener);
      }
    _.length || this._events_.delete(event);
    for (const listener of listeners) await listener(...args);
    return this;
  }

  /**
   * The same as emitSync, but wait for each typed listener to
   * return before calling the next typed listener.
   * @param event The typed event name.
   * @param args The arguments to pass to the typed listeners.
   */
  public queue<K extends keyof E>(event: K, ...args: Parameters<E[K]>): this;

  /**
   * The same as emitSync, but wait for each listener to return
   * before calling the next listener.
   * @param event The event name.
   * @param args The arguments to pass to the listeners.
   */
  public queue(event: EventName, ...args: Parameters<Callback>): this {
    return (
      (async () =>
        await this.emit(event, ...(args as Parameters<E[typeof event]>)))(),
      this
    );
  }

  /**
   * Wait for a typed event to be emitted and return the arguments.
   * @param event The typed event name to wait for.
   * @param timeout An optional amount of milliseconds to wait
   * before throwing.
   */
  public pull<K extends keyof E>(
    event: K,
    timeout?: number
  ): Promise<Parameters<E[K]>>;
  /**
   * Wait for an event to be emitted and return the arguments.
   * @param event The event name to wait for.
   * @param timeout An optional amount of milliseconds to wait
   * before throwing.
   */
  public pull(
    event: EventName,
    timeout?: number
  ): Promise<Parameters<Callback>> {
    return new Promise((resolve, reject) => {
      const listener = (...args: any[]) => {
        timeoutId === null || clearTimeout(timeoutId);
        resolve(args);
      };

      const timeoutId =
        typeof timeout == "number"
          ? setTimeout(
              () => (
                this.off(event, listener as E[typeof event]),
                reject(new Error("Timed out!"))
              )
            )
          : null;

      this.once(event, listener as E[typeof event]);
    });
  }

  /**
   * Wait for an event to be emitted and return the arguments.
   * Rejects if the event is not received before the event.
   * @param event The event name to wait for.
   * @param interruptor An optional interrupting event which will cause a rejection with its arguments.
   * @returns The resulting values of the awaited event.
   */
  waitfor<K1 extends keyof E, K2 extends Exclude<keyof E, K1>>(
    event: K1,
    interruptor?: K2
  ): Promise<Parameters<E[K1]>> {
    return new Promise((resolve, reject) => {
      let interrupt: (
        ...args: Parameters<E[NonNullable<typeof interruptor>]>
      ) => void;
      const listener = (...args: Parameters<E[typeof event]>) => (
        interrupt &&
          this.off(
            interruptor!,
            interrupt as E[NonNullable<typeof interruptor>]
          ),
        resolve(args)
      );

      if (typeof interruptor === "string")
        (interrupt = (...args: any[]) => (
          this.off(event, listener as E[typeof event]), reject(args)
        )),
          this.once(
            interruptor,
            interrupt as E[NonNullable<typeof interruptor>]
          );
      this.once(event, listener as E[typeof event]);
    });
  }
}
