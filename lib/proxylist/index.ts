import { chop } from "../../util/array.ts";
import { genrid } from "../sources/omegle.ts";
import {
  BaseProxiedSource,
  BaseProxiedSourceConstructor,
  CoreEvents,
  ProxyConnectOptions,
  Sources,
} from "../sources/types.ts";
import { Trollegle } from "../trollegle/mod.ts";

export type ProxyListOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  urls?: URL[];
};

export class ProxyList<S extends Sources> {
  trollegle: Trollegle<S>;

  list: ConnectionProxy<S>[] = [];
  get size() {
    return this.list.length;
  }

  constructor({ trollegle, urls }: ProxyListOptions<S>) {
    this.trollegle = trollegle;

    if (urls) for (const proxy of urls) this.addURL(proxy);
  }

  add(proxy: ConnectionProxy<S>) {
    this.list.push(proxy);
  }
  addURL(url: URL) {
    this.list.push(new ConnectionProxy({ url: url, list: this }));
  }

  sort() {
    return this.list.sort((a, b) => {
      const connections = a.connections.size - b.connections.size;
      if (connections !== 0) return connections;

      const banned = +b.banned - +a.banned;
      if (banned !== 0) return banned;

      return a.lastused - b.lastused;
    });
  }

  [Symbol.iterator](): Iterator<ConnectionProxy<S>> {
    const now = Date.now(),
      array = [...this.sort()].filter(
        (proxy) =>
          !proxy.banned ||
          now - proxy.lastused > this.trollegle.config.proxy.bantimems
      );
    let i = 0;
    return {
      next: () =>
        i < array.length
          ? { value: array[i++], done: false }
          : { done: true, value: undefined },
    };
  }
}

type ConnectionProxyOptions<S extends Sources> = {
  url: URL;
  list: ProxyList<S>;
};
export class ConnectionProxy<S extends Sources> {
  readonly client: Deno.HttpClient;
  readonly url: URL;
  readonly list: ProxyList<S>;

  banned = false;
  identifier?: string;

  connections: Set<BaseProxiedSource<keyof S, CoreEvents>> = new Set();
  lastused = 0;

  get host() {
    return this.url.host;
  }

  constructor({ url, list }: ConnectionProxyOptions<S>) {
    this.url = url;

    const { protocol, host, username, password } = url,
      clientoptions: Deno.CreateHttpClientOptions = {
        proxy: {
          url: `${protocol}//${host}`,
        },
      };
    if (username)
      clientoptions.proxy!.basicAuth = {
        username: username,
        password: password,
      };

    this.client = Deno.createHttpClient(clientoptions);
    this.list = list;
  }

  ban(): this {
    this.list.trollegle.broadServerAdmins(`Proxy ${this.host} is banned`);
    this.banned ||= true;
    return this;
  }

  use(conn: BaseProxiedSource<keyof S, CoreEvents>): this {
    if (this.connections.has(conn))
      throw new Error("proxy already has connection that tried to use it");
    this.connections.add(conn);
    this.lastused = Date.now();
    return this;
  }

  end(conn: BaseProxiedSource<keyof S, CoreEvents>): this {
    if (!this.connections.has(conn))
      throw new Error("proxy does not have connection that tried to end it");
    this.connections.delete(conn);
    return this;
  }

  async getidentifier(
    constructor: BaseProxiedSourceConstructor<any, CoreEvents>
  ): Promise<string | false | null> {
    const conn1 = new constructor(),
      conn2 = new constructor(),
      opts: ProxyConnectOptions = {
        interests: [`${this.host}::${Math.random()}`],
        proxy: this.url,
        client: this.client,
        randid: genrid(),
      };

    const digests = conn1.waitfor("identDigests").then(
      ([digests]) => {
        conn1.disconnect().catch(() => {});
        conn2.disconnect().catch(() => {});

        if (!digests) throw new Error("no digests");

        const identifiers = digests.split(",");
        if (identifiers.length !== 4)
          throw new Error(
            `weird identifiers '${digests}', "${identifiers.join('", "')}"`
          );

        const chopped = chop(identifiers, 2);
        if (chopped[0][0] !== chopped[1][0] || chopped[0][1] !== chopped[1][1])
          throw new Error(`identifiers inequal ${digests}, ${chopped}`);

        return identifiers;
      },
      (e: unknown) => {
        console.error("conn1 waitfor", e);
        conn1.disconnect().catch(() => {});
        conn2.disconnect().catch(() => {});
      }
    );

    const success1 = await conn1
      .connect(opts)
      .catch((e: unknown) => (conn1.off(), console.error("conn1", e)));

    if (!success1) return null;
    conn1.once("end", () => conn1.off());

    const success2 = await conn2
      .connect(opts)
      .catch(
        (e: unknown) => (conn1.off(), conn2.off(), console.error("conn2", e))
      );
    if (!success2) return false;
    conn2.once("end", () => conn2.off());

    const identifier = (await digests)?.[0];
    if (typeof identifier !== "string") return null;

    if (this.identifier !== identifier) this.identifier = identifier;
    return identifier;
  }

  async check(
    constructor: BaseProxiedSourceConstructor<keyof S, CoreEvents>
  ): Promise<boolean> {
    const identifier = await this.getidentifier(constructor).catch(
      console.error
    );

    if (!identifier) return false;
    return true;
  }
}
