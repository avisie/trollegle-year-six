import { Strictly } from "../../util/types.ts";

import {
  BaseDummySource,
  BaseSourceConstructor,
  CoreEvents,
  SourceID,
  Sources,
} from "../sources/types.ts";
import { UserList } from "../userlist/mod.ts";
import { Mothership, Pulse } from "../mothership/mod.ts";
import { CLI } from "../cli/mod.ts";

import { CommandHandler } from "../commandhandler/mod.ts";
import { ProxyList } from "../proxylist/index.ts";

import { dict } from "../../data/dict.ts";
import { fstring } from "../../util/string.ts";
import { formatNick } from "../../util/dict.ts";
import { StateKeeper, UserState } from "../statekeeper/mod.ts";
import { AccountManager } from "../accounts/mod.ts";
import { User, isUserData } from "../userlist/user.ts";
import { prepareToothpaste } from "../../events/connected.ts";
import { onceend } from "../../events/end.ts";
import { ongotMessage } from "../../events/gotMessage.ts";
import { Dummy } from "../sources/dummy.ts";
import { decode as decodehex } from "https://deno.land/std@0.176.0/encoding/hex.ts";

export type UserID = number;

export type Interests = string[];

export type MothershipInitializer<S extends Sources> = {
  sourceid: keyof S;
  url: string;
  chatdab: number;
};

export type RawTrollegleConfig = Strictly<{
  user: {
    invitedelayms: number;
    toothpaste: boolean;
    toothtries: number;
  };
  account: {
    username: {
      minlength: number;
      maxlength: number;
    };
    password: {
      minlength: number;
      maxlength: number;
      salt: string;
      iterations: number;
      bitlength: number;
    };
  };
  message: {
    flood: {
      maxcount: number;
      timems: number;
    };
  };
  proxy: {
    bantimems: number;
  };
  pulse: {
    decaytimems: number;
    defaultinterests: string[] | null;
  };
}>;

export type TrollegleConfig = Strictly<{
  user: {
    invitedelayms: number;
    toothpaste: boolean;
    toothtries: number;
  };
  account: {
    username: {
      minlength: number;
      maxlength: number;
    };
    password: {
      minlength: number;
      maxlength: number;
      salt: Uint8Array;
      iterations: number;
      bitlength: number;
    };
  };
  message: {
    flood: {
      maxcount: number;
      timems: number;
    };
  };
  proxy: {
    bantimems: number;
  };
  pulse: {
    decaytimems: number;
    defaultinterests: string[] | null;
  };
}>;

export type TrollegleOptions<S extends Sources> = {
  cli: CLI;
  config: RawTrollegleConfig;
  statefile: string;
  sources: S;
  proxies?: URL[];
  commandPath: string;
  defaultNicks: string[];
  userInitializer: UserInitializer<S>;
  mothership: MothershipInitializer<S>;
};

export type InviteOptions<S extends Sources> = {
  sourceid: keyof S;
  pulse: boolean;
};
export type UserInitializer<S extends Sources> = (
  trollegle: Trollegle<S>,
  user: User<S>
) => any;

export type MessageCallback<S extends Sources> = (user: User<S>) => string;

const encode = TextEncoder.prototype.encode.bind(new TextEncoder());
export function parseConfig(raw: RawTrollegleConfig): TrollegleConfig {
  const result = structuredClone(raw);

  result.account.password.salt = decodehex(encode(raw.account.password.salt));

  return result as TrollegleConfig;
}

const maxwordlength = 24,
  longwordregex = new RegExp(`(\\S{${maxwordlength}})(?=\\S)`, "g"),
  longwordsplitter = " ";
export class Trollegle<S extends Sources> {
  cli: CLI;

  config: TrollegleConfig;
  state: StateKeeper<S>;

  sources: S;
  proxies: ProxyList<S>;
  commands: CommandHandler;

  users: UserList<S>;
  userInitializer: UserInitializer<S>;

  accounts: AccountManager<S> = new AccountManager({ trollegle: this });

  mothership: Mothership<S>;

  killed = false;

  constructor({
    cli,
    config,
    statefile,
    sources,
    proxies,
    commandPath,
    defaultNicks,
    userInitializer,
    mothership,
  }: TrollegleOptions<S>) {
    this.cli = cli;

    if (config.account.password.salt.length < 32) {
      cli.log(
        "config.json: account.password.salt has a length less than 32. this is an important password security part!"
      );
      cli.log(
        "config.json: make sure account.password.salt contains at least 32 characters 0-9, a-f, and that the amount is even."
      );
      cli.log(
        "config.json: check README.md if unsure - once done, DO NOT SHARE THESE NUMBERS WITH ANYONE"
      );
      cli.close();
      Deno.exit(0);
    }
    if (config.account.password.iterations < 100000) {
      cli.log(
        "config.json: account.password.iterations should at least be 100000"
      );
      cli.log(
        "config.json: it is recommended to set this value to a number greater than 300000"
      );
      cli.log(
        "config.json: but the higher it is, the longer operations like /register and /login take to complete"
      );
      cli.close();
      Deno.exit(0);
    }
    if (
      config.account.password.bitlength < 512 ||
      config.account.password.bitlength % 8
    ) {
      cli.log(
        "config.json: account.password.bitlength should at least be 512 and must be a multiple of 8"
      );
      cli.log(
        "config.json: a recommended value for this property is 4096, which means password hashes will be quite long"
      );
      cli.close();
      Deno.exit(0);
    }
    this.config = parseConfig(config);

    if (typeof mothership.chatdab !== "number") {
      cli.log(
        "trollegle.ts: a value for the 'chatdab' mothership property was not provided"
      );
      cli.log(
        "trollegle.ts: this is the thing that defines your room name. do not share once set!"
      );
      cli.close();
      Deno.exit(0);
    }

    this.sources = sources;

    this.proxies = new ProxyList({ trollegle: this, urls: proxies });
    this.commands = new CommandHandler({
      communicate: this.cli.log.bind(cli),
      path: commandPath,
    });

    this.users = new UserList({ trollegle: this, defaultNicks });
    this.userInitializer = userInitializer;

    this.mothership = new Mothership({
      trollegle: this,
      ...mothership,
    });

    this.state = new StateKeeper({
      trollegle: this,
      path: statefile,
      savedelayms: 5000,
    });
  }

  async init() {
    if (!(await this.commands.init()))
      throw new Error("failed to initialize commands");

    const { users } = this,
      ignoremap: Record<string, number[]> = {},
      resurrected: User<S>[] = [];
    for (const [idstr, state] of Object.entries(this.state.data.users)) {
      const {
          source,
          resurrectdata,
          nick,
          mothership,
          pulse,
          ignores,
          birthedat,
          joinedat,
          leavereason,
          unconfirmed,
          tailcheck,
          floodwarned,
          muted,
          admin,
          data,
          account,
        } = state,
        id = +idstr;
      if (isNaN(id)) throw new Error(`nan id ${idstr}`);

      const constructor = this.sources[source] as BaseSourceConstructor<
        keyof S,
        CoreEvents
      >;

      if (!constructor.isValidResurrectData(resurrectdata))
        throw new Error(`invalid resurrect data for ${idstr} ${nick}`);

      if (!isUserData(data))
        throw new Error(`invalid user data for ${idstr} ${nick}`);

      const conn = constructor.resurrect(resurrectdata),
        user = new User({
          trollegle: this,
          connection: conn,
          id,
          nick,
        });

      if (mothership !== null) user.mothership = this.mothership;
      if (pulse !== null) {
        if (!mothership)
          throw new Error(`no mothership but yes pulse for ${idstr} ${nick}`);

        const p = (user.pulse = new Pulse({
            mothership: this.mothership,
            word: pulse.word,
            name: pulse.name,
            user,
          })),
          time = (p.time = pulse.time);
        if (!(p.down = pulse.down)) {
          user.mothership!.pulses.set(time, p);
          user.decaytimeout = user.mothership!.createDecayTimeout(user, time);
        }
      }
      user.nick = nick;
      if (birthedat !== null) user.birthedat = birthedat;
      if (joinedat !== null) user.joinedat = joinedat;
      if (leavereason !== null) user.leavereason = leavereason;
      user.unconfirmed = unconfirmed;
      user.tailcheck = tailcheck;
      user.floodwarned = floodwarned;
      user.muted = muted;
      user.admin = admin;
      user.data = data;
      if (account !== null) {
        this.accounts.used.add(account.userlow);
        user.account = account;
      }

      users.list.set(id, user);

      (ignoremap[id] ||= []).push(...ignores);
    }

    for (const [idstr, list] of Object.entries(ignoremap)) {
      const id = +idstr;
      if (isNaN(id)) throw new Error(`got nan id ${idstr} from ignoremap`);

      const user = users.list.get(id);
      if (!user) throw new Error(`user ${id} not found from ignoremap`);

      for (const i of list) {
        const u = users.list.get(i);
        if (!u) throw new Error(`user ${i} not found from ignoremap for ${id}`);

        user.ignores.add(u);
      }

      resurrected.push(user);
    }

    for (const user of resurrected) {
      if (this.users.nicks.take(user.nick, undefined, user.account) === false)
        throw new Error("well fuck");

      const conn = user.connection;
      conn.once("end", onceend.bind(user));

      if (!conn.connected) conn.reset("died");
      else if (user.unconfirmed) {
        const commonlikes = user.connection.commonlikes;
        prepareToothpaste(
          user as User<Sources>,
          Promise.resolve(commonlikes && [["commonLikes", ...commonlikes]])
        );
      } else conn.on("gotMessage", ongotMessage.bind(user));
    }

    const listener = () => this.kill();
    globalThis.addEventListener("error", listener, {
      once: true,
    });
    globalThis.addEventListener("unhandledrejection", listener, {
      once: true,
    });

    if (!this.users.list.find((u) => u.connection instanceof Dummy))
      this.addUser({ sourceid: "dummy", pulse: false });
  }

  kill() {
    if (this.killed) return;

    const users: Record<string, UserState<any>> = {},
      { state } = this;
    for (const u of this.users.list.values()) {
      users[u.id] = {
        source: u.connection.constructor.id,
        resurrectdata: u.connection.serialize(),
        ignores: [...u.ignores].map((u) => u.id),
        mothership: u.mothership?.url ?? null,
        pulse: u.pulse?.serialize() ?? null,
        nick: u.nick,
        birthedat: u.birthedat ?? null,
        joinedat: u.joinedat ?? null,
        leavereason: u.leavereason ?? null,
        unconfirmed: u.unconfirmed,
        tailcheck: u.tailcheck,
        floodwarned: u.floodwarned,
        muted: u.muted,
        admin: u.admin,
        data: u.data,
        account: u.account ?? null,
      };
    }

    state.data.users = users;
    state.save();

    Deno.stdin.setRaw(false);
    this.killed = true;

    this.cli.log(
      "[known issue] you may have to press enter for the room to exit after a crash or graceful shutdown"
    );
    this.cli.setprefix("");
    this.cli.close();
    Deno.exit();
  }

  hasSource<ID extends SourceID>(sourceid: ID): this is Trollegle<Sources<ID>> {
    return Object.hasOwn(this.sources, sourceid);
  }

  addUser({
    sourceid,
    pulse,
  }: InviteOptions<S>): ReturnType<UserInitializer<S>> {
    const connection = new (this.sources[sourceid] as BaseSourceConstructor<
        keyof S,
        CoreEvents
      >)(),
      { users } = this,
      nick = users.nicks.take();
    if (!nick)
      return this.broadServerAdmins(
        "Tried getting a nickname for the next User, but failed miserably!"
      );
    const isadmin = connection instanceof BaseDummySource,
      user = new User<S>({
        trollegle: this,
        connection,
        id: users.getid(isadmin),
        nick,
        mothership: pulse ? this.mothership : undefined,
      });
    if (isadmin) user.admin = true;

    users.add(user);
    return this.userInitializer(this, user);
  }

  informJoined(user: User<S>, except?: User<S>) {
    for (const u of (except
      ? this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink(
          except,
          true,
          false
        )
      : this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink()
    ).values())
      u.serverMessage(
        fstring(dict.user.joined, formatNick(user, u.data.dids))
      ).catch(() => {});
  }

  informLeft(user: User<S>, except?: User<S>) {
    for (const u of (except
      ? this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink(
          except,
          true,
          false
        )
      : this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink()
    ).values())
      u.serverMessage(
        user.leavereason
          ? fstring(
              dict.user.leftreason,
              formatNick(user, u.data.dids),
              user.leavereason
            )
          : fstring(dict.user.left, formatNick(user, u.data.dids))
      ).catch(() => {});
  }

  informRenamed(user: User<S>, old: string, except?: User<S>) {
    for (const u of (except
      ? this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink(
          except,
          true,
          false
        )
      : this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink()
    ).values())
      u.serverMessage(fstring(dict.user.renamed, old, user.nick)).catch(
        () => {}
      );
  }

  broadAction(
    user: User<S>,
    msg: MessageCallback<S> | string,
    except?: User<S>
  ) {
    for (const u of (except
      ? this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink(
          except,
          true,
          false
        )
      : this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink()
    ).values())
      u.actionMessage(user, typeof msg === "string" ? msg : msg(u)).catch(
        () => {}
      );
  }

  broadServer(msg: MessageCallback<S> | string, except?: User<S>) {
    for (const u of (except
      ? this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink(
          except,
          true,
          false
        )
      : this.users.shouldbeabletoseemosttypesofuserandsystemmessagesithink()
    ).values())
      u.serverMessage(typeof msg === "string" ? msg : msg(u)).catch(() => {});
  }

  broadServerAdmins(msg: MessageCallback<S> | string, except?: User<S>) {
    for (const u of (except
      ? this.users.admins(except, true, true)
      : this.users.admins()
    ).values())
      u.serverMessage(typeof msg === "string" ? msg : msg(u)).catch(() => {});
  }

  broadSpecialAdmins(
    user: User<S>,
    msg: MessageCallback<S> | string,
    tag: string,
    except?: User<S>
  ) {
    for (const u of (except
      ? this.users.admins(except, true, true)
      : this.users.admins()
    ).values())
      u.specialUserMessage(
        user,
        typeof msg === "string" ? msg : msg(u),
        tag
      ).catch(() => {});
  }

  async repulse(user: User<S>): Promise<void> {
    if (!user.pulse) return;
    const { pulse } = user,
      { operation, down } = pulse;

    if (operation) await operation;
    if (down) return;

    await pulse.takedown();
    this.addUser({
      sourceid: user.connection.constructor.id as keyof S,
      pulse: true,
    });
  }

  checkMessage(user: User<S>, msg: string): string {
    let replaces = 0;
    msg = msg
      .replace(longwordregex, (_, b) => (replaces++, b + longwordsplitter))
      .trim();

    if (replaces)
      user.serverMessage(
        fstring(dict.error.toolong, replaces, longwordsplitter, maxwordlength)
      );

    return msg;
  }

  checkFlood(user: User<S>): boolean {
    const count = ++user.floodcount,
      { maxcount, timems } = this.config.message.flood,
      warned = user.floodwarned;

    if (count > maxcount) {
      const time = (timems / 1000).toFixed(1);

      for (const id of user.floodtimeouts) clearTimeout(id);
      user.floodcount = 0;

      if (warned) {
        user.kick(fstring(dict.flood.kicked, maxcount, time));
        return true;
      } else {
        user.serverMessage(
          fstring(dict.flood.warning, maxcount, time, user.nick)
        );
        user.floodwarned = true;
      }
    } else user.floodtimeouts.push(setTimeout(() => --user.floodcount, timems));

    return false;
  }
}
