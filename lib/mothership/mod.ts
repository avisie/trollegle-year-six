import { words } from "../../data/words.ts";
import { names } from "../../data/names.ts";

import { Collection } from "../collection/mod.ts";
import { Sources } from "../sources/types.ts";
import { Trollegle } from "../trollegle/mod.ts";
import { User } from "../userlist/user.ts";
import { isRecord } from "../../util/object.ts";

type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

export type Word = ArrayElement<typeof words>;
export type Name = ArrayElement<typeof names>;

export type PulseOptions<S extends Sources> = {
  mothership: Mothership<S>;
  word: Word;
  name: Name;
  user: User<S>;
};

export type PulseWords = {
  word: Word;
  name: Name;
};

export type MothershipOptions<S extends Sources> = {
  trollegle: Trollegle<S>;
  sourceid: keyof S;
  url: string;
  chatdab: number;
};

export type CreatePulseOptions<S extends Sources> = {
  user: User<S>;
  word: string;
  name: string;
};

export type PulseState = {
  word: string;
  name: string;
  time: number;
  down: boolean;
};

export function isPulseState(obj: unknown): obj is PulseState {
  if (!isRecord(obj)) return false;

  const { word, name, time, down } = obj;
  if (
    !(
      typeof word === "string" &&
      typeof name === "string" &&
      typeof time === "number" &&
      typeof down === "boolean"
    )
  )
    return false;

  return true;
}

export class Pulse<S extends Sources> {
  mothership: Mothership<S>;
  word: Word;
  name: Name;
  user: User<S>;

  time = Date.now();
  down = false;

  operation?: Promise<void>;

  serialize(): PulseState {
    return {
      word: this.word,
      name: this.name,
      time: this.time,
      down: this.down,
    };
  }

  constructor({ mothership, word, name, user }: PulseOptions<S>) {
    this.mothership = mothership;
    this.word = word;
    this.name = name;
    this.user = user;
  }

  async takedown(): Promise<boolean | null> {
    if (this.down) return null;
    const key = this.time;
    if (!this.mothership.pulses.has(key)) return false;

    await this.mothership.takedown(key);

    return true;
  }
}

const nameregex = /added: \d+, name: ([^]+)/;
export class Mothership<S extends Sources> {
  trollegle: Trollegle<S>;
  sourceid: keyof S;

  url: string;

  chatdab: number;
  chatname: Promise<string>;

  pulses: Collection<number, Pulse<S>> = new Collection();

  constructor({ trollegle, sourceid, url, chatdab }: MothershipOptions<S>) {
    this.trollegle = trollegle;
    this.sourceid = sourceid;
    this.url = url;
    this.chatdab = chatdab;

    this.chatname = this.getname();
  }

  async getname(): Promise<string> {
    const res = await fetch(`${this.url}/${Math.random()},-2-${this.chatdab}`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
    }).catch(() => {});
    if (!res)
      throw new Error(`unable to get chat name for chatdab ${this.chatdab}`);

    const body = await res.text().catch(() => {});
    if (!body)
      throw new Error(
        `unable to read body for chat name check request for chatdab ${this.chatdab}`
      );

    const match = nameregex.exec(body);
    if (!match) throw new Error("got no chat name match");
    if (match.length !== 2)
      throw new Error(
        `got match array of length ${match.length}: "${match.join(", ")}"`
      );

    return match[1];
  }

  async set(options: PulseWords, state: boolean): Promise<void> {
    const { word, name } = options;

    await fetch(`${this.url}/${word},${name}-${+state}-${this.chatdab}`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
    }).catch(() => {});
  }

  create(options: CreatePulseOptions<S>): Pulse<S> {
    return new Pulse({
      mothership: this,
      word: options.word,
      name: options.name,
      user: options.user,
    });
  }

  createDecayTimeout(user: User<S>, from?: number): number {
    const { decaytimems } = user.trollegle.config.pulse,
      delay =
        typeof from === "number"
          ? from + decaytimems - Date.now()
          : decaytimems;

    return setTimeout(
      () => (user.connection.disconnect(), user.trollegle.repulse(user)),
      Math.max(0, delay)
    );
  }

  async post(pulse: Pulse<S>): Promise<Pulse<S> | null> {
    const { word, name, time } = pulse;
    if (pulse.operation) return null;

    this.pulses.set(time, pulse);

    await (pulse.operation = this.set({ word, name }, true));
    pulse.operation = undefined;

    return pulse;
  }

  async takedown(key: number): Promise<Pulse<S> | null> {
    const pulse = this.pulses.get(key);
    if (!pulse || pulse.operation) return null;

    pulse.down = true;
    this.pulses.delete(key);

    const { word, name } = pulse;
    await (pulse.operation = this.set({ word, name }, false));
    pulse.operation = undefined;

    return pulse;
  }
}
