import { RawTrollegleConfig } from "./lib/trollegle/mod.ts";

export const config: RawTrollegleConfig = {
  user: {
    invitedelayms: 10000,
    toothpaste: true,
    toothtries: 3,
  },
  account: {
    username: {
      minlength: 1,
      maxlength: 16,
    },
    password: {
      minlength: 8,
      maxlength: 32,
      salt: "",
      iterations: 0,
      bitlength: 0,
    },
  },
  message: {
    flood: {
      maxcount: 4,
      timems: 7500,
    },
  },
  proxy: {
    bantimems: 60000,
  },
  pulse: {
    decaytimems: 600000,
    defaultinterests: [
      "trollegleyearsix",
      "trollegle",
      "bellawhiskey",
      "developer",
      "development",
      "programming",
      "programmer",
      "javascript",
      "typescript",
      "python",
      "twentyonerooms",
      "java",
      "maths",
      "science",
      "chemistry",
      "cs",
    ],
  },
};
