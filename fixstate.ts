const key = prompt("key?")?.trim();
if (!key) Deno.exit();

const json = prompt("value?")?.trim();
if (!json) Deno.exit();

const value = JSON.parse(json);
let users = 0;
const state = (await import("./state.json", { assert: { type: "json" } }))
  .default;
for (const user of Object.values(state.users) as any[]) {
  (user as any)[key] = value;
  users++;
}
Deno.writeTextFileSync("./state.json", JSON.stringify(state));

console.log(`updated ${users} users`);
