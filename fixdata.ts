import { Database } from "x/sqlite3@0.9.1/mod.ts";

const db = new Database("accounts.db", { unsafeConcurrency: true });
db.exec(
  `CREATE TABLE IF NOT EXISTS Accounts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        userlow TEXT,
        user TEXT,
        pash TEXT,
        data TEXT
      )`
);
const statements = {
    data: db.prepare("SELECT id, data FROM Accounts"),
    update: db.prepare("UPDATE Accounts SET data = ? WHERE id = ?"),
  },
  map: Map<number, any> = new Map();

for (const { id, data } of statements.data.all<{ id: number; data: string }>())
  map.set(id, JSON.parse(data));

const key = prompt("key?")?.trim();
if (!key) Deno.exit();

const json = prompt("value?")?.trim();
if (!json) Deno.exit();

const value = JSON.parse(json);
let records = 0,
  users = 0;
for (const [id, data] of map.entries()) {
  data[key] = value;
  records += statements.update.run(JSON.stringify(data), id);
}

const state = (await import("./state.json", { assert: { type: "json" } }))
  .default;
for (const { data } of Object.values(state.users) as any[]) {
  (data as any)[key] = value;
  users++;
}
Deno.writeTextFileSync("./state.json", JSON.stringify(state));

console.log(`updated ${records} records`);
console.log(`updated ${users} users`);
