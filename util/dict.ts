import { dict } from "../data/dict.ts";
import { User } from "../lib/userlist/user.ts";
import { fstring } from "./string.ts";

export function formatNick(
  user: User<any>,
  dids: keyof (typeof dict)["format"]["dids"]
): string {
  return fstring(dict.format.dids[dids], user.id, user.nick);
}

export function formatMoney(money: number): string {
  return fstring(dict.symbols.money, money.toFixed(2));
}
