export const proxyurls = [
  "username:password@127.0.0.1:8080",
  "username@0.0.0.0:420",
  "192.168.1.108:69",
  "...",
].map((str) => new URL(`http://${str}`));
