export type Strictly<T extends Record<PropertyKey, any>> =
  | Record<PropertyKey, never>
  | T;
