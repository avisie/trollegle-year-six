export function formattime(
  seconds: number,
  minutes: number,
  hours: number
): string {
  const result: string[] = [];
  hours && result.push(`${hours} h`),
    minutes && result.push(`${minutes} min`),
    seconds && result.push(`${seconds} s`);
  return result.join(" ");
}

export function minsec(time: number): string {
  let sec = Math.floor(time) / 1000;
  const ms = sec < 20,
    min = Math.floor(sec / 60);
  if (min > 90) return formattime(0, min % 60, Math.floor(min / 60));
  if (!(min < 1)) sec = Math.floor((sec % 60) * 1000) / 1000;
  return formattime(ms ? sec : Math.floor(sec), min, 0);
}
