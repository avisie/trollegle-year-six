export function isRecord(value: unknown): value is Record<any, unknown> {
  if (value === null || typeof value !== "object" || Array.isArray(value))
    return false;
  return true;
}

export type SetCallback = (
  target: Record<PropertyKey, unknown>,
  prop: keyof typeof target,
  value: typeof target[typeof prop]
) => unknown;

export function proxify<T extends Partial<Record<PropertyKey, any>>>(
  obj: T,
  callback: SetCallback
): T {
  return new Proxy(obj, {
    set(target, property, value) {
      if (value && typeof value === "object")
        value = deepproxify(value, callback);
      const result = Reflect.set(target, property, value);
      callback(target, property, value);
      return result;
    },
  });
}

export function deepproxify<T extends Record<PropertyKey, any> | any[]>(
  obj: T,
  callback: SetCallback
): T {
  if (Array.isArray(obj)) return proxify(obj, callback);
  const result: Partial<T> = {};
  for (const key of Object.keys(obj)) {
    const value: T[keyof T] = obj[key];
    result[key as keyof T] =
      value && typeof value === "object"
        ? deepproxify(value, callback)
        : obj[key];
  }
  return proxify(result, callback) as T;
}
