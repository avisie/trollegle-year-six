import { TemplateArray } from "./string.ts";

export type Element<T extends readonly unknown[]> =
  T extends readonly (infer E)[] ? E : never;

export type ArrayFilter<T extends any> = Parameters<
  (typeof Array<T>)["prototype"]["filter"]
>[0];

export function chop<T>(array: T[], n: number): T[][] {
  const result: T[][] = [];
  for (let i = 0; i < Math.ceil(array.length / n); i++)
    result.push(array.slice(n * i, n * i + n));
  return result;
}

export function random<T extends any>(array: readonly T[]): T | undefined {
  if (!array.length) return;
  return array[Math.floor(Math.random() * array.length)];
}

export function normalize(thing: string | TemplateArray): string {
  if (typeof thing === "string") return thing;

  let result = "";
  for (const element of thing) {
    const part: string = Array.isArray(element) ? random(element) : element;
    result += Array.isArray(part) ? normalize(part) : part ?? "";
  }
  return result;
}

export function equals(a1: any[], a2: any[]): boolean {
  if (!a2 || a1.length != a2.length) return false;

  a1 = a1.slice().sort();
  a2 = a2.slice().sort();

  for (let i = 0, l = a1.length; i < l; i++)
    if (a1[i] instanceof Array && a2[i] instanceof Array) {
      if (!equals(a1[i], a2[i])) return false;
    } else if (a1[i] != a2[i]) return false;

  return true;
}

export const base64map: readonly string[] = [
  "=",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "+",
  "/",
];

export function uint8arraytobase64(arr: Uint8Array): string {
  let i = 0;
  const parts: number[] = [];
  while (i < arr.length) {
    const current = (arr[i++] << 16) | ((arr[i++] ?? 0) << 8) | (arr[i++] ?? 0);
    for (let i = 18; i >= 0; i -= 6) {
      const index = (current & (0b111111 << i)) >>> i;
      parts.push(index ? index + 1 : 0);
    }
  }

  let result = "";
  for (const part of parts) result += base64map[part];
  return result;
}
