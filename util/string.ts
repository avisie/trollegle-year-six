import { normalize } from "./array.ts";

export type TemplateArray = readonly (string | TemplateArray)[];
export type Template = string | TemplateArray;

export function fstring(
  format: Template,
  ...args: (Template | number | undefined)[]
): string {
  const str = typeof format === "string" ? format : normalize(format);

  return str.replace(/{(\d+)}/g, (match, index) => {
    const part = args[index];

    if (part === undefined) return match;
    if (typeof part === "number") return part.toString();
    if (typeof part === "string") return part;

    return normalize(part);
  });
}

export type Stringable = { toString(): string };
export function padStart(
  thing: Stringable,
  maxlength: number,
  fillString?: string
): string {
  return thing.toString().padStart(maxlength, fillString);
}
export function padEnd(
  thing: Stringable,
  maxlength: number,
  fillString?: string
): string {
  return thing.toString().padEnd(maxlength, fillString);
}

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
export function fdate(time: number): string {
  const date = new Date(time),
    y = padStart(date.getUTCFullYear(), 4, "0"),
    M = months[date.getUTCMonth()],
    d = padStart(date.getUTCDate(), 2, "0"),
    h = padStart(date.getUTCHours(), 2, "0"),
    m = padStart(date.getUTCMinutes(), 2, "0"),
    s = padStart(date.getUTCSeconds(), 2, "0");

  return `${d} ${M} ${y} at ${h}:${m}:${s} (UTC)`;
}

export function ftime(time: number): string {
  const seconds = Math.floor(time / 1000),
    s = seconds % 60,
    minutes = Math.floor(seconds / 60),
    m = minutes % 60,
    hours = Math.floor(minutes / 60),
    h = hours % 24,
    days = Math.floor(hours / 24),
    d = days % 30,
    months = Math.floor(days / 30),
    M = months % 12,
    years = Math.floor(months / 12);

  const result = [];
  if (years) result.push(`${years} year${years === 1 ? "" : "s"}`);
  if (M) result.push(`${M} month${M === 1 ? "" : "s"}`);
  if (d) result.push(`${d} day${d === 1 ? "" : "s"}`);
  if (h) result.push(`${h} hour${h === 1 ? "" : "s"}`);
  if (m) result.push(`${m} minute${m === 1 ? "" : "s"}`);
  if (s) result.push(`${s} second${s === 1 ? "" : "s"}`);

  const last = result.splice(result.length - 2, 2).join(" and ");

  return result.reduce((res, str) => (res += str + ", "), "") + last;
}
