import { Trollegle } from "./lib/trollegle/mod.ts";
import { CLI } from "./lib/cli/mod.ts";

import { proxyurls } from "./util/proxyurls.ts";

import { userinit } from "./events/init.ts";
import { Omegle } from "./lib/sources/omegle.ts";
import { Dummy } from "./lib/sources/dummy.ts";

import { names } from "./data/names.ts";
import { config } from "./config.ts";

export const cli = new CLI({
  paused: true,
  prefix: "\x1b[38;5;21mYou:\x1b[0m ",
});

const sources = {
  omegle: Omegle,
  dummy: Dummy,
};

export type TrollegleSources = typeof sources;
export const trollegle = new Trollegle<TrollegleSources>({
  cli,
  config,
  statefile: "./state.json",
  sources,
  proxies: proxyurls,
  commandPath: "./commands",
  defaultNicks: names,
  userInitializer: userinit,
  mothership: {
    sourceid: "omegle",
    // make sure the url does not have a slash at the end
    url: "https://bellawhiskey.ca/trollegle",
    // replace 'null' with a 12-or-so-decimal number (eg. 0.123456789012)
    // and DO NOT USE THE ONE LISTED ABOVE, just create your own
    // DO NOT SHARE THIS VALUE, else someone could impersonate your room or take its pulses down
    chatdab: null,
  },
});

trollegle.init();
